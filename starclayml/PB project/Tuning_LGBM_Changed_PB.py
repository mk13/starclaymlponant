import pandas as pd
import numpy as np
import os
from sklearn.model_selection import RandomizedSearchCV , PredefinedSplit
import lightgbm as lgb
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error,  mean_squared_error, f1_score , confusion_matrix , roc_auc_score
import random

path = 'F:/Datalab/xx - Works/ML_FINAL'

os.chdir(path + '/starclayml/PB project')
from Preprocessing import correct_format, select_data, get_previous_PB_and_delay_changed_days, fill_missing_values
from Preprocessing import transform_categorical_variables_values, add_FROM_TO_latitude_longitude, add_calendar_variables
from Preprocessing import add_FCST_total_cabin_by_PB_level_variables, categorical_to_dummies, drop_unuseful_variables
from Preprocessing import *
from utils import predict_PB_for_one_cruise, lgb_plot_importance
#from metrics import  regTotCabinIndivAccuracy, avgRegTotCabinIndivAccuracy
data_input_link = path + '/Data/Data_input'



#Chargement de l'extrait de données, 
df = pd.read_excel(data_input_link+'/DATA9.xlsx')

df = correct_format(df)
df = select_data(df)
df = fill_missing_values(df)
df = get_previous_PB_and_delay_changed_days(df , PBSSCALENDAR_link = 'PBSSCALENDAR.xlsx')
df = transform_categorical_variables_values(df)

df = add_FROM_TO_latitude_longitude(df)
df = add_calendar_variables(df)
df = add_FCST_total_cabin_by_PB_level_variables(df)
df = categorical_to_dummies(df)
df = drop_unuseful_variables(df)
df0 = df.copy()
df = drop_unuseful_variables(df)

#For binary classification
df['PB_CHANGED'] = 0
df.loc[df['Current_PB'] <  df['previous_PB_value'], 'PB_CHANGED'] = 1
target = ['PB_CHANGED']

predictors = [col for col in list(df.columns) if (col not in ( [ 'PB_predicted', 'test_fold', 'data_set_id' ,'Round_PB_Changed_Predicted' ,'PB_changed_predicted' ,'PB_CHANGED'  ,'Current_PB' ,'Booking_Date_OBS' , 'Key_Cruise_Code'] 
              )) ]


    
#*******************************
#Preprocessing - NEW
#*******************************
df = correct_format(df)
df = select_data(df)
df = fill_missing_values(df)
if shift_changes_to_previous: 
    df = get_previous_PB_and_delay_changed_days_shift_changes_to_previous(df)
else:
    df = get_previous_PB_and_delay_changed_days_shift_changes_to_next(df , PBSSCALENDAR_link = data_input_link + '/PBSSCALENDAR.xlsx') 
df = transform_categorical_variables_values(df)
df = add_FROM_TO_latitude_longitude(df)
df = add_calendar_variables(df)
df = add_FCST_total_cabin_by_PB_level_variables(df)
df = categorical_to_dummies(df)
df = drop_unuseful_variables(df)

predictors = get_predictors(df, with_PB_infos = True,  PB_Changed_Predicted = False)



cruises_list = list(df['Key_Cruise_Code'].unique())
Training_percentage = 70
Number_of_cruises_for_training = round((Training_percentage/100)*df['Key_Cruise_Code'].unique().shape[0])
training_cruises = random.sample(cruises_list,  Number_of_cruises_for_training )

train = df[df['Key_Cruise_Code'].isin(training_cruises)].copy()
test = df[~df['Key_Cruise_Code'].isin(training_cruises)].copy()

CV_dataset_percentage = 33
Number_of_cruises_by_cv_dataset = round((CV_dataset_percentage/100)*df['Key_Cruise_Code'].unique().shape[0])
cruises_set1 = random.sample(cruises_list,  Number_of_cruises_by_cv_dataset )
cruises_set2 = random.sample(list(set(cruises_list) - set(cruises_set1)),  len(cruises_set1) )
cruises_set3 = list((set(cruises_list) - set(cruises_set1)) - set(cruises_set2))

df['test_fold'] = 1
df.loc[df['Key_Cruise_Code'].isin(cruises_set2),  'test_fold'] = 2
df.loc[df['Key_Cruise_Code'].isin(cruises_set3),  'test_fold'] = 3




params = {    
#Boosting parameters
    'boosting_type': ['dart'] , # 'gbdt'
    'objective': ['binary'], 
    'metric': ['auc'],
    'scale_pos_weight': [45,10,30,70,90,120,200],
#    
    'learning_rate':  [0.1,0.01,0.05,0.09,0.15,0.2,0.05,0.03],  #0.08
    'num_iterations' : [274,50,100,150,200,250,300,350,400]   , #312
 
    'max_bin':  [243,50,100,150,200,300],
    'min_data_in_bin': [12,2,5,8,10,15,20] ,

 #Tree specific Parameters   
    'min_data_in_leaf': [9,1,5,15,20,25],
    'max_depth': [10,3,5,12,15,20],

    'bagging_freq' :  [5,2,8,10,15],     
    'bagging_fraction' :   [ 0.95,0.09,0.08,0.07], 
    'feature_fraction':  [ 0.67,0.5,0.4,0.3,0.8,0.9],
    'bagging_seed' : [3],
    
    'reg_alpha' : [0.9,0.8,0.5],
    'reg_lambda' : [29,10,50],
    
    'random_state': [3],
        }


#GridSearchCV 
#RandomizedSearchCV
grid = RandomizedSearchCV(lgb.LGBMClassifier() , 
                          params, 
                          scoring = 'roc_auc',
                          n_jobs=-1, 
                          cv =  PredefinedSplit(df['test_fold']) , 
                          verbose = 10)  

grid.fit(df[predictors] , np.ravel(df['PB_CHANGED']))  
print(grid.best_estimator_) 

grid.best_estimator_.get_params()


params = {    
#Boosting parameters
    'boosting_type': 'dart' , # 'gbdt'
    'objective': 'binary', 
    'metric': 'auc',
    'scale_pos_weight': 45,
#    
    'learning_rate':  0.1,  #0.08
    'num_iterations' : 274   , #312
 
    'max_bin':  243,
    'min_data_in_bin': 12 ,

 #Tree specific Parameters   
    'min_data_in_leaf': 9,
    'max_depth': 10,

    'bagging_freq' :  5,     
    'bagging_fraction' :    0.95, 
    'feature_fraction':   0.67,
    'bagging_seed' : 3,
    
    'reg_alpha' : 0.9,
    'reg_lambda' : 29,
    
    'random_state': 3,
        }
lgb_train = lgb.Dataset(
    train[predictors], 
    train[target], 
    feature_name=predictors,
    )
lgb_train.raw_data = None

model = lgb.train(
    params,
    lgb_train,
    num_boost_round=3000,
#    verbose_eval=100,
)
test['prediction'] = model.predict(test[predictors])
print(roc_auc_score(test[target] , test['prediction']))
print(f1_score(test[target] , round(test['prediction'])))
confusion_matrix(test[target] , round(test['prediction']))




0.9597097300063206
0.4744645799011532
Out[32]: 
array([[13104,   555],
       [   83,   288]])


#best params PB_changed_roc_auc, shift changes to the day before
params = {    
#Boosting parameters
    'boosting_type': 'dart' , # 'gbdt'
    'objective': 'binary', 
    'metric': 'auc',
    'scale_pos_weight': 45,
#    
    'learning_rate':  0.1,  #0.08
    'num_iterations' : 274   , #312
 
    'max_bin':  243,
    'min_data_in_bin': 12 ,

 #Tree specific Parameters   
    'min_data_in_leaf': 9,
    'max_depth': 10,

    'bagging_freq' :  5,     
    'bagging_fraction' :    0.95, 
    'feature_fraction':   0.67,
    'bagging_seed' : 3,
    
    'reg_alpha' : 0.9,
    'reg_lambda' : 29,
    
    'random_state': 3,
        }

    
    
    
    
#best params PB_changed_roc_auc, shift changes to the day after
#params = {    
##Boosting parameters
#    'boosting_type': 'dart' , # 'gbdt'
#    'objective': 'binary', 
#    'metric': 'auc',
#    'scale_pos_weight': 45,
##    
#    'learning_rate':  0.13,  #0.08
#    'num_iterations' : 243   , #312
# 
#    'max_bin':  193,
#    'min_data_in_bin': 10 ,
#
# #Tree specific Parameters   
#    'min_data_in_leaf': 5,
#    'max_depth': 6,    
#
#    'bagging_freq' :  5,     
#    'bagging_fraction' :   0.9, 
#    'feature_fraction':  0.7,
#    'bagging_seed' : 3,
#    
#    'reg_alpha' : 0.1,
#    'reg_lambda' : 25,
#    
#    'random_state': 3,
#        }
    

#Best params for PB_changed with F1 as metric
#params = {    
##Boosting parameters
#    'boosting_type': ['dart' ], # 'gbdt'
#    'objective': ['binary'], 
#    'metric': ['f1'],
#    'scale_pos_weight': [42,43, 44, 45, 46],
##    
#    'learning_rate': [0.28],
#    'num_iterations' : [278],
#
# 
#    'max_bin': [ 225],
#    'min_data_in_bin': [3 ],
#
# #Tree specific Parameters   
#    'min_data_in_leaf': [3],
#    'max_depth': [8],    
#
#    'bagging_freq' : [9],     
#    'bagging_fraction' : [ 0.85], 
#    'feature_fraction': [0.8],
#    'bagging_seed' : [3],
#    
#    'reg_alpha' : [0.6],
#    'reg_lambda' : [25],
#    
#    'random_state': [3],
#        }
       

    
    
    #Params shift day after, cruise by cruise exe, PB_changed
#    params = {    
##Boosting parameters
#    'boosting_type': 'dart' , # 'gbdt'
#    'objective': 'binary', 
#    'metric': 'auc',
#    'scale_pos_weight': 45,
##    
#    'learning_rate':  0.1,  #0.08
#    'num_iterations' : 266   , #312
#    
#    'max_bin':  225,
#    'min_data_in_bin': 3 ,
# 
# #Tree specific Parameters   
#    'min_data_in_leaf': 5,
#    'max_depth': 6,    
#    
#    'bagging_freq' :  5,     
#    'bagging_fraction' :   0.9, 
#    'feature_fraction':  0.7,
#    'bagging_seed' : 3,
#    
#    'reg_alpha' : 0.1,
#    'reg_lambda' : 25,
#    
#    'random_state': 3,
#        }