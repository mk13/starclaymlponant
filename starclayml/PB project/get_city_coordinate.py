from geopy.geocoders import Nominatim
import pandas as pd

geolocator = Nominatim(user_agent="Ponant")

def get_city_coordinates(city_name,encoding = 'ISO-8859-1'):

    # On lit le fichier csv des coordonnées des villes
    city_coordinates_file = open("city_coordinates.csv", "a")

    # Création d'un dataframe avec "city" comme index
    city_coordinates_df = pd.read_csv("city_coordinates.csv", sep="|",
                                                            names=["city",
                                                            "latitude",
                                                            "longitude"],
                                                            index_col=["city"],encoding = 'ISO-8859-1')
    latitude = ""
    longitude = ""

    # Récupération des données si la ville existe déjà
    if city_name in city_coordinates_df.index:
        latitude = city_coordinates_df.loc[city_name, "latitude"]
        longitude = city_coordinates_df.loc[city_name, "longitude"]

    # Sinon utiliser geopy pour récupérer les coordonnées
    else:
        location = geolocator.geocode(city_name)

        # Si la ville n'a pas de location on throw une erreur
        if not location:
            city_coordinates_file.close()
            raise ValueError(f"Can't find coordinates of city \"{city_name}\"")

        # Sauvegarde des coordonnées dans le fichier
        city_coordinates_file.write(f"{city_name}|{location.latitude}|{location.longitude} \n")
        city_coordinates_file.close()
        latitude = location.latitude
        longitude = location.longitude

    city_coordinates_file.close()
    return latitude, longitude


def get_all_city_coordinates(city_as_index=True,encoding = 'ISO-8859-1'):
    if city_as_index:
        city_coordinates_df = pd.read_csv("city_coordinates.csv", sep="|",
                                                                names=["city",
                                                                "latitude",
                                                                "longitude"],
                                                                index_col=["city"],encoding = 'ISO-8859-1')

    else:
        city_coordinates_df = pd.read_csv("city_coordinates.csv", sep="|",
                                                                names=["city",
                                                                "latitude",
                                                                "longitude"],encoding = 'ISO-8859-1')
    return city_coordinates_df
