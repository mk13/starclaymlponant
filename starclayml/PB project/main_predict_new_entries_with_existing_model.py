import pandas as pd
import os
from sklearn.externals import joblib


path = 'F:/Datalab/xx - Works/ML_FINAL'

#links
model_link = path + '/Data/Models'
project_link = path + '/starclayml/PB project'
data_to_predict_link = path + '/Data/Data_to_predict'
data_output_link = path + '/Data/Data_output'
data_input_link = path + '/Data/Data_input'


shift_changes_to_previous = True
with_PB_infos = True
PB_Changed_Predicted = True
target = 'Current_PB'
cruise_by_cruise = False

os.chdir(project_link)
from Preprocessing import read_input_data
from Preprocessing import correct_format, select_data, get_previous_PB_and_delay_changed_days_shift_changes_to_previous, fill_missing_values
from Preprocessing import transform_categorical_variables_values, add_FROM_TO_latitude_longitude, add_calendar_variables
from Preprocessing import add_FCST_total_cabin_by_PB_level_variables, categorical_to_dummies, drop_unuseful_variables
from Preprocessing import get_previous_PB_and_delay_changed_days_shift_changes_to_next
from utils import predict_PB_for_one_cruise, lgb_plot_importance
from modelization import get_predictors, Define_Cross_Validation_Sets, get_best_recorded_parameters, Predict_PB_Cross_Validation_KFlods
from modelization import Evaluation_metrics, Predict_PB_Leave_One_Cruise_Out


#*******************************
#Chargement de l'extrait de données
#*******************************
df_predict = read_input_data(data_to_predict_link)

    
#*******************************
#Preprocessing
#*******************************
df_predict = correct_format(df_predict)
df_predict = select_data(df_predict)
df_predict = fill_missing_values(df_predict)
if shift_changes_to_previous: 
    df_predict = get_previous_PB_and_delay_changed_days_shift_changes_to_previous(df_predict)
else:
    df_predict = get_previous_PB_and_delay_changed_days_shift_changes_to_next(df_predict , PBSSCALENDAR_link = (data_input_link + '/PBSSCALENDAR.xlsx'))
df_predict = transform_categorical_variables_values(df_predict)
df_predict = add_FROM_TO_latitude_longitude(df_predict)
df_predict = add_calendar_variables(df_predict)
df_predict = add_FCST_total_cabin_by_PB_level_variables(df_predict)
df_predict = categorical_to_dummies(df_predict)
df_predict = drop_unuseful_variables(df_predict)


#*******************************
#Predict PB_CHANGED
#*******************************
if PB_Changed_Predicted:
    model = joblib.load(model_link +'/premodel_PB_CHANGED_predictor.pkl')
    predictors = model.feature_name()
    df_predict['PB_Changed_Predicted'] = model.predict(df_predict[predictors])
    #df_predict['Round_PB_Changed_Predicted'] = round(df_predict['PB_Changed_Predicted'])
    df_predict['Round_PB_Changed_Predicted']  = df_predict['PB_Changed_Predicted'].round()
#df_predict.to_excel(data_output_link + '/DATA_to_predict_results.xlsx')

#*******************************
#Main model
#*******************************
model = joblib.load(model_link + '/' + target + '_predictor'+ '.pkl')
predictors = model.feature_name()
df_predict['PB_Predicted'] = model.predict(df_predict[predictors])
df_predict['Round_PB_Predicted']  = df_predict['PB_Predicted'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )


Evaluation_metrics(df_predict , target = 'PB_CHANGED', with_PB_infos = True, tolerance_values= [0, 1, 2])

#Evaluation_metrics(df_predict , target = 'Current_PB', with_PB_infos = True, tolerance_values= [0, 1, 2])


#*******************************
#Save the dataframe
#*******************************
df_predict.to_excel(data_output_link + '/DATA_to_predict_results.xlsx')


def main():
    pass

if __name__ == "__main__":
   main()