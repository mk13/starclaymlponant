import pandas as pd
import numpy as np
import os
from sklearn.model_selection import RandomizedSearchCV , PredefinedSplit
import lightgbm as lgb
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error, mean_squared_error
import datetime
import random
import csv


os.chdir('/home/allan/Documents/PONANT/starclayml/PB project')
from Preprocessing import correct_format, select_data, get_previous_PB_and_delay_changed_days, fill_missing_values
from Preprocessing import transform_categorical_variables_values, add_FROM_TO_latitude_longitude, add_calendar_variables
from Preprocessing import add_FCST_total_cabin_by_PB_level_variables, categorical_to_dummies, drop_unuseful_variables
from utils import predict_PB_for_one_cruise, lgb_plot_importance, dict_product, save_obj
from metrics import regTimingAccuracy, avgRegTimingAccuracy, regTotCabinIndivAccuracy, avgRegTotCabinIndivAccuracy
os.chdir('/home/allan/Documents/PONANT/Data')


#Chargement de l'extrait de données, 
df = pd.read_excel('DATA9.xlsx')

df = correct_format(df)
df = select_data(df)
df = get_previous_PB_and_delay_changed_days(df , PBSSCALENDAR_link = 'PBSSCALENDAR.xlsx')
df = fill_missing_values(df)
df = transform_categorical_variables_values(df)
df0 = df.copy()
df = add_FROM_TO_latitude_longitude(df)
df = add_calendar_variables(df)
df = add_FCST_total_cabin_by_PB_level_variables(df)
df = categorical_to_dummies(df)
df = drop_unuseful_variables(df)


continuious_varibles_out_of_model=  ['Delay_PB_Changed_Days',
                                     'previous_PB_value',
                                                 
                                     'Total_Cab_PB_30',
                                     'Total_Cab_PB_25',
                                     'Total_Cab_PB_20',
                                     'Total_Cab_PB_15',
                                     'Total_Cab_PB_10',
                                     'Total_Cab_PB_5',
                                     'Total_Cab_PB_0',
                                     'Cluster_Moyenne_Moyenne_PB',
                                     'Moyenne_PB',
                            
                                     'FCST_PB30',
                                     'FCST_PB25',
                                     'FCST_PB20',
                                     'FCST_PB15',
                                     'FCST_PB10',
                                     'FCST_PB5',
                                     'FCST_PB0',
                                     
                                     'FCST_Last_PB',
                                     'Total_Cab_Last_PB',
                                     'Ecart_Last_PB',
                                     
                                     'Ecart_PB_30',
                                     'Ecart_PB_25',
                                     'Ecart_PB_20',
                                     'Ecart_PB_15',
                                     'Ecart_PB_05',
                                     'Ecart_PB_0']

target = ['Current_PB']
predictors = [col for col in list(df.columns) if (col not in (target + ['Booking_Date_OBS' , 'Key_Cruise_Code'] 
            #  + continuious_varibles_out_of_model
              )) ]

cruises_list = list(df['Key_Cruise_Code'].unique())
Training_percentage = 70
Number_of_cruises_for_training = round((Training_percentage/100)*df['Key_Cruise_Code'].unique().shape[0])
training_cruises = random.sample(cruises_list,  Number_of_cruises_for_training )

train = df[df['Key_Cruise_Code'].isin(training_cruises)].copy()
test = df[~df['Key_Cruise_Code'].isin(training_cruises)].copy()

CV_dataset_percentage = 33
Number_of_cruises_by_cv_dataset = round((CV_dataset_percentage/100)*df['Key_Cruise_Code'].unique().shape[0])
cruises_set1 = random.sample(cruises_list,  Number_of_cruises_by_cv_dataset )
cruises_set2 = random.sample(list(set(cruises_list) - set(cruises_set1)),  len(cruises_set1) )
cruises_set3 = list((set(cruises_list) - set(cruises_set1)) - set(cruises_set2))

df['test_fold'] = 1
df.loc[df['Key_Cruise_Code'].isin(cruises_set2),  'test_fold'] = 2
df.loc[df['Key_Cruise_Code'].isin(cruises_set3),  'test_fold'] = 3




params = {    
#Boosting parameters
    'boosting_type': ['dart'], # 'gbdt'
    'objective': ['regression'], 
    'metric': ['mean_absolute_error'],
#    
    'learning_rate': [ 0.95],
    'num_iterations' : [164],

 
    'max_bin': [ 225],
    'min_data_in_bin': [3 ],

 #Tree specific Parameters   
    'min_data_in_leaf': [7],
   # 'max_depth': [4, 5, 6,7, 8],
    'min_child_samples' : [4],
    'num_leaves': [12] ,
    

    'bagging_freq' : [7],     
    'bagging_fraction' : [0.95], 
    'feature_fraction': [0.95],
    'bagging_seed' : [3],
    
    'reg_alpha' : [0 ],
    'reg_lambda' : [16],
    
    'random_state': [3],
        }


grid = RandomizedSearchCV(lgb.LGBMRegressor() , 
                          params, 
                          scoring = 'neg_mean_absolute_error', 
                          n_jobs=-1, 
                          cv =  PredefinedSplit(df['test_fold']) , 
                          verbose = 10)  

grid.fit(df[predictors] , np.ravel(df[target]))  
print(grid.best_estimator_) 



lgb_train = lgb.Dataset(
    train[predictors], 
    train[target], 
    feature_name=predictors,
    )
lgb_train.raw_data = None

lgb_valid = lgb.Dataset(
    test[predictors], 
    test[target], 
    )
lgb_valid.raw_data = None



model = lgb.train(
    grid.best_params_,
    lgb_train,
    num_boost_round=10000,
    valid_sets=[lgb_train, lgb_valid],
    early_stopping_rounds=250,
    verbose_eval=100,
)

test['prediction'] = model.predict(test[predictors])
#test['prediction']  = test['prediction'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
mae = mean_absolute_error(test[target] , test['prediction'])
print('mae: ', mae)












#start_time = datetime.datetime.now()
#score_opt = 0
#iteration = 0
#for parameters in dict_product(params):
#    iteration = iteration + 1 
#    print('iteration N : ' , iteration)
#    model = lgb.train(
#        parameters,
#        lgb_train,
#        num_boost_round=10000,
#        valid_sets=[lgb_train, lgb_valid],
#        early_stopping_rounds=250,
#        verbose_eval=100,
#    )
#    print("training duration : ", datetime.datetime.now() - start_time)
#
#    test['prediction'] = model.predict(test[predictors])
#    test['prediction']  = test['prediction'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
#    mae = mean_absolute_error(test[target] , test['prediction'])
#    print('mae: ', mae)
#    
#    test['equal'] = 0
#    test.loc[test['Current_PB'] == test['prediction'] , 'equal'] = 1
#    test[ 'previous_prediction'] = test['prediction'].shift(1)
#    test.loc[first_lines_cruises , 'previous_prediction']  = test[ 'prediction']
#    test['equal_to_previous'] = 0
#    test.loc[test['Current_PB'] == test['previous_prediction'] , 'equal_to_previous'] = 1
#    results_matrix = test.groupby([ 'PB_changed','equal' , 'equal_to_previous']).agg({'Current_PB' : 'count'})
##    score = results_matrix.loc[1].loc[1].loc[0] + results_matrix.loc[1].loc[0].loc[1]
#    results_matrix.reset_index(inplace = True)
#    score = 0
#    if len(results_matrix.loc[(results_matrix['PB_changed'] == 1)  & (results_matrix['equal'] == 1) & (results_matrix['equal_to_previous'] == 0) , 'Current_PB']) == 1  :
#        score = score  + results_matrix.loc[(results_matrix['PB_changed'] == 1)  & (results_matrix['equal'] == 1) & (results_matrix['equal_to_previous'] == 0) , 'Current_PB'].values[0]
#    if len(results_matrix.loc[(results_matrix['PB_changed'] == 1)  & (results_matrix['equal'] == 0) & (results_matrix['equal_to_previous'] == 1) , 'Current_PB']) == 1:
#        score =  score +  results_matrix.loc[(results_matrix['PB_changed'] == 1)  & (results_matrix['equal'] == 0) & (results_matrix['equal_to_previous'] == 1) , 'Current_PB'].values[0]
#    if score != np.nan:
#        if score > score_opt:
#            opt_parameters = parameters
#            score_opt = score
#            print(score)
#            print("******BETTER PARAMS FOUND **** : ", parameters)
#            with open('opt_lgbm_parameters_V2.csv', 'w') as f:
#                for key in opt_parameters.keys():
#                    f.write("%s,%s\n"%(key,opt_parameters[key]))
#    print(parameters)
       
