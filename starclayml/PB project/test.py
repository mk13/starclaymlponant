df['dayofweek'] = df.PBSS_CAL_Date.dt.dayofweek 
df['week'] = df.PBSS_CAL_Date.dt.week 
df = df.sort_values(['Cruise_Code' ,  'PBSS_CAL_Date' ])

def next_filled_line_of_cruise(line_index, df, columns_to_fill):
    cruise = df.loc[line_index , 'Cruise_Code']
    return df[line_index:][(~df[line_index:][
            'Key_Booking_Date_OBS' ].isna()) & (df[line_index:][
                    'Cruise_Code'] == cruise)][columns_to_fill].reset_index(inplace = False ).loc[0]



def next_filled_line_of_cruise(line_index, df):
    cruise = df.iloc[line_index, 'Cruise_Code']
    return df[line_index:][(~df[line_index:][
            'Key_Booking_Date_OBS' ].isna()) & (df[line_index:][
                    'Cruise_Code'] == cruise)].index[0]     
columns_to_fill = [col for col in list(df.columns) if (col not in list(df_PB.columns))]
#df['ind'] = df.apply(lambda x: next_filled_line_of_cruise(x.index , df))
df[['Cruise_Code' , 'PBSS_CAL_Date']].groupby('Cruise_Code').last
df[columns_to_fill] = df[columns_to_fill].fillna(method = 'bfill', limit = 15)
        