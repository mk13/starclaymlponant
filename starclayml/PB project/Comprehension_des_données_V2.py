#Chargement des bibliothèques
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib.pylab as pylab

os.chdir('/home/allan/Documents/PONANT/Data')


#Chargement de l'extrait de données, 
df = pd.read_excel('Data_Third_Extract.xlsx')

#**************************
#Data Selection
#************************
#Format datetime variables
df['FCST_DATE_CREATION'] = pd.to_datetime(df['FCST_DATE_CREATION'])
df['Date_Premiere_Reservation_Indiv'] = pd.to_datetime(df['Date_Premiere_Reservation_Indiv'])
df['PB_Changed_Date'] = pd.to_datetime(df['PB_Changed_Date'])
df['Booking_Date_OBS'] = pd.to_datetime(df['Booking_Date_OBS'])
df['Mapping_Date_Departure'] = pd.to_datetime(df['Mapping_Date_Departure'])

#Sélection des données après le 01/11/2016
df = df[df['Booking_Date_OBS'] >= '2016-11-26'] 
print(df.shape)
#Suppression des enregistrement de croisi�res apr�s leurs d�parts
df = df[df['Booking_Date_OBS'] <= df['Mapping_Date_Departure'] ] 

#Remove lines with otal_NTR_EUR NULL
df = df[~df['Total_NTR_EUR'].isna()]
print(df.shape)
#Remove lines with Current_PB NULL
df = df[~df['Current_PB'].isna()]
print(df.shape)
#Remove lines with Current_PB NULL
df = df[~df['Key_Cruise_Code'].isna()]
print(df.shape)
#Remove lines with Current_PB NULL
df = df[~df['FCST_TOTAL_CABIN'].isna()]
print(df.shape)

#Remove group cruises
df = df[~((df['Mapping_Vessel'] == 'PONANT')  & 
   (df['Total_Cabin_Total_Group'] / df['Mapping_Cabin_Capacity'] >= 0.7)) ]
df = df[~((df['Mapping_Vessel'] != 'PONANT')  & 
   (df['Total_Cabin_Total_Group'] / df['Mapping_Cabin_Capacity'] >= 0.90)) ]
df = df[~df['Date_Premiere_Reservation_Indiv'].isna()]
print(df.shape)



#Tri des données par Code croisière et date
df = df.sort_values(['Key_Cruise_Code' , 'Booking_Date_OBS'])
df.nunique(dropna = False)[df.nunique(dropna = False) == 1]

#********************************
#Select variables
#********************************
#suppression des variables statiques
variables_to_remove = ['Mapping_Charter',
                       'Mapping_Cruise_Status',
                       'Mapping_Cluster_ML',
                       'Mapping_Histo_ML',
                       'Mapping_Destination_Production',
                       'FCST_UID',
                       'FCST_DATE',
                       'ID',
                       'Delay_PB_Changed_Days_MIN',
                       'FCST_COMMENTAIRES',
                       'CRUISE_CODE'
                       ]
df.drop(variables_to_remove , axis = 1, inplace = True)

#**************************
#Variables format
#************************
#Check string variables
print(df.dtypes[df.dtypes == "object"].index)
df[list(df.dtypes[df.dtypes == "object"].index)].head()


#********************************
#Correct values
#********************************
#Delay_PB_Changed_Days
df0 = df[[ 'Key_Cruise_Code',
          'Booking_Date_OBS', 
          'Current_PB',
 'Current_SupSingle',
 'Current_NeedDateLevel',
 'PB_Changed_Date',
 'Delay_PB_Changed_Days',
 'PB_CHANGED']]
df.reset_index(inplace = True, drop = True)
index_PB_Changed = list(df.loc[df['PB_CHANGED'] == 1 , 'Delay_PB_Changed_Days'].index)
first_lines_cruises = list(df[['Key_Cruise_Code']].reset_index().groupby('Key_Cruise_Code').first().index)
index_PB_Changed = [ind for ind in index_PB_Changed if (ind not in first_lines_cruises)]
df['previous_PB_Changed_Date'] =  df.loc[:,'PB_Changed_Date'].shift(1)
df.loc[index_PB_Changed, 'Delay_PB_Changed_Days'] = (df.loc[index_PB_Changed, 'Booking_Date_OBS'] - df.loc[index_PB_Changed,'previous_PB_Changed_Date']).dt.days
#df = df.drop(['previous_PB_Changed_Date',
#              'PB_CHANGED',
#              'PB_Changed_Date',] ,  axis = 1)
                       
#Mapping season to [Winter/Summer]
df['Mapping_Season'] = df['Mapping_Season'].apply(lambda x:''.join([c for c in x if c.isalpha()]))



#********************************
#Missing Values
#********************************
#Categorical Variables
df = df[~df['Mapping_FCST_Model'].isna() ]
df.loc[df['Mapping_Theme'].isna() , 'Mapping_Theme'] = 'No_Theme'
df.loc[df['Mapping_Guest'].isna() , 'Mapping_Guest'] = 'No_Guest'
df.loc[df['Mapping_Partnership'].isna() , 'Mapping_Partnership'] = 'No_Partnership'
#Continious Variables
df.loc[df['Moyenne_PB'].isna(), 'Moyenne_PB'] = df.loc[df['Moyenne_PB'].isna(), 'Current_PB'] * -1 
#Remplacer les valeurs manquantes en 0 pour certaines valeurs manquantes
nan_to_zeros_var = list(df.dtypes[(df.dtypes != 'object' ) & (df.dtypes !='datetime64[ns]')].index)
df[nan_to_zeros_var] = df[nan_to_zeros_var].fillna(0)

#Nombre de Valeurs manquantes par colonnes
df.isnull().sum()


#Lecture de la table PBSSCALENDAR
df_PB = pd.read_excel('PBSSCALENDAR.xlsx')
#Les variables clairement inutiles �  la problèmatique sont �  supprimer
df_PB.drop([ 'ID', 
             'UID', 
             'Editor', 
             'Creation_Timestamp', 
             'Edition_Timestamp',
             'SS_CAL_Amount',
             'PBSS_LT_Week',
             'ND_CAL_Amount',] , axis = 1 ,inplace = True)
df_PB['PBSS_CAL_Date'] = pd.to_datetime(df_PB['PBSS_CAL_Date'])

df = pd.merge(left = df,
              right = df_PB,
              left_on = ['Key_Cruise_Code' , 'Booking_Date_OBS' ],
              right_on = ['Cruise_Code' , 'PBSS_CAL_Date'] ,
              how = 'left')
df = df[~df['Cruise_Code'].isna()]
df = df.drop( ['Cruise_Code' ,   'PBSS_CAL_Date' ] ,axis  =1)


#Statistiques desvariables continues
categorical_variables = list(df.dtypes[df.dtypes == 'object'].index)
datetime_variables = list(df.dtypes[df.dtypes == 'datetime64[ns]'].index)
continious_variables = [col for col in list(df.columns) 
                        if(col not in(categorical_variables + datetime_variables)) ]
df.describe().to_csv('stats.csv', sep = ';')

#EXtract Negative values
V = pd.DataFrame(df[continious_variables].min()[df[continious_variables].min()<0])
V['Key_Cruise_Code'] = 'A'
V.columns = ['min' , 'Key_Cruise_Code']
for ind in list(V.index):
    V.loc[ ind , 'Key_Cruise_Code'] = df.loc[df[ind] == V.loc[ind , 'min'], 'Key_Cruise_Code'].iloc[0]
V.to_csv('valid.csv', sep = ';')

#Correlation
df[continious_variables].corr().to_csv('correlation_matrix.csv')


params = { 'axes.titlesize': 10,}
pylab.rcParams.update(params)

#Histogrammes des variables continues
df[continious_variables[0:10]].dropna(axis = 0).hist(bins=20, 
                              figsize=(20,60), 
                              xlabelsize = 10,
                              ylabelsize = 10,
                              layout=(29, 3) )
plt.show()


plt.scatter( df[ 'Current_PB'] , df['PB_CAL_Amount'] , alpha = 0.1)
