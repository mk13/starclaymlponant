import pandas as pd
import numpy as np
import os
from sklearn.model_selection import RandomizedSearchCV, train_test_split
import lightgbm as lgb
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error, mean_squared_error,precision_score, f1_score , confusion_matrix , roc_auc_score
import random
import math

path = 'F:/Datalab/xx - Works/ML_FINAL'

os.chdir(path + '/starclayml/PB project')
from Preprocessing import correct_format, select_data, get_previous_PB_and_delay_changed_days, fill_missing_values
from Preprocessing import transform_categorical_variables_values, add_FROM_TO_latitude_longitude, add_calendar_variables
from Preprocessing import add_FCST_total_cabin_by_PB_level_variables, categorical_to_dummies, drop_unuseful_variables
from Preprocessing import get_previous_PB_and_delay_changed_days_shift_change_to_previous
from utils import predict_PB_for_one_cruise, lgb_plot_importance
from metrics import regTimingAccuracy, avgRegTimingAccuracy, regTotCabinIndivAccuracy, avgRegTotCabinIndivAccuracy, avgClassTimingAccuracy, classTimingAccuracy
os.chdir(path + '/Data')


#Chargement de l'extrait de données, 
df = pd.read_excel('DATA9.xlsx')
df00 = df.copy()
df = correct_format(df)
df = select_data(df)
df = fill_missing_values(df)
#df = get_previous_PB_and_delay_changed_days(df , PBSSCALENDAR_link = 'PBSSCALENDAR.xlsx')
df = get_previous_PB_and_delay_changed_days_shift_change_to_previous(df)
df = transform_categorical_variables_values(df)

df = add_FROM_TO_latitude_longitude(df)
df = add_calendar_variables(df)
df = add_FCST_total_cabin_by_PB_level_variables(df)
df = categorical_to_dummies(df)
df = drop_unuseful_variables(df)
df0 = df.copy()



df['PB_CHANGED'] = 0
df.loc[df['Current_PB'] <  df['previous_PB_value'], 'PB_CHANGED'] = 1
categorical_variables = [var for var in list(df.dtypes[df.dtypes == 'object'].index) if (var not in ['Key_Cruise_Code']) ]

target = ['PB_CHANGED']
predictors = [col for col in list(df.columns) if (col not in ( 
        categorical_variables + 
        [ 'PB_predicted', 'test_fold',
              'data_set_id' ,'Round_PB_Changed_Predicted' ,'PB_changed_predicted' ,
              'PB_CHANGED'  ,'Current_PB' ,'Booking_Date_OBS' , 'Key_Cruise_Code'] 
              )) ]

cruises_list = list(df['Key_Cruise_Code'].unique())
Number_of_data_sets = 90
Number_of_cruises_by_set = round(len(cruises_list)/ Number_of_data_sets)
df['data_set_id'] = 0
for data_set_id in range(1,Number_of_data_sets):
    cruises_set = random.sample(cruises_list,  Number_of_cruises_by_set )
    df.loc[df['Key_Cruise_Code'].isin(cruises_set) , 'data_set_id'] = data_set_id
    cruises_list = list( set(cruises_list) - set(cruises_set) )

#Paramètres du modèle de prédiction de changement de PB
params = {    
#Boosting parameters
    'boosting_type': 'dart' , # 'gbdt'
    'objective': 'binary', 
    'metric': 'auc',
    'scale_pos_weight': 45,
#    
    'learning_rate':  0.1,  #0.08
    'num_iterations' : 274   , #312
 
    'max_bin':  243,
    'min_data_in_bin': 12 ,

 #Tree specific Parameters   
    'min_data_in_leaf': 9,
    'max_depth': 10,

    'bagging_freq' :  5,     
    'bagging_fraction' :    0.95, 
    'feature_fraction':   0.67,
    'bagging_seed' : 3,
    
    'reg_alpha' : 0.9,
    'reg_lambda' : 29,
    
    'random_state': 3,
        }
#Cross validation on "Number_of_data_sets" sets
#for data_set_id in list(df.data_set_id.unique()):
#    train = df[ df['data_set_id'] != data_set_id] 
#    lgb_train = lgb.Dataset(
#        train[predictors], 
#        train[target], 
#        feature_name=predictors
#        )
#
#    model = lgb.train( params,  lgb_train , num_boost_round=493)
#
#    df.loc[ df['data_set_id'] == data_set_id, 'PB_changed_predicted'] = model.predict(df.loc[ df['data_set_id'] == data_set_id, predictors] )
#    print('roc_auc = ' ,roc_auc_score(df.loc[ df['data_set_id'] == data_set_id, target] , df.loc[ df['data_set_id'] == data_set_id, 'PB_changed_predicted']))
#    print('f1_score = ' , f1_score(df.loc[ df['data_set_id'] == data_set_id, target] , round(df.loc[ df['data_set_id'] == data_set_id, 'PB_changed_predicted'])))
#    print(confusion_matrix(df.loc[ df['data_set_id'] == data_set_id, target] , round(df.loc[ df['data_set_id'] == data_set_id, 'PB_changed_predicted'])))


#Leave one cruise out
iteration = 0
for cruise in list(df['Key_Cruise_Code'].unique()):
    iteration = iteration +1
    print(iteration, ' ' , cruise)
    train = df[ df['Key_Cruise_Code'] != cruise] 
    lgb_train = lgb.Dataset(
        train[predictors], 
        train[target], 
        feature_name=predictors
        )

    model = lgb.train( params,  lgb_train , num_boost_round=2000)

    df.loc[ df['Key_Cruise_Code'] == cruise, 'PB_changed_predicted'] = model.predict(df.loc[ df['Key_Cruise_Code'] == cruise, predictors] )


#Metrics
print('roc_auc = ' ,roc_auc_score(df[ target] , df[ 'PB_changed_predicted']))
print('f1_score = ' , f1_score(df[ target] , round(df[ 'PB_changed_predicted'])))
print(confusion_matrix(df[ target] , round(df[ 'PB_changed_predicted'])))
df['Round_PB_Changed_Predicted'] = round(df[ 'PB_changed_predicted'])
metric = metric = classTimingAccuracy(df , pb_changed_col= 'PB_CHANGED' , pred_col=  'Round_PB_Changed_Predicted', time_delta = 0)[2]


#****************************
#******Evaluation by column*****
#****************************
df['good_pred'] = 0
df.loc[df['Round_PB_Changed_Predicted'] == df['PB_CHANGED'], 'good_pred'] = 1

list_eval = ['Current_PB',
             'Mapping_Vessel'
              'Mapping_Ship_Class_2',
              'Mapping_Cluster_Pricing_2'
              'Mapping_Year_Departure',
              'Current_Month',
              'FCST_Editor',
              'Current_Year',
              'Mapping_FCST_Model',
              ]

P = 'Mapping_Cluster_Pricing_2'
df_grp = df.groupby(['PB_CHANGED', P]).agg({'good_pred' : 'mean'})
df_grp.plot(kind = 'bar', 
                      figsize = (6,4), 
                      title = "Taux de bonnes prédictions par "+ P + ' et par baisse ou pas de PB')

df_grp[(df_grp.good_pred <0.99)].sort_values('good_pred')


#Test metric
df3 = df[df.Booking_Date_OBS >= '2018-07-19']
for cruise in list(df3.Key_Cruise_Code.unique()):
    metric = avgClassTimingAccuracy(df = df3.loc[(df3.Key_Cruise_Code== cruise)] , label_col= 'Current_PB' , pred_col=  'prediction', time_delta = 0)
    precision = precision_score(df3.loc[df3.Key_Cruise_Code == cruise, target] , round(df3.loc[df3.Key_Cruise_Code == cruise, 'PB_changed_predicted']))
    if ~(math.isnan(metric)) &  (metric != 100* precision):
        print('metric = ', metric , ' and precision = ', precision, ' for ', cruise)