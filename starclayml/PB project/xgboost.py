



param = {
        'objective':    ['reg:linear'], #'reg:linear'   'multi:softprob'
      #  'booster':      ['gbtree', 'gblinear' , 'dart'],
#        'n_estimators': list(range(100,110)),
        'learning_rate': [0.05, 0.1 , 0.15, 0.2],
        'max_depth':     [6, 7, 8],
#        'gamma':         [0 , 1, 2, 5],
#        'subsample':     [ 0.2,  0.4, 0.6,0.8,1],
#        'min_child_weight': range(1,16),
#        'colsample_bytree': [ 0.5, 0.6, 0.7, 0.8, 0.9 ],
#        'reg_lambda' :   [0, 0.5, 0.9, 1.2 , 1.5 ,2],
#        'reg_alpha' :   [0, 0.5, 0.9, 1.2 , 1.5 ,2],
        }
grid_cv = RandomizedSearchCV(estimator = xgboost.XGBRegressor() ,
                             param_distributions  = param , #
                             scoring = 'accuracy',
                             cv = 3,
                             random_state = 10,
                             )
grid_cv.fit(x_train[predictors] , np.ravel(x_train[target]) )
xgb_model = grid_cv.best_estimator_








param = {
        'objective':    ['reg:linear' , 'reg:gamma', 'reg:tweedie'],
        'booster':      ['gbtree', 'gblinear' , 'dart'],
        'n_estimators': list(range(10,15)),
#        'learning_rate': [0.05, 0.1 , 0.15, 0.2],
#        'max_depth':     [6, 7, 8],
#        'gamma':         [0 , 1, 2, 5],
#        'subsample':     [ 0.2,  0.4, 0.6,0.8,1],
#        'min_child_weight': range(1,16),
#        'colsample_bytree': [ 0.5, 0.6, 0.7, 0.8, 0.9 ],
#        'reg_lambda' :   [0, 0.5, 0.9, 1.2 , 1.5 ,2],
#        'reg_alpha' :   [0, 0.5, 0.9, 1.2 , 1.5 ,2],
        }

D_x_train = xgboost.DMatrix(x_train[predictors])

grid_cv = RandomizedSearchCV(estimator = xgboost.XGBRegressor() ,
                             param_distributions  = param , #
                             scoring = 'r2',
                             cv = 3,
                             random_state = 10,
                             )
grid_cv.fit(D_x_train , x_train[target] )
xgb_model = grid_cv.best_estimator_




