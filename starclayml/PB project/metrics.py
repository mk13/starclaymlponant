#Chargement des bibliothèquesplotResultGraph, regTimingAccuracy, avgRegTimingAccuracy
import os
import datetime

import pandas as pd
pd.options.mode.chained_assignment = None
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

from pathlib import Path

# Permet de trouver l'enregistrement le plus proche d'une date donnée.
# Utilisé dans la métrique pour trouver tous les enregistrements compris dans
# un interval donné.
def getClosestDateIdx(df, column, date):
    date = pd.to_datetime(date, format="%Y-%m-%d")
    closest_date =  min(df[column], key=lambda x: abs(x - date))
    return df[df[column]==closest_date].index[0]


# Permet de tracer un graph avec les changements de PB ainsi que les prédictions
# Cette fonction fonctionne avec les résultats d'un modèle de régression
def plotResultGraph(results_df, prediction="prediction", pred_proba="PB_changed_predicted", label="label"):
    counter = 0
    for cruise_code, df_cruise in results_df.groupby(results_df["Key_Cruise_Code"]):
        counter +=1
        if counter > 10:
            break

        plt.figure(figsize=(20, 10))
        plt.title(cruise_code)
        # Valeur prédite du PB
        plt.plot(df_cruise[prediction], "b-", label=prediction)
        # Valeur du PB
        plt.plot(df_cruise[label], "--",color="darkorange", label="Current_PB")

        if pred_proba in df_cruise.columns:
            # Affiche la probabilité de changement à chaque instant
            plt.plot(df_cruise[pred_proba]/10, "-",color="deepskyblue", label="Probabilité de changement")

        # Les mauvaises prédictions seront représentées avec un carré rouge
        plt.plot(df_cruise[prediction][df_cruise["graph"]==1], "rs", markersize=15, label="Erreur")
        # Les bonnes prédictions seront représentées avec un triangle vert
        plt.plot(df_cruise[prediction][df_cruise["graph"]==0], "g^", markersize=15, label="Succès")
        plt.legend(loc="upper right", fontsize=14)
        plt.show()


# Permet de détecter les changements de PB:
# 1: baisse du PB
# -1: augmentation du PB
def detectChange(df, column):
    return 1 * (df[column].shift(1) < df[column]) \
            -1 * (df[column].shift(1) > df[column])

# Métrique pour le modèle de régression se basant sur le temps.
# Le paramètre accept_delay  détermine si les prédictions
# en retard sont acceptées ou non (par défaut ce n'est pas le cas, sa valeur
# est donc False
# Valeurs de retour:
# scores_by_cruise: le score par croisières, si il n'y a pas de changements de PB
# lors d'une croisière alors le score pour cettre croisière sera np.NaN
# results_df: la dataframe des données à laquelle on a rajouté les colonnes:
# - "pred_changed"
# - "label_changed"
# - "graph"
# tot_score: score total (sans considérer les croisières individuellement):
# nombre total de changements considérés comme corrects sur le nombre total de
# changement prédits.

def regTimingAccuracy(df, label_col = "label",
                      pred_col = "prediction",
                      time_delta=0,
                      accept_delay=False):

    df = df.sort_values(['Key_Cruise_Code' , "Booking_Date_OBS"])
    df.reset_index(drop=True, inplace=True)
    tot_good_preds = 0
    tot_changes = 0

    df["Booking_Date_OBS"] = pd.to_datetime(df["Booking_Date_OBS"], format="%Y-%m-%d")

    scores = []
    updated_cruise_df = []

    for cruise_code, df_cruise in df.groupby(df["Key_Cruise_Code"]):
        # La colonne graphe est remplie de -1.
        # Pour chaque enregistrement si le modèle à prédit un bon changement
        # dans l'interval d'acceptance "graph" prendra la valeur 0 sinon
        # il prendra la valeur 1.
        df_cruise["graph"] = -1.0

        df_cruise["pred_change"] = detectChange(df_cruise, pred_col)

        df_cruise["label_change"] = detectChange(df_cruise, label_col)


        for index, row in df_cruise.iterrows():

            if row["label_change"] == 0:
                continue

            if row["label_change"] == row["pred_change"]:
                df_cruise.loc[index, "graph"] = 0
                continue

            closest_date_idx = getClosestDateIdx(df_cruise, "Booking_Date_OBS", row["Booking_Date_OBS"] - pd.Timedelta(weeks=time_delta))
            delta_changes = df_cruise["pred_change"].loc[closest_date_idx:index]

            change = delta_changes[delta_changes != 0].tail(1)

            if change.count() > 0:
                 if int(change) == row["label_change"]:
                     df_cruise.loc[change.index, "graph"] = 0.0
                     continue

            closest_date_idx = getClosestDateIdx(df_cruise, "Booking_Date_OBS", row["Booking_Date_OBS"] + pd.Timedelta(weeks=time_delta))
            delta_changes = df_cruise["pred_change"].loc[index:closest_date_idx]

            change = delta_changes[delta_changes != 0].head(1)

            if change.count() > 0:
                if int(change) == row["label_change"] and accept_delay:
                    df_cruise.loc[change.index, "graph"] = 0.0
                    continue
                else:
                    df_cruise.loc[change.index, "graph"] = 1.0
                    continue

            df_cruise.loc[index, "graph"] = 1

        df_cruise["graph"][(df_cruise["graph"] == -1) & (df_cruise["pred_change"])] = 1


        changes_count = df_cruise["graph"][df_cruise["graph"] != -1].count()
        good_preds = df_cruise["graph"][df_cruise["graph"] == 0].count()


        tot_good_preds += good_preds
        tot_changes += changes_count

        if changes_count == 0:
            score = np.Nan
        else:
            score = (good_preds/changes_count)*100

        scores.append([cruise_code, score])
        updated_cruise_df.append(df_cruise)

    scores_by_cruise = pd.DataFrame(scores, columns=["Key_Cruise_Code", "score"]).sort_values(by=["score"])
    results_df = pd.concat(updated_cruise_df)
    results_df.reset_index(drop=True, inplace=True)

    if tot_changes == 0:
        tot_score = np.NaN
    else:
        tot_score = (tot_good_preds/tot_changes) * 100

    # print(f'Tot changes: {tot_changes}')
    # print(f'Tot good preds: {tot_good_preds}')

    return scores_by_cruise, results_df, tot_score

# Métrique pour le modèle de classification se basant sur le temps.
# Le paramètre accept_delay  détermine si les prédictions en retard sont acceptées
# ou non (par défaut ce n'est pas le cas, sa valeur est donc False
# Valeurs de retour:
# scores_by_cruise: le score par croisières, si il n'y a pas de changements de PB
# lors d'une croisière alors le score pour cettre croisière sera np.NaN
# results_df: la dataframe des données à laquelle on a rajouté les colonnes:
# - "pred_changed"
# - "label_changed"
# - "graph"
# tot_score: score total (sans considérer les croisières individuellement):
# nombre total de changements considérés comme corrects sur le nombre total de
# changement prédits.
# ATTENTION: cette fonction prends en paramètre "PB_CHANGED"


def classTimingAccuracy(df, pb_changed_col = "PB_CHANGED",
                      pred_col = "prediction",
                      time_delta=0,
                      accept_delay=False):

    df = df.sort_values(['Key_Cruise_Code' , "Booking_Date_OBS"])
    df.reset_index(drop=True, inplace=True)

    df["Booking_Date_OBS"] = pd.to_datetime(df["Booking_Date_OBS"], format="%Y-%m-%d")

    scores = []

    updated_cruise_df = []

    tot_good_preds = 0
    tot_changes = 0

    for cruise_code, df_cruise in df.groupby(df["Key_Cruise_Code"]):
        # La colonne graphe est remplie de -1.
        # Pour chaque enregistrement si le modèle à prédit un bon changement
        # dans l'interval d'acceptance "graph" prendra la valeur 0 sinon
        # il prendra la valeur 1.
        df_cruise["graph"] = -1.0

        df_cruise["pred_change"] = df_cruise[pred_col]

        df_cruise["label_change"] = df_cruise[pb_changed_col]

        for index, row in df_cruise.iterrows():

            if row["label_change"] == 0:
                continue

            # if row["label_change"] == row["pred_change"]:
            #     df_cruise.loc[index, "graph"] = 0
            #     continue

            closest_date_idx = getClosestDateIdx(df_cruise, "Booking_Date_OBS", row["Booking_Date_OBS"] - pd.Timedelta(weeks=time_delta))
            delta_changes = df_cruise["pred_change"].loc[closest_date_idx:index]

            start_change_idx = delta_changes[delta_changes == 0].tail(1).index+1
            end_change_idx = delta_changes[delta_changes != 0].tail(1).index

            if len(end_change_idx) != 0 and \
            (len(start_change_idx) == 0 or end_change_idx >= start_change_idx):
                if len(start_change_idx) > 0:
                    df_cruise.loc[start_change_idx[0]:end_change_idx[0], "graph"] = 0.0
                else:
                    df_cruise.loc[delta_changes.head(1).index[0]:end_change_idx[0], "graph"] = 0.0

                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1
                continue
            else:
                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1

            closest_date_idx = getClosestDateIdx(df_cruise, "Booking_Date_OBS", row["Booking_Date_OBS"] + pd.Timedelta(weeks=time_delta))
            delta_changes = df_cruise["pred_change"].loc[index:closest_date_idx]

            start_change_idx = delta_changes[delta_changes == 0].tail(1).index+1
            end_change_idx = delta_changes[delta_changes != 0].tail(1).index

            if accept_delay and len(end_change_idx) != 0 and \
            (len(start_change_idx) == 0 or end_change_idx >= start_change_idx):

                if len(start_change_idx) > 0:
                    df_cruise.loc[start_change_idx[0], end_change_idx[0], "graph"] = 0.0
                else:
                    df_cruise.loc[delta_changes.head(1).index[0], end_change_idx[0], "graph"] = 0.0

                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1
                continue
            else:
                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1
                continue


        df_cruise["graph"][(df_cruise["graph"] == -1) & (df_cruise["pred_change"])] = 1

        changes_count = df_cruise["graph"][df_cruise["graph"] != -1].count()
        good_preds = df_cruise["graph"][df_cruise["graph"] == 0].count()

        tot_good_preds += good_preds
        tot_changes += changes_count

        # print(f'Change counts: {changes_count}')
        # print(f'Good preds: {good_preds}')

        if changes_count == 0:
            score = np.NaN
        else:
            score = (good_preds/changes_count)*100

        scores.append([cruise_code, score])
        updated_cruise_df.append(df_cruise)

    scores_by_cruise = pd.DataFrame(scores, columns=["Key_Cruise_Code", "score"]).sort_values(by=["score"])
    results_df = pd.concat(updated_cruise_df)
    results_df.reset_index(drop=True, inplace=True)

    if tot_changes == 0:
        tot_score = np.NaN
    else:
        tot_score = (tot_good_preds/tot_changes) * 100

    # print(f'Tot changes: {tot_changes}')
    # print(f'Tot good preds: {tot_good_preds}')

    return scores_by_cruise, results_df, tot_score



# Calcul la différence de remplissage entre deux enregistrements consécutifs
# et ce pour tous les enregistrements.
# Les valeurs calculées seront utiliées pour la métrique se basant le remplissage
# afin de déterminer si un changement prédit est correct ou non.
def diffCabinPayingIndiv(df, column):
    diff_cabin_paying_indiv = df[column] - df[column].shift(1)
    diff_cabin_paying_indiv.iloc[0]=0
    return diff_cabin_paying_indiv


# Métrique pour le modèle de régression se basant sur le taux de remplissage.
# Le paramètre accept_delay  détermine si les prédictions en retard sont acceptées
# ou non (par défaut ce n'est pas le cas, sa valeur est donc False
# Valeurs de retour:
# scores_by_cruise: le score par croisières, si il n'y a pas de changements de PB
# lors d'une croisière alors le score pour cettre croisière sera np.NaN
# results_df: la dataframe des données à laquelle on a rajouté les colonnes:
# - "pred_changed"
# - "label_changed"
# - "graph"
# tot_score: score total (sans considérer les croisières individuellement):
# nombre total de changements considérés comme corrects sur le nombre total de
# changement prédits.
# ATTENTION: cette fonction prends en paramètre "PB_CHANGED"

def regTotCabinIndivAccuracy(df, label_col = "label",
                             pred_col = "prediction",
                             tot_cabin_delta=0,
                             accept_delay=False):


    df = df.sort_values(['Key_Cruise_Code' , "Booking_Date_OBS"])
    df.reset_index(drop=True, inplace=True)

    scores = []
    updated_cruise_df = []

    for cruise_code, df_cruise in df.groupby(df["Key_Cruise_Code"]):

        # La colonne graphe est remplie de -1.
        # Pour chaque enregistrement si le modèle à prédit un bon changement
        # dans l'interval d'acceptance "graph" prendra la valeur 0 sinon
        # il prendra la valeur 1.
        df_cruise["graph"] = -1

        df_cruise["pred_change"] = detectChange(df_cruise, pred_col)

        df_cruise["label_change"] = detectChange(df_cruise, label_col)

        df_cruise["diff_cabin_paying_indiv"] = diffCabinPayingIndiv(df_cruise,
                                                                    "Total_Cabin_Paying_Indiv")

        bad_preds = 0

        for index, row in df_cruise.iterrows():
            # print(index)
            if row["label_change"] == 0:
                continue

            if row["label_change"] == row["pred_change"]:
                df_cruise.loc[index, "graph"] = 0
                continue

            df_cruise_early = df_cruise.loc[:index]

            sum = df_cruise_early.loc[::-1, "diff_cabin_paying_indiv"].cumsum()[::-1].shift(-1)
            delta_changes = df_cruise_early["pred_change"][sum <= tot_cabin_delta]
            last_change = delta_changes[delta_changes != 0].tail(1)

            if last_change.count() > 0:
                if int(last_change) == row["label_change"]:
                    df_cruise.loc[last_change.index, "graph"] = 0
                    continue
                else:
                    df_cruise.loc[last_change.index, "graph"] = 1

            df_cruise_late = df_cruise.loc[index:]

            delta_changes = df_cruise_late["pred_change"][df_cruise_late["diff_cabin_paying_indiv"].cumsum() - row["diff_cabin_paying_indiv"] <= tot_cabin_delta]

            first_change = delta_changes[delta_changes != 0].head(1)

            if first_change.count() > 0:
                if int(first_change) == row["label_change"] and accept_delay:
                    df_cruise.loc[first_change.index, "graph"] = 0
                    continue
                else:
                    df_cruise.loc[first_change.index, "graph"] = 1

            df_cruise.loc[index, "graph"] = 1

        df_cruise["graph"][(df_cruise["graph"] == -1) & (df_cruise["pred_change"])] = 1

        changes_count = df_cruise["graph"][df_cruise["graph"] != -1].count()
        bad_preds = df_cruise["graph"][df_cruise["graph"] == 1].sum()

        if changes_count == 0:
            score = 100.0
        else:
            score = ((changes_count-bad_preds)/changes_count)*100

        scores.append([cruise_code, score])
        updated_cruise_df.append(df_cruise)


    scores = pd.DataFrame(scores, columns=["Key_Cruise_Code", "score"]).sort_values(by=["score"])

    results_df = pd.concat(updated_cruise_df)

    results_df.reset_index(drop=True, inplace=True)

    return scores, results_df


# Métrique pour le modèle de classification se basant sur le taux de remplissage.
# Le paramètre accept_delay  détermine si les prédictions en retard sont acceptées
# ou non (par défaut ce n'est pas le cas, sa valeur est donc False
# Valeurs de retour:
# scores_by_cruise: le score par croisières, si il n'y a pas de changements de PB
# lors d'une croisière alors le score pour cettre croisière sera np.NaN
# results_df: la dataframe des données à laquelle on a rajouté les colonnes:
# - "pred_changed"
# - "label_changed"
# - "graph"
# tot_score: score total (sans considérer les croisières individuellement):
# nombre total de changements considérés comme corrects sur le nombre total de
# changement prédits.
# ATTENTION: cette fonction prends en paramètre "PB_CHANGED"

def classTotCabinIndivAccuracy(df, pb_changed_col = "PB_CHANGED",
                             pred_col = "prediction",
                             tot_cabin_delta=0, accept_delay=False):

    df = df.sort_values(['Key_Cruise_Code' , "Booking_Date_OBS"])
    df.reset_index(drop=True, inplace=True)

    tot_good_preds = 0
    tot_changes = 0

    scores = []
    updated_cruise_df = []

    for cruise_code, df_cruise in df.groupby(df["Key_Cruise_Code"]):
        # La colonne graphe est remplie de -1.
        # Pour chaque enregistrement si le modèle à prédit un bon changement
        # dans l'interval d'acceptance "graph" prendra la valeur 0 sinon
        # il prendra la valeur 1.
        df_cruise["graph"] = -1.0

        df_cruise["pred_change"] = df_cruise[pred_col]

        df_cruise["label_change"] = df_cruise[pb_changed_col]

        df_cruise["diff_cabin_paying_indiv"] = diffCabinPayingIndiv(df_cruise,
                                                                    "Total_Cabin_Paying_Indiv")

        bad_preds = 0

        for index, row in df_cruise.iterrows():
            # print(index)
            if row["label_change"] == 0:
                continue

            df_cruise_early = df_cruise.loc[:index]

            sum = df_cruise_early.loc[::-1, "diff_cabin_paying_indiv"].cumsum()[::-1].shift(-1)
            sum.fillna(0, inplace=True)
            delta_changes = df_cruise_early["pred_change"][sum <= tot_cabin_delta]

            start_change_idx = delta_changes[delta_changes == 0].tail(1).index+1
            end_change_idx = delta_changes[delta_changes != 0].tail(1).index

            if len(end_change_idx) != 0 and \
            (len(start_change_idx) == 0 or end_change_idx >= start_change_idx):
                if len(start_change_idx) > 0:
                    df_cruise.loc[start_change_idx[0]:end_change_idx[0], "graph"] = 0.0
                else:
                    df_cruise.loc[delta_changes.head(1).index[0]:end_change_idx[0], "graph"] = 0.0

                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1
                continue
            else:
                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1

            df_cruise_late = df_cruise.loc[index:]

            delta_changes = df_cruise_late["pred_change"][df_cruise_late["diff_cabin_paying_indiv"].cumsum() - row["diff_cabin_paying_indiv"] <= tot_cabin_delta]

            start_change_idx = delta_changes[delta_changes != 0].head(1).index
            end_change_idx = delta_changes[delta_changes == 0].head(1).index

            if accept_delay and len(end_change_idx) != 0 and \
            (len(start_change_idx) == 0 or end_change_idx >= start_change_idx):
                if len(start_change_idx) > 0:
                    df_cruise.loc[start_change_idx[0]:end_change_idx[0], "graph"] = 0.0
                else:
                    df_cruise.loc[delta_changes.head(1).index[0]:end_change_idx[0], "graph"] = 0.0

                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1
                continue
            else:
                df_cruise.loc[delta_changes.index, "graph"][df_cruise.loc[delta_changes.index, "graph"] != 0] = 1




        df_cruise["graph"][(df_cruise["graph"] == -1) & (df_cruise["pred_change"])] = 1

        changes_count = df_cruise["graph"][df_cruise["graph"] != -1].count()
        good_preds = df_cruise["graph"][df_cruise["graph"] == 0].count()

        tot_good_preds += good_preds
        tot_changes += changes_count

        # print(f'Change counts: {changes_count}')
        # print(f'Good preds: {good_preds}')

        if changes_count == 0:
            score = np.NaN
        else:
            score = (good_preds/changes_count)*100

        scores.append([cruise_code, score])
        updated_cruise_df.append(df_cruise)

    scores_by_cruise = pd.DataFrame(scores, columns=["Key_Cruise_Code", "score"]).sort_values(by=["score"])
    results_df = pd.concat(updated_cruise_df)
    results_df.reset_index(drop=True, inplace=True)

    if tot_changes == 0:
        tot_score = np.NaN
    else:
        tot_score = (tot_good_preds/tot_changes) * 100

    # print(f'Tot changes: {tot_changes}')
    # print(f'Tot good preds: {tot_good_preds}')

    return scores_by_cruise, results_df, tot_score

def main():
    pass

if __name__ == "__main__":
   main()
