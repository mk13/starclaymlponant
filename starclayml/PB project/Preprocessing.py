import pandas as pd
import numpy as np
import re
import os
import glob

#os.chdir('/home/allan/Documents/PONANT/starclayml/PB project')
from get_city_coordinate import get_city_coordinates
from utils import get_number_changes_between_records , get_Last_FCST_PB_Column , get_Last_Total_Cab_PB_Column


def read_input_data(data_input_link):
    filenames = glob.glob(data_input_link + "/DATA"+"*.xlsx")
    dfs_list = []
    for filename in filenames:
        dfs_list.append(pd.read_excel(filename))
    df = pd.concat(dfs_list, ignore_index=True)
    df = df.drop_duplicates(subset = ['Key_Cruise_Code' , 'Booking_Date_OBS'])
    del dfs_list
    return df


def correct_format(df):
    #Format datetime variables
    df['FCST_DATE_CREATION'] = pd.to_datetime(df['FCST_DATE_CREATION'])
    df['Date_Premiere_Reservation_Indiv'] = pd.to_datetime(df['Date_Premiere_Reservation_Indiv'])
    df['Option_Date_Premiere_Option_Indiv'] = pd.to_datetime(df['Option_Date_Premiere_Option_Indiv'])
    df['PB_Changed_Date'] = pd.to_datetime(df['PB_Changed_Date'])
    df['Booking_Date_OBS'] = pd.to_datetime(df['Booking_Date_OBS'])
    df['Mapping_Date_Departure'] = pd.to_datetime(df['Mapping_Date_Departure'])
    return df



def select_data(df):
    #**************************
    #Lines Selection
    #************************ 
    
    #Remove lines with Current_PB NULL
    df = df[~df['FCST_TOTAL_CABIN'].isna()]
    print(df.shape)
    
    #Remove Positioning cruises
    df = df[df['Mapping_Destination'] != 'Positioning' ]
    
    #Remove group cruises
    df = df[~((df['Mapping_Vessel'] == 'PONANT')  & 
       (df['Total_Cabin_Total_Group'] / df['Mapping_Cabin_Capacity'] >= 0.7)) ]
    df = df[~((df['Mapping_Vessel'] != 'PONANT')  & 
       (df['Total_Cabin_Total_Group'] / df['Mapping_Cabin_Capacity'] >= 0.94)) ]

    
    #Remove list of cruises
    cruises_to_remove = ['P250317',#vendue aux groupes mais en Pax indiv
                         
                         'P180120', #Pas ouverte à la vente
                         
                         'P180220', #7 croisières tout le temps à PB 0 pour travaux de rénovation
                         'P250202',
                         'P030320',
                         'P100320',
                         'P170320',
                         'P240320',
                         'P310320']
    df = df[~df['Key_Cruise_Code'].isin(cruises_to_remove)]
    
    
    #Tri des donnÃ©es par Code croisiÃ¨re et date
    df = df.sort_values(['Key_Cruise_Code' , 'Booking_Date_OBS'])
    return df






def get_previous_PB_and_delay_changed_days_shift_changes_to_previous(df):    
    df.reset_index(inplace = True, drop = True)
    first_lines_cruises = list(df[['Key_Cruise_Code']].reset_index().groupby('Key_Cruise_Code').first()['index'].values)
    last_lines_cruises = list(df[['Key_Cruise_Code']].reset_index().groupby('Key_Cruise_Code').last()['index'].values)

    df['next_PB_value'] =    df.loc[:,'Current_PB'].shift(-1)
    df['next_PB_date'] =    df.loc[:,'Booking_Date_OBS'].shift(-1)
    df['next_PB_Changed_date'] =    df.loc[:,'PB_Changed_Date'].shift(-1)


    df['previous_SupSingle'] =    df.loc[:,'Current_SupSingle'].shift(1)

    df.loc[last_lines_cruises , 'next_PB_value'] =  df.loc[last_lines_cruises, 'Current_PB']
    df.loc[last_lines_cruises , 'next_PB_date'] =  df.loc[last_lines_cruises, 'Booking_Date_OBS']
    df.loc[last_lines_cruises , 'next_PB_Changed_date'] =  df.loc[last_lines_cruises, 'PB_Changed_Date']

 #Décaler les changements de PB au lundi d'avant (pour les changements hors lundis et pour les enregistrements décalés de plus d'un jour)
    df.loc[ ((df['next_PB_date']- df['Booking_Date_OBS']).dt.days > 1) &
            (df['next_PB_date'] != df['next_PB_Changed_date'] ) &
            (df['next_PB_value'] != df['Current_PB'] ) , 'Current_PB'] = df.loc[        
            ((df['next_PB_date']- df['Booking_Date_OBS']).dt.days > 1) &
            (df['next_PB_date'] != df['next_PB_Changed_date'] ) &
            (df['next_PB_value'] != df['Current_PB'] )  , 'next_PB_value'] 
#Recalculer le PB de la veille
    df['previous_PB_value'] =    df.loc[:,'Current_PB'].shift(1)
    
#ADDED
    
    #df['Delay_PB_Changed_Days'] = df.loc[:,'Delay_PB_Changed_Days'].shift(1)
    #df['Delay_PB_Changed_Days'] = df['Delay_PB_Changed_Days'].fillna(0)
    
    df.loc[first_lines_cruises , 'previous_PB_value'] =  df.loc[first_lines_cruises, 'Current_PB']
    df.loc[first_lines_cruises , 'previous_SupSingle'] =  df.loc[first_lines_cruises, 'Current_SupSingle']
    
    df['PB_CHANGED'] = 0
    df.loc[df['previous_PB_value'] != df['Current_PB'] , 'PB_CHANGED' ] = 1
    df.drop( ['next_PB_value' , 'next_PB_date', 'next_PB_Changed_date'], axis = 1, inplace = True )
# Correction Delay_PB_Changed_Date    
    index_PB_Changed = list(df.loc[df['PB_CHANGED'] == 1].index)
    index_PB_Changed = [ind for ind in index_PB_Changed if (ind not in first_lines_cruises)]
    df['previous_PB_Changed_Date'] =  df.loc[:,'PB_Changed_Date'].shift(1)
    df.loc[first_lines_cruises , 'previous_PB_Changed_Date'] =  df.loc[first_lines_cruises,'Booking_Date_OBS']
    df.loc[index_PB_Changed, 'Delay_PB_Changed_Days'] = (df.loc[index_PB_Changed , 'Booking_Date_OBS'] - df.loc[index_PB_Changed , 'previous_PB_Changed_Date']).dt.days 
    
    return df




def get_previous_PB_and_delay_changed_days_shift_changes_to_next(df , PBSSCALENDAR_link):    
    #********************************
    #Creturn df with Previous PB value and days number between last change
    #********************************
    #Lecture de la table PBSSCALENDAR
    df_PB = pd.read_excel(PBSSCALENDAR_link)
    #Les variables clairement inutiles Ã  la problÃ¨matique sont Ã  supprimer
    df_PB.drop([ 'ID', 
                 'UID', 
                 'Editor', 
                 'Creation_Timestamp', 
                 'Edition_Timestamp',
                 'PBSS_LT_Week',
                 'ND_CAL_Amount'] , axis = 1 ,inplace = True)
    df_PB['PBSS_CAL_Date'] = pd.to_datetime(df_PB['PBSS_CAL_Date'])
    df_PB.columns = ['Cruise_Code' , 'date', 'PB', 'SupSingle']
    df_PB.loc[df_PB['PB'] == 0.5 , 'PB'] = 0.05
    
    df.reset_index(inplace = True, drop = True)
    first_lines_cruises = list(df[['Key_Cruise_Code']].reset_index().groupby('Key_Cruise_Code').first()['index'].values)
    df['previous_PB_value'] =    df.loc[:,'Current_PB'].shift(1)
    df['previous_SupSingle'] =    df.loc[:,'Current_SupSingle'].shift(1)
    df.loc[first_lines_cruises , 'previous_PB_value'] =  df['Current_PB']
    df.loc[first_lines_cruises , 'previous_SupSingle'] =  df['Current_SupSingle']
    df['PB_CHANGED'] = 0
    df.loc[df['previous_PB_value'] != df['Current_PB'] , 'PB_CHANGED' ] = 1
    
    index_PB_Changed = list(df.loc[df['PB_CHANGED'] == 1].index)
    index_PB_Changed = [ind for ind in index_PB_Changed if (ind not in first_lines_cruises)]
    df['previous_PB_Changed_Date'] =  df.loc[:,'PB_Changed_Date'].shift(1)
    df.loc[first_lines_cruises , 'previous_PB_Changed_Date'] =  df.loc[first_lines_cruises,'Booking_Date_OBS']
    df.loc[index_PB_Changed, 'Delay_PB_Changed_Days'] = (df.loc[index_PB_Changed , 'Booking_Date_OBS'] - df.loc[index_PB_Changed , 'previous_PB_Changed_Date']).dt.days 
    #get_number_changes_between_records(date, previous_date , cruise  ,df_PB)
    for INDEX in index_PB_Changed:
        tmp = get_number_changes_between_records(date = df.loc[INDEX , 'Booking_Date_OBS'] , 
                                                 previous_date = df.loc[:,'Booking_Date_OBS'].shift(1)[INDEX] , 
                                                 cruise = df.loc[INDEX , 'Key_Cruise_Code']  ,
                                                 PB_changed_date = df.loc[INDEX , 'PB_Changed_Date' ],
                                                 df_PB = df_PB)
        if tmp[0] > 2:
            df.loc[INDEX , 'Delay_PB_Changed_Days'] = tmp[1]
            df.loc[INDEX , 'previous_PB_value'] = tmp[2]
            df.loc[INDEX , 'previous_SupSingle'] = tmp[3]
    #df = df.drop('NB_changes' , axis = 1)
    return df



def fill_missing_values(df):
    #********************************
    #Missing Values
    #********************************
    #Continious Variables
    #Remove lines with Current_PB NULL
    print()
    df.loc[df['Current_PB'] == 0.5 , 'Current_PB'] = 0.05
    
    df.loc[df['Current_PB'].isna() , 'Current_PB'] = 0.3
    df.loc[df['Moyenne_PB'].isna(), 'Moyenne_PB'] = df.loc[df['Moyenne_PB'].isna(), 'Current_PB'] * -1 
    #
    df['FCST_MODELE_PU_DEFORMATION'] = df['FCST_MODELE_PU_DEFORMATION'].fillna(1)
    
    #Remplacer les valeurs manquantes en 0 pour certaines valeurs manquantes
    nan_to_zeros_var = list(df.dtypes[(df.dtypes != 'object' ) & (df.dtypes !='datetime64[ns]')].index)
    df[nan_to_zeros_var] = df[nan_to_zeros_var].fillna(0)
    return df

def transform_categorical_variables_values(df):
    #Mapping season to [Winter/Summer]
    df['Mapping_Season'] = df['Mapping_Season'].apply(lambda x:''.join([c for c in x if c.isalpha()]))
    Themes_dic = {  'Gastronomy': 'Gastronomy',
                    'Food & Wine' : 'Gastronomy',
                    'Wine Tasting' : 'Gastronomy',
                    "Gastronomy - Kid's Club" : 'Gastronomy',
                    'QC - Food & Wine': 'Gastronomy',
                     
                    'Loyalty' : 'Loyalty',
                    'Chairman Cruise' : 'Loyalty',
                    'AUS - Chairman Cruise' : 'Loyalty',
                    
                    'Classical Music' : 'Classical_Music',
                    'QC - Music': 'Classical_Music',
                    }
    
    df['Theme'] = df['Mapping_Theme'].apply(lambda x: np.nan if x not in Themes_dic.keys() else Themes_dic[x])
    df.loc[(~df['Mapping_Theme'].isna()) & (df['Theme'].isna()) , 'Theme'] = 'Other_Theme' 
    df.loc[df['Theme'].isna() , 'Theme'] = 'No_Theme'
    df = df.drop('Mapping_Theme' , axis = 1)
    
    df.loc[~df['Mapping_Guest'].isna() , 'Mapping_Guest'] = 'With_Guest'
    df.loc[df['Mapping_Guest'].isna() , 'Mapping_Guest'] = 'No_Guest'
    
    df.loc[~df['Mapping_Partnership'].isna() , 'Mapping_Partnership'] = 'With_Partnership'
    df.loc[df['Mapping_Partnership'].isna() , 'Mapping_Partnership'] = 'No_Partnership'
    
    df['Mapping_Ship_Class'] = df['Mapping_Ship_Class'].str.upper()
    return df

def add_FROM_TO_latitude_longitude(df):
    #Coordonnées FROM  -  TO
    df['Mapping_From_To'] = df['Mapping_From_To'].str.replace(' ', '')
    df['Mapping_From_To'] = df['Mapping_From_To'].str.replace('Fort-de-France/PontaDelgada', 'Fort de France-PontaDelgada')
    df['FROM'] = df['Mapping_From_To'].str.split('-').apply(lambda x: x[0])
    df['FROM'] = df['FROM'].apply(lambda x: re.sub(r"(\w)([A-Z])", r"\1 \2", x))
    df['TO'] = df['Mapping_From_To'].str.split('-').apply(lambda x: x[1])
    df['TO'] = df['TO'].apply(lambda x: re.sub(r"(\w)([A-Z])", r"\1 \2", x))
    
    df['FROM'] = df['FROM'].str.replace('de ', ' de ')
    df['TO'] = df['TO'].str.replace('de ', ' de ')
    
    df['FROM'] = df['FROM'].str.replace('à ', ' à ')
    df['TO'] = df['TO'].str.replace('à ', ' à ')
    
    cities = list(df['FROM'].unique()) + list(df['TO'].unique() )
    latitude = []
    longitude = []
    
    for city in cities:
        lat, long = get_city_coordinates(city)
        latitude.append(lat)
        longitude.append(long)
    
    cities = pd.DataFrame({
            'city': cities,
            'latitude_FROM' : latitude,
            'longitude_FROM': longitude})    
    cities = cities.drop_duplicates(subset = ['city'])
    df = pd.merge(left = df,
                  right = cities,
                  left_on = 'FROM',
                  right_on = 'city',
                  how = 'left')
    
    df.drop(['city'] , axis = 1, inplace = True)
    cities.columns = ['city' , 'latitude_TO', 'longitude_TO']
    
    df = pd.merge(left = df,
                  right = cities,
                  left_on = 'TO',
                  right_on = 'city',
                  how = 'left')
    
    df.drop(['city'] , axis = 1, inplace = True)
    return df


def add_calendar_variables(df):
    #Durée depuis l'ouverture de vente
    df['min_premiere_reservation_option'] = df['Option_Date_Premiere_Option_Indiv'].copy()
    df.loc[df['min_premiere_reservation_option'].isna() , 'min_premiere_reservation_option' ] = df.loc[df['min_premiere_reservation_option'].isna() , 'Date_Premiere_Reservation_Indiv']
    df.loc[df['min_premiere_reservation_option'] > df['Date_Premiere_Reservation_Indiv'] , 'min_premiere_reservation_option' ] = df.loc[df['min_premiere_reservation_option'] > df['Date_Premiere_Reservation_Indiv'], 'Date_Premiere_Reservation_Indiv']
    df['duration_since_first_reservation']  = (df['Booking_Date_OBS']  - df['min_premiere_reservation_option']).dt.days
    #Departure Month
    df['Departure_Month'] = df['Mapping_Date_Departure'].dt.month
    df['Current_Month'] =   df['Booking_Date_OBS'].dt.month
    df['Current_Year'] = df['Booking_Date_OBS'].dt.year
    return df

def add_FCST_total_cabin_by_PB_level_variables(df):
    #Différence entre le FCST et les réservations
    df['Ecart_PB_30'] = df['FCST_PB30']  - df['Total_Cab_PB_30'] 
    df['Ecart_PB_25'] = df['FCST_PB25']  - df['Total_Cab_PB_25'] 
    df['Ecart_PB_20'] = df['FCST_PB20']  - df['Total_Cab_PB_20']
    df['Ecart_PB_15'] = df['FCST_PB15']  - df['Total_Cab_PB_15']
    df['Ecart_PB_05'] = df['FCST_PB5']  - df['Total_Cab_PB_5']
    df['Ecart_PB_0'] = df['FCST_PB0']  - df['Total_Cab_PB_0']
    
    df['FCST_Last_PB'] = 9999
    df['Total_Cab_Last_PB'] = 9999
    for PB_level in list(df['previous_PB_value'].unique()):
        Last_Total_Cab_PB_Column = get_Last_Total_Cab_PB_Column(PB_level)
        df.loc[df['previous_PB_value'] == PB_level , 'Total_Cab_Last_PB'] = df.loc[df['previous_PB_value'] == PB_level , Last_Total_Cab_PB_Column].values
        Last_FCST_PB_Column = get_Last_FCST_PB_Column(PB_level)
        df.loc[df['previous_PB_value'] == PB_level , 'FCST_Last_PB'] = df.loc[df['previous_PB_value'] == PB_level , Last_FCST_PB_Column].values
    df['Ecart_Last_PB'] = df['FCST_Last_PB'] - df['Total_Cab_Last_PB']
    return df


def categorical_to_dummies(df):
    #datetime_variables = list(df.dtypes[df.dtypes == 'datetime64[ns]'].index)
    df = pd.concat([
            df,
            pd.get_dummies(df['Mapping_Season']),
            pd.get_dummies(df['Mapping_FCST_Model'], prefix = 'Model'),
            pd.get_dummies(df['Mapping_Destination'] , prefix = 'Destination'),
            pd.get_dummies(df['Mapping_Ship_Class'] , prefix = 'Ship_class'),
            pd.get_dummies(df['Mapping_Guest'] , prefix = 'Guest'),
            pd.get_dummies(df['Mapping_Partnership'] , prefix = 'Partnership'),
            pd.get_dummies(df['Theme'] , prefix = 'Theme')
            ], axis = 1)
    #remove spaces from the fitures names to avoid conversions by the model later
    df.columns = [name.replace(' ', '_' ) for name in list(df.columns)]

    return df

def drop_unuseful_variables(df):
    #categorical_variables = [var for var in list(df.dtypes[df.dtypes == 'object'].index) if (var not in ['Key_Cruise_Code']) ]
    datetime_variables = [var for var in list(df.dtypes[df.dtypes == 'datetime64[ns]'].index) if (var not in ['Booking_Date_OBS'])]
    df.drop(datetime_variables , axis = 1, inplace = True)    
  #  df.drop(categorical_variables , axis = 1, inplace = True)    
    variables_to_remove =   [
                             'Current_SupSingle',
                             'Delay_PB_Changed_Days_MIN',
                             'ID',
                             'Total_PAX_Paying_Group_Corrected',
                             'Total_NTR_EUR_Group_Corrected',
                             'Mapping_Cluster_ML',
                             'Mapping_Histo_ML',
                             'Mapping_Destination_Production',
                                'Total_Pax_Paying_FBS',
                                'Total_Pax_Paying_Direct',
                                'Total_Pax_Paying_SOB',
                               'Total_Pax_Paying_Web',
                                'Total_Pax_Paying_Repeaters',
                                'Total_NTR_Max',
                                'Total_Cabin_Paying_Sup',
                                'Total_Cabin_Paying_Inf',
                                'FCST_PERDIEM_RAF'                              
                             ]             
    df.drop(variables_to_remove , axis = 1, inplace = True)
    return df



