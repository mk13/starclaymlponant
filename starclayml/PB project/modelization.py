import lightgbm as lgb
import pandas as pd
import random
import numpy as np
from sklearn.model_selection import RandomizedSearchCV, train_test_split
import lightgbm as lgb
from sklearn.metrics import recall_score, r2_score, explained_variance_score, mean_absolute_error, mean_squared_error,precision_score, f1_score , confusion_matrix , roc_auc_score
import matplotlib.pyplot as plt
import warnings

import os
path = 'F:/Datalab/xx - Works/ML_FINAL'

os.chdir(path+'/starclayml/PB project')
from metrics import classTimingAccuracy, classTotCabinIndivAccuracy


def Define_Cross_Validation_Sets(df, Number_of_data_sets = 90):
    cruises_list = list(df['Key_Cruise_Code'].unique())
    Number_of_cruises_by_set = round(len(cruises_list)/ Number_of_data_sets)
    df['data_set_id'] = 0
    for data_set_id in range(1,Number_of_data_sets):
        cruises_set = random.sample(cruises_list,  Number_of_cruises_by_set )
        df.loc[df['Key_Cruise_Code'].isin(cruises_set) , 'data_set_id'] = data_set_id
        cruises_list = list( set(cruises_list) - set(cruises_set) )
    return df


def Predict_PB_Leave_One_Cruise_Out(df, params ,predictors, target):
    num_boost_round = params['num_iterations']
    iteration = 0
    for cruise in list(df['Key_Cruise_Code'].unique()):
        iteration = iteration +1
        print(iteration, ' ' , cruise)
        train = df[ df['Key_Cruise_Code'] != cruise] 
        lgb_train = lgb.Dataset(
            train[predictors], 
            train[target], 
            feature_name=predictors
            )
    
        model = lgb.train( params,  lgb_train , num_boost_round=num_boost_round)
        if target == 'Current_PB':
            df.loc[ df['Key_Cruise_Code'] == cruise, 'PB_Predicted'] = model.predict(df.loc[ df['Key_Cruise_Code'] == cruise, predictors] )
        else:
            df.loc[ df['Key_Cruise_Code'] == cruise, 'PB_Changed_Predicted'] = model.predict(df.loc[ df['Key_Cruise_Code'] == cruise, predictors] )

    if target == 'Current_PB':
        df['Round_PB_Predicted']  = df['PB_Predicted'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
    else: 
        df['Round_PB_Changed_Predicted'] = round(df['PB_Changed_Predicted'])
        
    lgb_train = lgb.Dataset(
        df[predictors], 
        df[target], 
        feature_name=predictors
        )

    model = lgb.train( params,  lgb_train , num_boost_round=num_boost_round) 
    return (df, model)

def Predict_PB_Cross_Validation_KFlods(df, params, predictors, target ):
    
    num_boost_round = params['num_iterations']
    
    for data_set_id in list(df.data_set_id.unique()):
        print('Processing the dataset Number ', data_set_id)
        train = df[ df['data_set_id'] != data_set_id] 
        lgb_train = lgb.Dataset(
            train[predictors], 
            train[target], 
            feature_name=predictors
            )
    
        model = lgb.train( params,  lgb_train , num_boost_round= num_boost_round)
     
        if target == 'Current_PB':
            df.loc[ df['data_set_id'] == data_set_id, 'PB_Predicted'] = model.predict(df.loc[ df['data_set_id'] == data_set_id, predictors] )
        else:
            df.loc[ df['data_set_id'] == data_set_id, 'PB_Changed_Predicted'] = model.predict(df.loc[ df['data_set_id'] == data_set_id, predictors] )

    if target == 'Current_PB':
        df['Round_PB_Predicted']  = df['PB_Predicted'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
    else: 
        df['Round_PB_Changed_Predicted'] = round(df['PB_Changed_Predicted'])
       
    lgb_train = lgb.Dataset(
        df[predictors], 
        df[target], 
        feature_name=predictors
        )

    model = lgb.train( params,  lgb_train , num_boost_round=num_boost_round) 
    return (df, model)


def get_predictors(df, with_PB_infos = True,  PB_Changed_Predicted = True):
    PB_infos_variables=  ['Delay_PB_Changed_Days',
                                     'previous_PB_value',
                                                 
                                     'Total_Cab_PB_30',
                                     'Total_Cab_PB_25',
                                     'Total_Cab_PB_20',
                                     'Total_Cab_PB_15',
                                     'Total_Cab_PB_10',
                                     'Total_Cab_PB_5',
                                     'Total_Cab_PB_0',
                                     'Cluster_Moyenne_Moyenne_PB',
                                     'Moyenne_PB',
                            
                                     'FCST_PB30',
                                     'FCST_PB25',
                                     'FCST_PB20',
                                     'FCST_PB15',
                                     'FCST_PB10',
                                     'FCST_PB5',
                                     'FCST_PB0',
                                     
                                     'FCST_Last_PB',
                                     'Total_Cab_Last_PB',
                                     'Ecart_Last_PB',
                                     
                                     'Ecart_PB_30',
                                     'Ecart_PB_25',
                                     'Ecart_PB_20',
                                     'Ecart_PB_15',
                                     'Ecart_PB_05',
                                     'Ecart_PB_0']
    
    not_in_predictors =   ['PB_Predicted', 
                                     'test_fold',
                                     'data_set_id' ,
                                     'PB_CHANGED',
                                     'Current_PB',
                                     'Booking_Date_OBS' ,
                                     'Key_Cruise_Code'
                                     ]
    
    #categorical_variables are not in predictors anyway
    categorical_variables = [var for var in list(df.dtypes[df.dtypes == 'object'].index) if (var not in ['Key_Cruise_Code']) ]
    not_in_predictors = not_in_predictors + categorical_variables
    
    if not PB_Changed_Predicted:
        not_in_predictors = not_in_predictors + ['Round_PB_Changed_Predicted' ,
                                                 'PB_Changed_Predicted']
                
    if not with_PB_infos:
        not_in_predictors = not_in_predictors  + PB_infos_variables
        
    predictors = [col for col in list(df.columns) if (col not in ( not_in_predictors)) ]
    
    return predictors
    
def get_best_recorded_parameters(target = 'Current_PB', with_PB_infos = True, PB_Changed_Predicted = True ):
    if target == 'PB_CHANGED':
        #best params PB_changed_roc_auc, shift changes to the day before
        return {    
        #Boosting parameters
            'boosting_type': 'dart' , # 'gbdt'
            'objective': 'binary', 
            'metric': 'auc',
            'scale_pos_weight': 45,
        #    
            'learning_rate':  0.1,  #0.08
            'num_iterations' : 274   , #312
         
            'max_bin':  243,
            'min_data_in_bin': 12 ,
        
         #Tree specific Parameters   
            'min_data_in_leaf': 9,
            'max_depth': 10,
        
            'bagging_freq' :  5,     
            'bagging_fraction' :    0.95, 
            'feature_fraction':   0.67,
            'bagging_seed' : 3,
            
            'reg_alpha' : 0.9,
            'reg_lambda' : 29,
            
            'random_state': 3,
                }
    
    #Modèle de régression sans infos PB
    if not with_PB_infos:
        return {    
            'boosting_type': 'dart', 
            'objective': 'regression', 
            'metric': 'mean_absolute_error',  
            
            'learning_rate':  0.35,
            'num_iterations' : 234, 
         
            'max_bin':  243,
            'min_data_in_bin': 12 ,
        
            'min_data_in_leaf': 7,
            'max_depth': 8,
        
            'bagging_freq' : 6,     
            'bagging_fraction' : 0.85, 
            'feature_fraction': 0.8,
            'bagging_seed' : 3,
            
            'reg_lambda' : 15,
            
            'random_state': 3,
                }
    
    #Modèle de régression avec infos PB
    if ((with_PB_infos)  & (not PB_Changed_Predicted) ):
        return {    
            'boosting_type': 'dart', 
            'objective': 'regression', 
            'metric': 'mean_absolute_error',  
       
            'learning_rate': 0.6, 
            'num_iterations' : 274, 
         
            'max_bin':  243,
            'min_data_in_bin': 12 ,
        
            'min_data_in_leaf': 6,
            'max_depth': 5,
        
            'bagging_freq' : 8,     
            'bagging_fraction' : 0.85, 
            'feature_fraction': 0.8,
            'bagging_seed' : 3,
            
            'reg_lambda' : 24,
            
            'random_state': 3,
                }
    
    #Modèle de régression avec info PB et PB_Changed_Predicted
    params = {    
        'boosting_type': 'dart', 
        'objective': 'regression', 
        'metric': 'mean_absolute_error',  
        
        'learning_rate': 1, 
        'num_iterations' : 235, 
     
        'max_bin': 249,
        'min_data_in_bin': 9,
    
        'min_data_in_leaf': 11,
        'max_depth': 5,    
    
        'bagging_freq' : 5,     
        'bagging_fraction' : 0.85, 
        'feature_fraction': 0.9,
        'bagging_seed' : 3,
        
        'reg_lambda' : 16,
        
        'random_state': 3,
            }
    return params

def Evaluation_metrics(df , target = 'Current_PB', with_PB_infos = True, tolerance_values= [0, 1, 2]):
    if (with_PB_infos & (target == 'Current_PB')):
        df['PB_CHANGED'] = 0
        df.loc[df.previous_PB_value > df.Current_PB , 'PB_CHANGED'] = 1
        df['Round_PB_Changed_Predicted'] = 0
        df.loc[df.previous_PB_value > df.Round_PB_Predicted , 'Round_PB_Changed_Predicted'] = 1
    
    if with_PB_infos:
        print('f1_score = ' , f1_score(df[ 'PB_CHANGED'] , df[ 'Round_PB_Changed_Predicted']))
        print('confusion_matrix = ' , confusion_matrix(df[ 'PB_CHANGED'] , df[ 'Round_PB_Changed_Predicted']))
        print('recall_score = ' , recall_score(df[ 'PB_CHANGED'] , df[ 'Round_PB_Changed_Predicted']))
        print('precision_score = ' , precision_score(df[ 'PB_CHANGED'] , df[ 'Round_PB_Changed_Predicted']))
    
    if target == 'Current_PB':
        print('mae: ', mean_absolute_error(df[target] , df['PB_Predicted']))
    
    for tolerance in  tolerance_values:
        print('TimingAccuracy with tolerance = ',
              tolerance, ' : ', classTimingAccuracy(df , pb_changed_col= 'PB_CHANGED' , pred_col=  'Round_PB_Changed_Predicted', time_delta = tolerance)[2])
    
    for tolerance in  tolerance_values:
        print('TotCabinIndivAccuracy with tolerance = ',
              tolerance, ' : ', classTotCabinIndivAccuracy(df , pb_changed_col= 'PB_CHANGED' , pred_col=  'Round_PB_Changed_Predicted', tot_cabin_delta = tolerance)[2])

