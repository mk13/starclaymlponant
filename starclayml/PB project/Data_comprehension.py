#Chargement des bibliothèques
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib.pylab as pylab
os.chdir('/home/allan/Documents/PONANT/Data')



#Lecture de la table PBSSCALENDAR
df_PB = pd.read_excel('PBSSCALENDAR.xlsx')
#Les variables clairement inutiles à la problèmatique sont à supprimer
df_PB.drop([ 'ID', 
             'UID', 
             'Editor', 
             'Creation_Timestamp', 
             'Edition_Timestamp'] , axis = 1 ,inplace = True)
#On retient uniquement les croisière ayant une date de départ2015 ou plus tard 
df_PB = df_PB[ df_PB['Cruise_Code'].str[-2:].astype(int) >14]
#Supprimer les lignes sans date d'application du PB (Pré-enregistrement?)
df_PB_by_cruise = df_PB[~df_PB['PBSS_CAL_Date'].isna()]
#Tri de la table par croisière et par date
df_PB.sort_values([ 'Cruise_Code' ,'PBSS_CAL_Date'] , inplace = True)
#Vérifier le type des variables
df_PB.dtypes



#Vérifier que les dates sont les bonnes formant une série temporelle par croisière, entre l'ouverture des ventes et la date de la croisière
#Dour chaque croisière: Nombre de dates uniques == nombre de lignes == nombre de jours entre la première date et la dernière
df_PB_by_cruise = df_PB_by_cruise.groupby('Cruise_Code').agg({
        'PBSS_CAL_Date' : ['nunique', 'count', 'min', 'max' ],})
df_PB_by_cruise.reset_index(inplace =True)
df_PB_by_cruise['NB_days'] = (df_PB_by_cruise.PBSS_CAL_Date['max'] - df_PB_by_cruise.PBSS_CAL_Date['min']).dt.days + 1 
df_PB_by_cruise[df_PB_by_cruise.PBSS_CAL_Date['count'] != df_PB_by_cruise.PBSS_CAL_Date['nunique'] ]
df_PB_by_cruise[df_PB_by_cruise.PBSS_CAL_Date['count'] != df_PB_by_cruise['NB_days'] ]

#Correction des erreurs
index_to_remove = df_PB[(df_PB.Cruise_Code == 'A011215') & (df_PB.PBSS_CAL_Date == '2016-10-25')].index.values
df_PB.drop(index_to_remove , axis = 0, inplace = True)
df_PB.loc[(df_PB['Cruise_Code'] == 'P030513') & (df_PB['PBSS_CAL_Date'].dt.year <2016) ,'Cruise_Code' ] = 'P030513_1'
df_PB.loc[(df_PB['Cruise_Code'] == 'P030513') & (df_PB['PBSS_CAL_Date'].dt.year >= 2016) ,'Cruise_Code' ] = 'P030513_2'


#Chargement de l'extrait de données, 
df = pd.read_excel('Data_First_Extract.xlsx')
#selection de croisières partant après 2014
df = df[ df['Key_Cruise_Code'].str[-2:].astype(int) >14]
#suppression des variables statiques, et des ratios mal calculés
variables_to_remove = ['Mapping_Charter',
                       'Mapping_Cruise_Status',
                       'Mapping_Cluster_ML',
                       'Mapping_Histo_ML',
                       'Mapping_Destination_Production',
                       'Occupation_Rate_excl_OPT',
                       'Part_de_Pax_Paying_FBS'
                       ]
df.drop(variables_to_remove , axis = 1, inplace = True)
#Repérer les Valeurs manquantes dans les photos
df = df.fillna(value = 9999999999)
df.dtypes


#Correction du format de la variable Key_Booking_Date_OBS 
df['Key_Booking_Date_OBS'] = pd.to_datetime(df['Key_Booking_Date_OBS'])
#Jointure des deux tables
df = pd.merge(left = df_PB,
              right = df,
              left_on = ['Cruise_Code' ,  'PBSS_CAL_Date'],
              right_on = ['Key_Cruise_Code' , 'Key_Booking_Date_OBS'] ,
              how = 'left')





#sélection des colonnes à remplir (variables en provenance des photos hebdomadaires)
columns_to_fill = [col for col in list(df.columns) if (col not in list(df_PB.columns))]
#Repérer la dernière ligne de chaque croisière (soit des barières empéchant la copie de données entre différentes croisières )
df['index'] = df.index
last_line_by_cruise = list(df[['Cruise_Code', 'index']].groupby('Cruise_Code').last()['index'])
df.loc[df.index.isin(last_line_by_cruise) & df['Key_Booking_Date_OBS'].isna() , columns_to_fill] = 1111111111
#Copie de la prochaine photo valide par croisière
df[columns_to_fill] = df[columns_to_fill].fillna(method = 'bfill')
#Remise des "vrais valeurs manquantes"
df = df.replace({9999999999 : np.nan})
#Suppression des dernières lignes de croisière sans photo
df = df[~ (df['Key_Booking_Date_OBS'] == 1111111111)] 
df.drop(['index', 'Key_Booking_Date_OBS' , 'Key_Cruise_Code'], axis = 1, inplace = True)  




#Nombre de Valeurs manquantes par colonnes
df.isnull().sum()



#Types de variables
df.dtypes



#Remplissage des valeurs manquantes pour les variables catégorielles
df.loc[df['Mapping_FCST_Model'].isna() , 'Mapping_FCST_Model'] = 'Unknown'
df.loc[df['Mapping_Theme'].isna() , 'Mapping_Theme'] = 'Other'
df.loc[df['Mapping_Guest'].isna() , 'Mapping_Guest'] = 'Other'
df.loc[df['Mapping_Partnership'].isna() , 'Mapping_Partnership'] = 'Other'





#Définition du format des variables catégorielles, et des variables datetime
categorical_variables = ['Mapping_Vessel',
                         'Mapping_From_To',
                         'Mapping_Destination',
                         'Mapping_Season',
                         'Mapping_Theme',
                         'Mapping_Guest',
                         'Mapping_Partnership',
                         'Mapping_Cluster_Pricing',
                         'Mapping_FCST_Model',
                         'Mapping_Destination_Semester',
                         'Mapping_Cluster_Pricing_2',
                        ]
df[categorical_variables] = df[categorical_variables].astype('category')
df['Date_Premiere_Reservation_Indiv'] = pd.to_datetime(df['Date_Premiere_Reservation_Indiv'])
df.dtypes



#Recalcul de la moyenne des Moyennes
cab_by_pb_var = ['Total_Cab_PB_30' , 'Total_Cab_PB_25', 'Total_Cab_PB_20',
     'Total_Cab_PB_15', 'Total_Cab_PB_10' , 'Total_Cab_PB_5', 'Total_Cab_PB_0']
df[cab_by_pb_var] = df[cab_by_pb_var].fillna(0)
df['Moyenne_PB'] = (0.30*df['Total_Cab_PB_30'] 
                    + 0.25*df['Total_Cab_PB_25']
                    + 0.20*df['Total_Cab_PB_20']
                    +0.15*df['Total_Cab_PB_15']
                    +0.10*df['Total_Cab_PB_10']
                    +0.05*df['Total_Cab_PB_5'])/(-df[cab_by_pb_var].sum(axis = 1))
df['Moyenne_PB'] = df['Moyenne_PB'].fillna(0)

df['Moyenne_PerDiem_Pax_EUR'] =  df['Total_NTR_EUR'] / df['Total_PCD_Paying']
df['Moyenne_PerDiem_Cabine_EUR'] =  df['Total_NTR_EUR'] / df['Total_CCD_Paying']

df['Moyenne Perdiem_Cabine EUR_Indiv'] = df['Total_NTR_EUR_INDIV'] / df['Total_CCD_Paying']
df['Moyenne Perdiem_Cabine EUR_Group'] = df['Total_NTR_EUR_Group'] /df['Total_CCD_Paying_Group']
df['Moyenne_Panier_EUR_Indiv'] = df['Total_NTR_EUR_INDIV'] /  df['Total_Cabin_Paying_Indiv']



#Remplacer les valeurs manquantes en 0 pour certaines valeurs manquantes
nan_to_zeros_var = ['Moyenne_PerDiem_Cabine_EUR',
                    'Moyenne_PerDiem_Pax_EUR',
                    'Moyenne Perdiem_Cabine EUR_Indiv',
                    'Moyenne Perdiem_Cabine EUR_Group',
                    'Moyenne_Panier_EUR_Indiv',
                    'Total_Cabin_GIP_Group',
                    ]
df[nan_to_zeros_var] = df[nan_to_zeros_var].fillna(0)



