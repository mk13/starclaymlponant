import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import lightgbm as lgb
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error, mean_squared_error
import datetime
from itertools import product
import pickle



def get_number_changes_between_records( date, previous_date , PB_changed_date ,  cruise  ,df_PB):
    df_cruise = df_PB[df_PB['Cruise_Code'] == cruise]
    df_cruise = df_cruise.sort_values(['date'])
    df_cruise.reset_index(inplace = True, drop = True)
    if (df_cruise.shape[0]== 0):
        return (np.nan, np.nan, np.nan)
    df_cruise.reset_index(inplace = True, drop = True)
    if (df_cruise[df_cruise['date'] == previous_date].shape[0]== 0):
        return (np.nan, np.nan, np.nan)
    if (df_cruise[df_cruise['date'] == date].shape[0]== 0):
        return (np.nan, np.nan, np.nan)
    index = df_cruise[df_cruise['date'] == date].index.values[0].copy()
    previous_index = df_cruise[df_cruise['date'] == previous_date].index.values[0].copy()
    previous_change_index = previous_index
    penultimate_change_index = previous_index
    NB_changes = 0
    while (index != previous_index) :

        if df_cruise.loc[previous_index  , 'PB'] != df_cruise.loc[previous_index+1  , 'PB']:
            penultimate_change_index = previous_change_index
            previous_change_index = previous_index+1
            NB_changes = NB_changes + 1 
            if NB_changes >1 : 
                previous_PB_value = df_cruise.loc[previous_index  , 'PB'].copy()
        previous_index = previous_index + 1
    Delay_PB_Changed_Days = (date - df_cruise.loc[penultimate_change_index , 'date']).days
    Last_SS_value = df_cruise.loc[penultimate_change_index , 'SupSingle' ]
    previous_PB_value =  df_cruise.loc[penultimate_change_index , 'PB' ]
    return (NB_changes , Delay_PB_Changed_Days, previous_PB_value ,  Last_SS_value)



def get_Last_Total_Cab_PB_Column(PB_Value):
    total_cab_columns =  ['Total_Cab_PB_30',
                          'Total_Cab_PB_25',
                          'Total_Cab_PB_20',
                          'Total_Cab_PB_15',
                          'Total_Cab_PB_10',
                          'Total_Cab_PB_5',
                          'Total_Cab_PB_0']
    name = 'Total_Cab_PB_'  + str(int(PB_Value *100))
    Last_Total_Cab_PB_Column = [col for col in total_cab_columns if name in col]
    if len(Last_Total_Cab_PB_Column) == 1:
        return Last_Total_Cab_PB_Column
    else:
        return np.nan
    
def get_Last_FCST_PB_Column(PB_Value) :
    FCST_PB_columns =  [ 'FCST_PB30',
                           'FCST_PB25',
                           'FCST_PB20',
                           'FCST_PB15',
                           'FCST_PB10',
                           'FCST_PB5',
                           'FCST_PB0']
    name = 'FCST_PB'  + str(int(PB_Value *100))
    Last_FCST_PB_Column = [col for col in FCST_PB_columns if name in col]
    if len(Last_FCST_PB_Column) == 1:
        return Last_FCST_PB_Column
    else:
        return np.nan
    
    
    
def lgb_plot_importance(lgb_model, predictors, nlargest=10 , nsmallest =10,  figsize = (15,25), **kwargs): 
    if type(lgb_model == 'lightgbm.basic.Booster'):
        df = pd.DataFrame(  lgb_model.feature_importance())
    else:
        df = pd.DataFrame(  lgb_model.feature_importances_)
    df.index = predictors
    df.columns = ['Feature_importance']
    df_smallest = df.nsmallest(nsmallest,  columns = 'Feature_importance').sort_values('Feature_importance', ascending = False)
    df_largest = df.nlargest(nlargest,  columns = 'Feature_importance')
    df = pd.concat([df_largest , df_smallest] , axis = 0)
    df.plot(kind = 'bar', figsize = figsize)
    
    
    
def predict_PB_for_one_cruise( df , cruise  , params, predictors, target):      
    print(cruise)
    train = df[df['Key_Cruise_Code'] != cruise]
    test = df[df['Key_Cruise_Code'] ==  cruise]
    lgb_train = lgb.Dataset(
        train[predictors], 
        train[target], 
        feature_name=predictors,
        )
    lgb_train.raw_data = None
    
    lgb_valid = lgb.Dataset(
        test[predictors], 
        test[target], 
        )
    lgb_valid.raw_data = None
    
    model = lgb.train(
        params,
        lgb_train,
        num_boost_round=1000) 
    
    test['prediction'] = model.predict(test[predictors])
    test['prediction']  = test['prediction'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
    mae = mean_absolute_error(test[target] , test['prediction'])
    print('mae: ', mae)
    test.index = test['Booking_Date_OBS']
    test[[ 'Current_PB' , 'prediction' ]].plot()
    plt.show()
    return np.array(test['prediction'])


def predict_PB_for_one_cruise_record_by_record( df , cruise  , params, predictors, target):      
    print(cruise)
    train = df[df['Key_Cruise_Code'] != cruise]
    test = df[df['Key_Cruise_Code'] ==  cruise]
    test.sort_values(['Booking_Date_OBS'] , inplace = True)
    test.reset_index(inplace = True, drop = True)
    lgb_train = lgb.Dataset(
        train[predictors], 
        train[target], 
        feature_name=predictors,
        )
    
    model = lgb.train(
        params,
        lgb_train,
        num_boost_round=params['num_iterations'])
    
    INDEX = 0 
    while INDEX <= (test.shape[0]-1):
        tmp = model.predict(test[predictors])
        test.loc[INDEX , 'PB_predicted'] = int(5 * round(float(100*tmp[INDEX])/5))/100 
        if INDEX != (test.shape[0]-1):
            test.loc[INDEX +1 , 'previous_PB_value'] =   int(5 * round(float(100*tmp[INDEX])/5))/100 
        INDEX = INDEX + 1 
    return test[[ 'Key_Cruise_Code', 'Booking_Date_OBS'  , 'previous_PB_value',  'PB_predicted']]


#To generate all combination from a dictionary of list
def dict_product(d):
    keys = d.keys()
    for element in product(*d.values()):
        yield dict(zip(keys, element))
        
def save_obj(obj, name ):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)