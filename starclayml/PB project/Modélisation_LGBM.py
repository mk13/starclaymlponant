import pandas as pd
import numpy as np
import os
from sklearn.model_selection import RandomizedSearchCV 
import lightgbm as lgb
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error, mean_squared_error
import datetime
import random

os.chdir('/home/allan/Documents/PONANT/starclayml/PB project')
from Preprocessing import correct_format, select_data, get_previous_PB_and_delay_changed_days, fill_missing_values
from Preprocessing import transform_categorical_variables_values, add_FROM_TO_latitude_longitude, add_calendar_variables
from Preprocessing import add_FCST_total_cabin_by_PB_level_variables, categorical_to_dummies, drop_unuseful_variables
from utils import predict_PB_for_one_cruise, lgb_plot_importance
from metrics import regTimingAccuracy
os.chdir('/home/allan/Documents/PONANT/Data')


#Chargement de l'extrait de données, 
df = pd.read_excel('DATA9.xlsx')

df = correct_format(df)
df = select_data(df)
df = get_previous_PB_and_delay_changed_days(df , PBSSCALENDAR_link = 'PBSSCALENDAR.xlsx')
df = fill_missing_values(df)
df = transform_categorical_variables_values(df)
df0 = df.copy()
df = add_FROM_TO_latitude_longitude(df)
df = add_calendar_variables(df)
df = add_FCST_total_cabin_by_PB_level_variables(df)
df = categorical_to_dummies(df)
df = drop_unuseful_variables(df)


continuious_varibles_out_of_model=  ['Delay_PB_Changed_Days',
                                     'previous_PB_value',
                                                 
                                     'Total_Cab_PB_30',
                                     'Total_Cab_PB_25',
                                     'Total_Cab_PB_20',
                                     'Total_Cab_PB_15',
                                     'Total_Cab_PB_10',
                                     'Total_Cab_PB_5',
                                     'Total_Cab_PB_0',
                                     'Cluster_Moyenne_Moyenne_PB',
                                     'Moyenne_PB',
                            
                                     'FCST_PB30',
                                     'FCST_PB25',
                                     'FCST_PB20',
                                     'FCST_PB15',
                                     'FCST_PB10',
                                     'FCST_PB5',
                                     'FCST_PB0',
                                     
                                     'FCST_Last_PB',
                                     'Total_Cab_Last_PB',
                                     'Ecart_Last_PB',
                                     
                                     'Ecart_PB_30',
                                     'Ecart_PB_25',
                                     'Ecart_PB_20',
                                     'Ecart_PB_15',
                                     'Ecart_PB_05',
                                     'Ecart_PB_0']

target = ['Current_PB']
predictors = [col for col in list(df.columns) if (col not in (target + ['Booking_Date_OBS' , 'Key_Cruise_Code'] 
              #+ continuious_varibles_out_of_model
              )) ]

cruises_list = list(df['Key_Cruise_Code'].unique())
Training_percentage = 80
Number_of_cruises_for_training = round((Training_percentage/100)*df['Key_Cruise_Code'].unique().shape[0])
training_cruises = random.sample(cruises_list,  Number_of_cruises_for_training )

train = df[df['Key_Cruise_Code'].isin(training_cruises)].copy()
test = df[~df['Key_Cruise_Code'].isin(training_cruises)].copy()
test.sort_values('Key_Cruise_Code' , inplace = True)
test.reset_index(inplace = True, drop = True)

params = {    
#Boosting parameters
    'boosting_type': [ 'dart' , 'gbdt'],
    'objective': ['regression'],
    'metric': ['auc'],
    
    'n_estimators' : [120, 130, 140, 150, 200, 220, 250,300,350],
    'max_bin': [ 225 , 255 , 285],
    'min_data_in_bin': [3 , 6 , 9],
    'learning_rate': [0.08, 0.1, 0.12],

    'feature_fraction': [ 0.8 , 0.9, 0.95],
    'bagging_fraction': [ 0.7, 0,8, 0.9],
    'bagging_freq': [0,  5, 10],
    
 #Tree specific Parameters   
    'num_leaves': [15, 20 , 25 , 30, 35, 40 ],
    'min_data_in_leaf': [3, 5, 10 , 15],
    'max_depth': [7, 9 , 11 , 13],
        }

model = lgb.LGBMRegressor(
          boosting_type= params['boosting_type'],
          objective = params['objective'],
          metric = params['metric'],


          n_estimators = params['n_estimators'],          
          max_bin = params['max_bin'],
          min_data_in_bin = params['min_data_in_bin'],
          learning_rate = params['learning_rate'],          

#          feature_fraction = params['feature_fraction'],
#          bagging_fraction = params['bagging_fraction'],
          bagging_freq = params['bagging_freq'],          
          
          num_leaves = params['num_leaves'],
          min_data_in_leaf = params['min_data_in_leaf'],
          max_depth = params['max_depth']
           )

# Create the grid
grid = RandomizedSearchCV(model, params,
                    verbose=10,
                    cv=3,
                    n_jobs= -1)
# Run the grid
grid.fit(np.array(train[predictors]), np.ravel(train[target]) )

mdl = lgb.LGBMRegressor(bagging_fraction=0.6, bagging_freq=5, boosting_type='gbdt',
       class_weight=None, colsample_bytree=1.0, feature_fraction=0.9,
       importance_type='split', learning_rate=0.08, max_bin=255,
       max_depth=9, metric='auc', min_child_samples=20,
       min_child_weight=0.001, min_data_in_bin=9, min_data_in_leaf=10,
       min_split_gain=0.0, n_estimators=100, n_jobs=-1, num_leaves=20,
       objective='regression', random_state=None, reg_alpha=0.0,
       reg_lambda=0.0, silent=True, subsample=1.0,
       subsample_for_bin=200000, subsample_freq=0)


lgb_train = lgb.Dataset(
    train[predictors], 
    train[target], 
    feature_name=predictors,
    )
lgb_train.raw_data = None

lgb_valid = lgb.Dataset(
    test[predictors], 
    test[target], 
    )
lgb_valid.raw_data = None

start_time = datetime.datetime.now()

model = lgb.train(
    grid.best_estimator_.get_params(),
    lgb_train,
    num_boost_round=10000,
    valid_sets=[lgb_train, lgb_valid],
    early_stopping_rounds=100,
    verbose_eval=100,
)
print("training duration : ", datetime.datetime.now() - start_time)

test['prediction'] = model.predict(test[predictors])
test['prediction']  = test['prediction'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
mae = mean_absolute_error(test[target] , test['prediction'])
print('mae: ', mae)



test['PB_changed'] = 0
test.loc[test['Current_PB'] != test['previous_PB_value']  ,'PB_changed' ] = 1
test['equal'] = 0
test.loc[test['Current_PB'] == test['prediction'] , 'equal'] = 1
test.groupby(['PB_changed']).agg({'equal' : 'sum'})
first_lines_cruises = list(test[['Key_Cruise_Code']].reset_index().groupby('Key_Cruise_Code').first()['index'].values)

#regTimingAccuracy(test, "Current_PB", "prediction", 0)

test[ 'previous_prediction'] = test['prediction'].shift(1)
test.loc[first_lines_cruises , 'previous_prediction']  = test[ 'prediction']
test['equal_to_previous'] = 0
test.loc[test['Current_PB'] == test['previous_prediction'] , 'equal_to_previous'] = 1
test.groupby([ 'PB_changed','equal' , 'equal_to_previous']).agg({'Current_PB' : 'count'})


lgb_plot_importance(model, predictors, 20, 20 ,  figsize = (20,10))






#Test par croisi�re
train = df[df['Key_Cruise_Code'] != 'A020619']
test = df[df['Key_Cruise_Code'] ==  'A020619']
params = {    
#Boosting parameters
    'boosting_type': [ 'dart' , 'gbdt'],
    'objective': ['regression'],
    'metric': ['auc'],
    
    'n_estimators' : [90,100, 110, 120, 130, 140, 150],
    'max_bin': [ 225 , 255 , 285],
    'min_data_in_bin': [3 , 6 , 9],
    'learning_rate': [0.08, 0.1, 0.12],

    'feature_fraction': [0.8 , 0.9, 0.95],
    'bagging_fraction': [0.6, 0.7, 0.9],
    'bagging_freq': [0,  5, 10],
    
 #Tree specific Parameters   
    'num_leaves': [15, 20 , 25 , 30],
    'min_data_in_leaf': [3, 5, 10],
    'max_depth': [9],
        }

model = lgb.LGBMRegressor(
          boosting_type= params['boosting_type'],
          objective = params['objective'],
          metric = params['metric'],

          n_estimators = params['n_estimators'],          
          max_bin = params['max_bin'],
          min_data_in_bin = params['min_data_in_bin'],
          learning_rate = params['learning_rate'],          

           feature_fraction = params['feature_fraction'],
           bagging_fraction = params['bagging_fraction'],
           bagging_freq = params['bagging_freq'],          
          
          num_leaves = params['num_leaves'],
          min_data_in_leaf = params['min_data_in_leaf'],
          max_depth = params['max_depth']
           )
# Create the grid
grid = RandomizedSearchCV(model, params,
                    verbose=10,
                    cv=3,
                    n_jobs= -1)
# Run the grid
grid.fit(np.array(train[predictors]), np.ravel(train[target])  )
df['prediction'] = 99
for cruise in list(df['Key_Cruise_Code'].unique()):
    df.loc[df['Key_Cruise_Code'] == cruise ,'prediction'] = predict_PB_for_one_cruise(df, cruise, grid)
mean_absolute_error(df[target] , df['prediction'])
  #0.0118  

df_pb_levels = df[[ 'Key_Cruise_Code' ,  'Current_PB']].groupby('Key_Cruise_Code').agg({ 'Current_PB':'nunique'})
cst_cruise = list(df_pb_levels[df_pb_levels['Current_PB'] == 1].index)
for crs in cst_cruise:
    predict_PB_for_one_cruise(df, crs, grid)

