import pandas as pd
import os
from sklearn.externals import joblib

path = 'F:/Datalab/xx - Works/ML_FINAL'

model_link = path + '/Data/Models'
project_link = path + '/starclayml/PB project'
data_input_link = path + '/Data/Data_input'
data_output_link = path +'/Data/Data_output'

shift_changes_to_previous = True
with_PB_infos = True
PB_Changed_Predicted = True
target = 'Current_PB'
cruise_by_cruise = False
Number_of_data_sets = 20

os.chdir(project_link)
from Preprocessing import read_input_data
from Preprocessing import correct_format, select_data, fill_missing_values
from Preprocessing import get_previous_PB_and_delay_changed_days_shift_changes_to_previous 
from Preprocessing import transform_categorical_variables_values, add_FROM_TO_latitude_longitude, add_calendar_variables
from Preprocessing import add_FCST_total_cabin_by_PB_level_variables, categorical_to_dummies, drop_unuseful_variables
from Preprocessing import get_previous_PB_and_delay_changed_days_shift_changes_to_next
from utils import predict_PB_for_one_cruise, lgb_plot_importance
from modelization import get_predictors, Define_Cross_Validation_Sets, get_best_recorded_parameters, Predict_PB_Cross_Validation_KFlods
from modelization import Evaluation_metrics, Predict_PB_Leave_One_Cruise_Out


#*******************************
#Chargement de l'extrait de données
#*******************************
df = read_input_data(data_input_link)
    
#*******************************
#Preprocessing
#*******************************
df = correct_format(df)
df = select_data(df)
df = fill_missing_values(df)
if shift_changes_to_previous: 
    df = get_previous_PB_and_delay_changed_days_shift_changes_to_previous(df)
else:
    df = get_previous_PB_and_delay_changed_days_shift_changes_to_next(df , PBSSCALENDAR_link = data_input_link + '/PBSSCALENDAR.xlsx') 
df = transform_categorical_variables_values(df)
df = add_FROM_TO_latitude_longitude(df)
df = add_calendar_variables(df)
df = add_FCST_total_cabin_by_PB_level_variables(df)
df = categorical_to_dummies(df)
df = drop_unuseful_variables(df)



#*******************************
#Predict PB_CHANGED
#*******************************


if (PB_Changed_Predicted & (target == 'Current_PB')):
    print('You choose to predict Current PB with an estimator of PB_CHANGED')
    predictors = get_predictors(df, with_PB_infos = True,  PB_Changed_Predicted = False)
    params = get_best_recorded_parameters(target = 'PB_CHANGED', with_PB_infos = True, PB_Changed_Predicted = False )
    if cruise_by_cruise:
        df,model = Predict_PB_Leave_One_Cruise_Out(df, params ,predictors, target = 'PB_CHANGED')
    else:
        df = Define_Cross_Validation_Sets(df, Number_of_data_sets = Number_of_data_sets)
        df , model = Predict_PB_Cross_Validation_KFlods(df, params, predictors, target = 'PB_CHANGED')
    joblib.dump(model, model_link +'/premodel_PB_CHANGED_predictor.pkl')
elif (target == 'Current_PB') :
    print('You choose to predict ' + target + ' without using an estimator of PB_CHANGED')
else :
    print('You choose to predict ' + target)

#*******************************
#Main model
#*******************************
predictors = get_predictors(df, with_PB_infos = with_PB_infos,  PB_Changed_Predicted = PB_Changed_Predicted)
params = get_best_recorded_parameters(target = target , with_PB_infos = with_PB_infos, PB_Changed_Predicted = PB_Changed_Predicted )
if cruise_by_cruise:
    df,model = Predict_PB_Leave_One_Cruise_Out(df, params ,predictors, target = target)
else:
    df = Define_Cross_Validation_Sets(df, Number_of_data_sets = Number_of_data_sets)
    df , model = Predict_PB_Cross_Validation_KFlods(df, params, predictors, target  = target)
joblib.dump(model, model_link + '/' + target + '_predictor'+ '.pkl')
    
#*******************************
#Save the model and the Data Frame with predictions
#*******************************
df.to_csv(data_output_link + '/DATA_results.csv')

#*******************************
#Evaluation
#*******************************
#Evaluation_metrics(df , target = 'Current_PB', with_PB_infos = True, tolerance_values= [0, 1, 2])
#TO DO
#Evaluation_metrics(df.loc[df['Booking_Date_OBS']<'19/07/2018',] , target = 'Current_PB', with_PB_infos = True, tolerance_values= [0, 1, 2])

#Evaluation_metrics(df.loc[df['Booking_Date_OBS']>='19/07/2018',] , target = 'Current_PB', with_PB_infos = True, tolerance_values= [0, 1, 2])

Evaluation_metrics(df , target = 'PB_CHANGED', with_PB_infos = True, tolerance_values= [0, 1, 2])
#TO DO
Evaluation_metrics(df.loc[df['Booking_Date_OBS']<'19/07/2018',] , target = 'PB_CHANGED', with_PB_infos = True, tolerance_values= [0, 1, 2])

Evaluation_metrics(df.loc[df['Booking_Date_OBS']>='19/07/2018',] , target = 'PB_CHANGED', with_PB_infos = True, tolerance_values= [0, 1, 2])




lgb_plot_importance(model, predictors, 20, 20 ,  figsize = (20,10))


#*******************************
#Annexe function, could be used to predict one cruise
#*******************************
#cruise = 'R150120'
#predict_PB_for_one_cruise( df , cruise  , params, predictors, target)


def main():
    pass

if __name__ == "__main__":
   main()
