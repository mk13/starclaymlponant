#Chargement des bibliothèques
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import matplotlib.pylab as pylab
import xgboost
from sklearn.model_selection import RandomizedSearchCV ,GridSearchCV, train_test_split, LeaveOneOut,KFold,StratifiedKFold
from sklearn.preprocessing import Normalizer , StandardScaler
import lightgbm as lgb
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error, mean_squared_error
import datetime


os.chdir('/home/allan/Documents/PONANT/Data')

from geopy.geocoders import Nominatim
import pandas as pd

#G�ocodeur
geolocator = Nominatim(user_agent="Ponant")
def get_city_coordinates(city_name):

    # On lit le fichier csv des coordonn�es des villes
    city_coordinates_file = open("city_coordinates.csv", "a")

    # Cr�ation d'un dataframe avec "city" comme index
    city_coordinates_df = pd.read_csv("city_coordinates.csv", sep="|",
                                                            names=["city",
                                                            "latitude",
                                                            "longitude"],
                                                            index_col=["city"])
    latitude = ""
    longitude = ""

    # R�cup�ration des donn�es si la ville existe d�j�
    if city_name in city_coordinates_df.index:
        latitude = city_coordinates_df.loc[city_name, "latitude"]
        longitude = city_coordinates_df.loc[city_name, "longitude"]

    # Sinon utiliser geopy pour r�cup�rer les coordonn�es
    else:
        location = geolocator.geocode(city_name)

        # Si la ville n'a pas de location on throw une erreur
        if not location:
            city_coordinates_file.close()
            raise ValueError(f"Can't find coordinates of city \"{city_name}\"")

        # Sauvegarde des coordonn�es dans le fichier
        city_coordinates_file.write(f"{city_name}|{location.latitude}|{location.longitude} \n")
        city_coordinates_file.close()
        latitude = location.latitude
        longitude = location.longitude

    city_coordinates_file.close()
    return (latitude, longitude)



#Chargement de l'extrait de données, 
df = pd.read_excel('DATA5.xlsx')

#**************************
#Data Selection
#************************
#Format datetime variables
df['FCST_DATE_CREATION'] = pd.to_datetime(df['FCST_DATE_CREATION'])
df['Date_Premiere_Reservation_Indiv'] = pd.to_datetime(df['Date_Premiere_Reservation_Indiv'])
df['PB_Changed_Date'] = pd.to_datetime(df['PB_Changed_Date'])
df['Booking_Date_OBS'] = pd.to_datetime(df['Booking_Date_OBS'])
df['Mapping_Date_Departure'] = pd.to_datetime(df['Mapping_Date_Departure'])
df['Date_Premiere_Option_Indiv Option'] = pd.to_datetime(df['Date_Premiere_Option_Indiv Option'])

#Sélection des données après le 01/11/2016
df = df[df['Booking_Date_OBS'] >= '2016-11-26'] 
print(df.shape)

#Remove lines with Current_PB NULL
df = df[~df['FCST_TOTAL_CABIN'].isna()]
print(df.shape)

#Remove group cruises
df = df[~((df['Mapping_Vessel'] == 'PONANT')  & 
   (df['Total_Cabin_Total_Group'] / df['Mapping_Cabin_Capacity'] >= 0.7)) ]
df = df[~((df['Mapping_Vessel'] != 'PONANT')  & 
   (df['Total_Cabin_Total_Group'] / df['Mapping_Cabin_Capacity'] >= 0.94)) ]

#Remove records before first reservation
df = df[~df['Date_Premiere_Reservation_Indiv'].isna()]
print(df.shape)

#Remove lines without FCST
df = df[~df['Mapping_FCST_Model'].isna() ]
print(df.shape)

#Remove list of cruises
cruises_to_remove = ['P250317',#vendue aux groupes mais en Pax indiv
                     
                     'P180120', #Pas ouverte � la vente
                     
                     'P180220', #7 croisi�res tout le temps � PB 0 pour travaux de r�novation
                     'P250202',
                     'P030320',
                     'P100320',
                     'P170320',
                     'P240320',
                     'P310320']
df = df[~df['Key_Cruise_Code'].isin(cruises_to_remove)]


#Tri des données par Code croisière et date
df = df.sort_values(['Key_Cruise_Code' , 'Booking_Date_OBS'])
df.nunique(dropna = False)[df.nunique(dropna = False) == 1]

#********************************
#Select variables
#********************************
#suppression des variables statiques
variables_to_remove = ['Mapping_Charter',
                       'Mapping_Cruise_Status',
                       'Mapping_Cluster_ML',
                       'Mapping_Histo_ML',
                       'Mapping_Destination_Production',
                       'FCST_UID',
                       'FCST_DATE',
                       'ID',
                       'Delay_PB_Changed_Days_MIN',
                       'FCST_COMMENTAIRES',
                       'CRUISE_CODE'
                       ]
df.drop(variables_to_remove , axis = 1, inplace = True)

#**************************
#Variables format
#************************
#Check string variables
print(df.dtypes[df.dtypes == "object"].index)
df[list(df.dtypes[df.dtypes == "object"].index)].head()


#********************************
#Correct values
#********************************
#Mapping season to [Winter/Summer]
df['Mapping_Season'] = df['Mapping_Season'].apply(lambda x:''.join([c for c in x if c.isalpha()]))

#Lecture de la table PBSSCALENDAR
df_PB = pd.read_excel('PBSSCALENDAR.xlsx')
#Les variables clairement inutiles �  la problèmatique sont �  supprimer
df_PB.drop([ 'ID', 
             'UID', 
             'Editor', 
             'Creation_Timestamp', 
             'Edition_Timestamp',
             'SS_CAL_Amount',
             'PBSS_LT_Week',
             'ND_CAL_Amount',] , axis = 1 ,inplace = True)
df_PB['PBSS_CAL_Date'] = pd.to_datetime(df_PB['PBSS_CAL_Date'])
df_PB.columns = ['Cruise_Code' , 'date', 'PB']

df.reset_index(inplace = True, drop = True)
first_lines_cruises = list(df[['Key_Cruise_Code']].reset_index().groupby('Key_Cruise_Code').first()['index'].values)
df['previous_PB_value'] =    df.loc[:,'Current_PB'].shift(1)
df.loc[first_lines_cruises , 'previous_PB_value'] =  df['Current_PB']

index_PB_Changed = list(df.loc[df['PB_CHANGED'] == 1].index)
index_PB_Changed = [ind for ind in index_PB_Changed if (ind not in first_lines_cruises)]
df['previous_PB_Changed_Date'] =  df.loc[:,'PB_Changed_Date'].shift(1)

def get_number_changes_between_records( date, previous_date , cruise  ,df_PB):
    df_cruise = df_PB[df_PB['Cruise_Code'] == cruise]
    df_cruise = df_cruise.sort_values(['date'])
    df_cruise.reset_index(inplace = True, drop = True)
    if (df_cruise.shape[0]== 0):
        return (np.nan, np.nan, np.nan)
    df_cruise.reset_index(inplace = True, drop = True)
    if (df_cruise[df_cruise['date'] == previous_date].shape[0]== 0):
        return (np.nan, np.nan, np.nan)
    if (df_cruise[df_cruise['date'] == date].shape[0]== 0):
        return (np.nan, np.nan, np.nan)
    index = df_cruise[df_cruise['date'] == date].index.values[0].copy()
    previous_index = df_cruise[df_cruise['date'] == previous_date].index.values[0].copy()
    previous_PB_value = df_cruise.loc[previous_index  , 'PB'].copy()  
    Delay_PB_Changed_Days = 0
    NB_changes = 0
    while (index != previous_index) :
        #print('index ' , index)
        #print('previous_index ' , previous_index)
        if df_cruise.loc[previous_index  , 'PB'] != df_cruise.loc[previous_index+1  , 'PB']:
            NB_changes = NB_changes + 1 
            if NB_changes >1 : 
                previous_PB_value = df_cruise.loc[previous_index  , 'PB'].copy()
            Delay_PB_Changed_Days = 0
        #print(  'PB_values'   ,previous_PB_value , df_cruise.loc[index  , 'PB'])
        previous_index = previous_index + 1
        Delay_PB_Changed_Days = Delay_PB_Changed_Days + 1
        #print( 'Delay_PB_Changed_Days ' , Delay_PB_Changed_Days)
    return (Delay_PB_Changed_Days, previous_PB_value , NB_changes)
#get_number_changes_between_records(date, previous_date , cruise  ,df_PB)
df['NB_changes'] = 0
for INDEX in index_PB_Changed:
    tmp = get_number_changes_between_records(date = df.loc[INDEX , 'Booking_Date_OBS'] , 
                                             previous_date = df.loc[:,'Booking_Date_OBS'].shift(1)[INDEX] , 
                                             cruise = df.loc[INDEX , 'Key_Cruise_Code']  ,
                                             df_PB = df_PB)
    if tmp[2] > 2:
        df.loc[INDEX , 'Delay_PB_Changed_Days'] = tmp[0]
        df.loc[INDEX , 'previous_PB_value'] = tmp[1]

#********************************
#Missing Values
#********************************
#Continious Variables
#Remove lines with Current_PB NULL
df.loc[df['Current_PB'].isna() , 'Current_PB'] = 0.3
df.loc[df['Moyenne_PB'].isna(), 'Moyenne_PB'] = df.loc[df['Moyenne_PB'].isna(), 'Current_PB'] * -1 
#
df['FCST_MODELE_PU_DEFORMATION'] = df['FCST_MODELE_PU_DEFORMATION'].fillna(1)

#Remplacer les valeurs manquantes en 0 pour certaines valeurs manquantes
nan_to_zeros_var = list(df.dtypes[(df.dtypes != 'object' ) & (df.dtypes !='datetime64[ns]')].index)
df[nan_to_zeros_var] = df[nan_to_zeros_var].fillna(0)

#Nombre de Valeurs manquantes par colonnes
df.isnull().sum()

df0= df.copy()

#**************************************
#**********Add variables***************
#**************************************
#Categorical Variables
Themes_dic = {  'Gastronomy': 'Gastronomy',
                'Food & Wine' : 'Gastronomy',
                'Wine Tasting' : 'Gastronomy',
                "Gastronomy - Kid's Club" : 'Gastronomy',
                'QC - Food & Wine': 'Gastronomy',
                 
                'Loyalty' : 'Loyalty',
                'Chairman Cruise' : 'Loyalty',
                'AUS - Chairman Cruise' : 'Loyalty',
                
                'Classical Music' : 'Classical_Music',
                'QC - Music': 'Classical_Music',
                }

df['Theme'] = df['Mapping_Theme'].apply(lambda x: np.nan if x not in Themes_dic.keys() else Themes_dic[x])
df.loc[(~df['Mapping_Theme'].isna()) & (df['Theme'].isna()) , 'Theme'] = 'Other_Theme' 
df.loc[df['Theme'].isna() , 'Theme'] = 'No_Theme'
df = df.drop('Mapping_Theme' , axis = 1)

df.loc[~df['Mapping_Guest'].isna() , 'Mapping_Guest'] = 'With_Guest'
df.loc[df['Mapping_Guest'].isna() , 'Mapping_Guest'] = 'No_Guest'

df.loc[~df['Mapping_Partnership'].isna() , 'Mapping_Partnership'] = 'With_Partnership'
df.loc[df['Mapping_Partnership'].isna() , 'Mapping_Partnership'] = 'No_Partnership'

df['Mapping_Ship_Class'] = df['Mapping_Ship_Class'].str.upper()

#Coordonn�es FROM  -  TO
df['Mapping_From_To'] = df['Mapping_From_To'].str.replace(' ', '')
df['Mapping_From_To'] = df['Mapping_From_To'].str.replace('Fort-de-France/PontaDelgada', 'Fort de France-PontaDelgada')
df['FROM'] = df['Mapping_From_To'].str.split('-').apply(lambda x: x[0])
df['FROM'] = df['FROM'].apply(lambda x: re.sub(r"(\w)([A-Z])", r"\1 \2", x))
df['TO'] = df['Mapping_From_To'].str.split('-').apply(lambda x: x[1])
df['TO'] = df['TO'].apply(lambda x: re.sub(r"(\w)([A-Z])", r"\1 \2", x))

df['FROM'] = df['FROM'].str.replace('de ', ' de ')
df['TO'] = df['TO'].str.replace('de ', ' de ')

df['FROM'] = df['FROM'].str.replace('� ', ' � ')
df['TO'] = df['TO'].str.replace('� ', ' � ')

cities = list(df['FROM'].unique()) + list(df['TO'].unique() )
latitude = []
longitude = []

for city in cities:
    lat, long = get_city_coordinates(city)
    latitude.append(lat)
    longitude.append(long)

cities = pd.DataFrame({
        'city': cities,
        'latitude_FROM' : latitude,
        'longitude_FROM': longitude})    
cities = cities.drop_duplicates(subset = ['city'])
df = pd.merge(left = df,
              right = cities,
              left_on = 'FROM',
              right_on = 'city',
              how = 'left')

df.drop(['city'] , axis = 1, inplace = True)
cities.columns = ['city' , 'latitude_TO', 'longitude_TO']

df = pd.merge(left = df,
              right = cities,
              left_on = 'TO',
              right_on = 'city',
              how = 'left')

df.drop(['city'] , axis = 1, inplace = True)
    
#Dur�e depuis l'ouverture de vente
df['duration_since_first_reservation']  = (df['Booking_Date_OBS']  - df['Date_Premiere_Reservation_Indiv']).dt.days
#Diff�rence entre le FCST et les r�servations
df['Ecart_PB_30'] = df['FCST_PB30']  - df['Total_Cab_PB_30'] 
df['Ecart_PB_25'] = df['FCST_PB25']  - df['Total_Cab_PB_25'] 
df['Ecart_PB_20'] = df['FCST_PB20']  - df['Total_Cab_PB_20']
df['Ecart_PB_15'] = df['FCST_PB15']  - df['Total_Cab_PB_15']
df['Ecart_PB_05'] = df['FCST_PB5']  - df['Total_Cab_PB_5']
df['Ecart_PB_0'] = df['FCST_PB0']  - df['Total_Cab_PB_0']

#Departure Month
df['Month'] = df['Mapping_Date_Departure'].dt.month
df['Year'] = df['Mapping_Date_Departure'].dt.year

def get_Last_Total_Cab_PB_Column(PB_Value) :
    total_cab_columns =  ['Total_Cab_PB_30',
                          'Total_Cab_PB_25',
                          'Total_Cab_PB_20',
                          'Total_Cab_PB_15',
                          'Total_Cab_PB_10',
                          'Total_Cab_PB_5',
                          'Total_Cab_PB_0']
    name = 'Total_Cab_PB_'  + str(int(PB_Value *100))
    Last_Total_Cab_PB_Column = [col for col in total_cab_columns if name in col]
    if len(Last_Total_Cab_PB_Column) == 1:
        return Last_Total_Cab_PB_Column
    else:
        return np.nan
df[['']]

categorical_variables = [var for var in list(df.dtypes[df.dtypes == 'object'].index) if var not in ['Key_Cruise_Code'] ]
datetime_variables = list(df.dtypes[df.dtypes == 'datetime64[ns]'].index)
continious_variables = [col for col in list(df.columns) 
                        if(col not in(categorical_variables + datetime_variables)) ]

df0 = df.copy()


df=df0.copy()

df = pd.concat([
        df,
        pd.get_dummies(df['Mapping_Season']),
        pd.get_dummies(df['Mapping_FCST_Model'], prefix = 'Model'),
        pd.get_dummies(df['Mapping_Destination'] , prefix = 'Destination'),
        pd.get_dummies(df['Mapping_Ship_Class'] , prefix = 'Ship_class'),
        pd.get_dummies(df['Mapping_Guest'] , prefix = 'Guest'),
        pd.get_dummies(df['Mapping_Partnership'] , prefix = 'Partnership'),
        pd.get_dummies(df['Theme'] , prefix = 'Theme')
        ], axis = 1)
df.drop(categorical_variables , axis = 1, inplace = True)
df.drop( ['Mapping_Date_Departure',
          'Date_Premiere_Reservation_Indiv',
          'Date_Premiere_Option_Indiv Option',
          'PB_Changed_Date',
          'FCST_DATE_CREATION',
          'PB_CHANGED',
          'Delay_PB_Changed_Days',
          'Current_SupSingle',
          
          'Moyenne_PerDiem_Pax_EUR',
          'Moyenne_Panier_Pax_EUR',
          'Part_Pax_Paying_Repeaters',
          
         'Total_Cab_PB_30',
         'Total_Cab_PB_25',
         'Total_Cab_PB_20',
         'Total_Cab_PB_15',
         'Total_Cab_PB_10',
         'Total_Cab_PB_5',
         'Total_Cab_PB_0',
         'Cluster_Moyenne_Moyenne_PB',

         'FCST_PB30',
         'FCST_PB25',
         'FCST_PB20',
         'FCST_PB15',
         'FCST_PB10',
         'FCST_PB5',
         'FCST_PB0',
         
         'Ecart_PB_30',
         'Ecart_PB_25',
         'Ecart_PB_20',
         'Ecart_PB_15',
         'Ecart_PB_05',
         'Ecart_PB_0',

          ] , axis = 1, inplace = True)

df = df.dropna()




target = ['Current_PB']
predictors = [col for col in list(df.columns) if (col not in (target + datetime_variables + ['Key_Cruise_Code'] )) ]
df[target] = df[target].astype('float') 




train = df[df['Booking_Date_OBS'] <  '2018-07-15']
test = df[df['Booking_Date_OBS'] >=  '2018-07-15']





params = {    
#Boosting parameters
    'boosting_type': [ 'dart' , 'gbdt'],
    'objective': ['regression'],
    'metric': ['auc'],
    
    'n_estimators' : [120, 130, 140, 150, 200, 220, 250, 300, 350, 400],
    'max_bin': [ 225 , 255 , 285],
    'min_data_in_bin': [3 , 6 , 9],
    'learning_rate': [0.08, 0.1, 0.12],

    'feature_fraction': [0,6 , 0,7 , 0.8 , 0.9],
    'bagging_fraction': [0.6, 0.7, 0,8, 0.9],
    'bagging_freq': [0,  5, 10],
    
 #Tree specific Parameters   
    'num_leaves': [15, 20 , 25 , 30, 35, 40 ],
    'min_data_in_leaf': [3, 5, 10 , 15],
    'max_depth': [7, 9 , 11 , 13],
        }







params = {    
#Boosting parameters
    'boosting_type': [ 'dart' , 'gbdt'],
    'objective': ['regression'],
    'metric': ['auc'],
    
    'n_estimators' : [120, 130, 140, 150, 200, 220, 250],
    'max_bin': [ 225 , 255 , 285],
    'min_data_in_bin': [3 , 6 , 9],
    'learning_rate': [0.08, 0.1, 0.12],

    'feature_fraction': [ 0.8 , 0.9, 0.95],
  #  'bagging_fraction': [ 0.7, 0,8, 0.9],
    'bagging_freq': [0,  5, 10],
    
 #Tree specific Parameters   
    'num_leaves': [15, 20 , 25 , 30, 35, 40 ],
    'min_data_in_leaf': [3, 5, 10 , 15],
    'max_depth': [7, 9 , 11 , 13],
        }

model = lgb.LGBMRegressor(
          boosting_type= params['boosting_type'],
          objective = params['objective'],
          metric = params['metric'],


          n_estimators = params['n_estimators'],          
          max_bin = params['max_bin'],
          min_data_in_bin = params['min_data_in_bin'],
          learning_rate = params['learning_rate'],          

#          feature_fraction = params['feature_fraction'],
#          bagging_fraction = params['bagging_fraction'],
          bagging_freq = params['bagging_freq'],          
          
          num_leaves = params['num_leaves'],
          min_data_in_leaf = params['min_data_in_leaf'],
          max_depth = params['max_depth']
           )



# Create the grid
grid = RandomizedSearchCV(model, params,
                    verbose=10,
                    cv=3,
                    n_jobs= -1)
# Run the grid
grid.fit(np.array(train[predictors]), np.ravel(train[target]) )



mdl = lgb.LGBMRegressor(bagging_fraction=0.6, bagging_freq=5, boosting_type='gbdt',
       class_weight=None, colsample_bytree=1.0, feature_fraction=0.9,
       importance_type='split', learning_rate=0.08, max_bin=255,
       max_depth=9, metric='auc', min_child_samples=20,
       min_child_weight=0.001, min_data_in_bin=9, min_data_in_leaf=10,
       min_split_gain=0.0, n_estimators=100, n_jobs=-1, num_leaves=20,
       objective='regression', random_state=None, reg_alpha=0.0,
       reg_lambda=0.0, silent=True, subsample=1.0,
       subsample_for_bin=200000, subsample_freq=0)


lgb_train = lgb.Dataset(
    train[predictors], 
    train[target], 
    feature_name=predictors,
    )
lgb_train.raw_data = None

lgb_valid = lgb.Dataset(
    test[predictors], 
    test[target], 
    )
lgb_valid.raw_data = None

start_time = datetime.datetime.now()

model = lgb.train(
    grid.best_estimator_.get_params(),
    lgb_train,
    num_boost_round=10000,
    valid_sets=[lgb_train, lgb_valid],
    early_stopping_rounds=100,
    verbose_eval=100,
)
print("training duration : ", datetime.datetime.now() - start_time)

test['prediction'] = model.predict(test[predictors])
test['prediction']  = test['prediction'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
mae = mean_absolute_error(test[target] , test['prediction'])
print('mae: ', mae)


def lgb_plot_importance(lgb_model, predictors, nlargest=0 , nsmallest =0,  figsize = (15,25), **kwargs): 
    df = pd.DataFrame(  lgb_model.feature_importance())
    df.index = predictors
    df.columns = ['Feature_importance']
    df_smallest = df.nsmallest(nlargest,  columns = 'Feature_importance').sort_values('Feature_importance', ascending = False)
    df_largest = df.nlargest(nlargest,  columns = 'Feature_importance')
    df = pd.concat([df_largest , df_smallest] , axis = 0)
    df.plot(kind = 'bar', figsize = figsize)
    
lgb_plot_importance(model, predictors, 20, 20 ,  figsize = (20,10))


def predict_PB_for_one_cruise( df , cruise  , grid):      
    print(cruise)
    categorical_variables = [var for var in list(df.dtypes[df.dtypes == 'object'].index) if var not in ['Key_Cruise_Code'] ]
    datetime_variables = list(df.dtypes[df.dtypes == 'datetime64[ns]'].index)
    continious_variables = [col for col in list(df.columns) 
                            if(col not in(categorical_variables + datetime_variables)) ]
    target = ['Current_PB']
    predictors = [col for col in list(df.columns) if (col not in (target + datetime_variables + ['Key_Cruise_Code'] )) ]
    train = df[df['Key_Cruise_Code'] != cruise]
    test = df[df['Key_Cruise_Code'] ==  cruise]
    lgb_train = lgb.Dataset(
        train[predictors], 
        train[target], 
        feature_name=predictors,
        )
    lgb_train.raw_data = None
    
    lgb_valid = lgb.Dataset(
        test[predictors], 
        test[target], 
        )
    lgb_valid.raw_data = None
    
    start_time = datetime.datetime.now()
   # eval = {}
    model = lgb.train(
        grid.best_estimator_.get_params(),
        lgb_train,
        num_boost_round=10000,
        valid_sets=[lgb_train, lgb_valid],
      #  early_stopping_rounds=100,
        verbose_eval=100,
    )
    print("training duration : ", datetime.datetime.now() - start_time)
    
    test['prediction'] = model.predict(test[predictors])
    test['prediction']  = test['prediction'].apply(lambda x: int(5 * round(float(100*x)/5))/100 )
#    df.loc[test.index , 'prediction'] = test['prediction']
    mae = mean_absolute_error(test[target] , test['prediction'])
    print('mae: ', mae)
    test.index = test['Booking_Date_OBS']
    test[[ 'Current_PB' , 'prediction' ]].plot()
    plt.show()
    return np.array(test['prediction'])

    



#Test par croisi�re
train = df[df['Key_Cruise_Code'] != 'A020619']
test = df[df['Key_Cruise_Code'] ==  'A020619']
params = {    
#Boosting parameters
    'boosting_type': [ 'dart' , 'gbdt'],
    'objective': ['regression'],
    'metric': ['auc'],
    
    'n_estimators' : [90,100, 110, 120, 130, 140, 150],
    'max_bin': [ 225 , 255 , 285],
    'min_data_in_bin': [3 , 6 , 9],
    'learning_rate': [0.08, 0.1, 0.12],

    'feature_fraction': [0.8 , 0.9, 0.95],
    'bagging_fraction': [0.6, 0.7, 0.9],
    'bagging_freq': [0,  5, 10],
    
 #Tree specific Parameters   
    'num_leaves': [15, 20 , 25 , 30],
    'min_data_in_leaf': [3, 5, 10],
    'max_depth': [9],
        }

model = lgb.LGBMRegressor(
          boosting_type= params['boosting_type'],
          objective = params['objective'],
          metric = params['metric'],


          n_estimators = params['n_estimators'],          
          max_bin = params['max_bin'],
          min_data_in_bin = params['min_data_in_bin'],
          learning_rate = params['learning_rate'],          

           feature_fraction = params['feature_fraction'],
           bagging_fraction = params['bagging_fraction'],
           bagging_freq = params['bagging_freq'],          
          
          num_leaves = params['num_leaves'],
          min_data_in_leaf = params['min_data_in_leaf'],
          max_depth = params['max_depth']
           )
# Create the grid
grid = RandomizedSearchCV(model, params,
                    verbose=10,
                    cv=3,
                    n_jobs= -1)
# Run the grid
grid.fit(np.array(train[predictors]), np.ravel(train[target]) , 
         #groups = train['Key_Cruise_Code']
         )
df['prediction'] = 99
for cruise in list(df['Key_Cruise_Code'].unique()):
    df.loc[df['Key_Cruise_Code'] == cruise ,'prediction'] = predict_PB_for_one_cruise(df, cruise, grid)
mean_absolute_error(df[target] , df['prediction'])
  #0.0118  



df_pb_levels = df[[ 'Key_Cruise_Code' ,  'Current_PB']].groupby('Key_Cruise_Code').agg({ 'Current_PB':'nunique'})
cst_cruise = list(df_pb_levels[df_pb_levels['Current_PB'] == 1].index)
for crs in cst_cruise:
    predict_PB_for_one_cruise(df, crs, grid)

rmv_cruises = [
               'P180220',
               'P250220',
               'P030320',
               'P100320',
               'P170320',
               'P240320',
               #'P310320'
               ]
predict_PB_for_one_cruise( df.loc[~df['Key_Cruise_Code'].isin( rmv_cruises)]   , 
                          'P310320', grid)