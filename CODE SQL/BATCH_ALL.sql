
/*Rack_PR4*/
SET ARITHABORT OFF 
SET ANSI_WARNINGS OFF

IF OBJECT_ID('tempdb..[###Rack_PR4]') IS NOT NULL 
DROP TABLE [###Rack_PR4]
GO
print '[###Rack_PR4]'

SELECT dbo.vRateGrid.Cruise_Code,MAX(dbo.vRateGrid.Rate) AS Rack_Price_Cruise_Only_EUR,
ROUND(MAX(dbo.vRateGrid.Rate)/Max(LOS),0) AS Rack_PerDiem_Cruise_Only_EUR
INTO [###Rack_PR4]
FROM dbo.vRateGrid
INNER JOIN Cruise ON dbo.vRateGrid.Cruise_Code = Cruise.Cruise_Code
WHERE 
dbo.vRateGrid.Currency='EUR' AND 
dbo.vRateGrid.Cabin_Code_Pricing IN ('PR4','MAG') AND 
dbo.vRateGrid.Ponant_Bonus_Code='A' AND
dbo.vRateGrid.Occupancy_Type='Double'AND
dbo.vRateGrid.Revenue_Type = 'Cruise'

GROUP BY dbo.vRateGrid.Cruise_Code;


/*Revenue_Max_par_cruise*/
IF OBJECT_ID('tempdb..[###Revenue_Max_par_cruise]') IS NOT NULL 
DROP TABLE [###Revenue_Max_par_cruise]
GO
print '[###Revenue_Max_par_cruise]'

SELECT Revenu_Max_par_cabine.Cruise_Code, Revenu_Max_par_cabine.Cruise_Status, Revenu_Max_par_cabine.From_To, Revenu_Max_par_cabine.LOS, Sum(Revenu_Max_par_cabine.Revenu_Max) AS SommeDeRevenu_Max
INTO [###Revenue_Max_par_cruise]
FROM 
(
/*Revenue Max par cabine*/
SELECT Cabin_Capacity.Cruise_Code, Cabin_Capacity.Cruise_Status, Cabin_Capacity.From_To, Cabin_Capacity.LOS, Cabin_Capacity.Cabin_Code_IBS, Cabin_Capacity.Cabin_NO, Cabin_Capacity.Capacity_Cabin, Cabin_Capacity.NB_PAX_Adult, Sum(dbo.vRateGrid.Rate) AS SommeDeRate, Sum(dbo.vRateGrid.Rate)*Max([Cabin_Capacity].[Capacity_Cabin])*Max([Cabin_Capacity].[NB_PAX_Adult]) AS Revenu_Max
FROM 
(/*Cabin_Capacity*/
SELECT dbo.cruise.Cruise_Code, dbo.cruise.Cruise_Status, dbo.CabinNumber.Cabin_NO, dbo.CabinNumber.Cabin_Code_IBS, dbo.cruise.From_To, dbo.cruise.LOS, dbo.CabinNumber.Capacity_Cabin, dbo.CabinNumber.NB_PAX_Adult
FROM dbo.CabinNumber INNER JOIN dbo.cruise ON dbo.CabinNumber.Vessel = dbo.cruise.Vessel
GROUP BY dbo.cruise.Cruise_Code, dbo.cruise.Cruise_Status, dbo.CabinNumber.Cabin_NO, dbo.CabinNumber.Cabin_Code_IBS, dbo.cruise.From_To, dbo.cruise.LOS, dbo.CabinNumber.Capacity_Cabin, dbo.CabinNumber.NB_PAX_Adult
) Cabin_Capacity 
INNER JOIN dbo.vRateGrid ON (Cabin_Capacity.Cabin_Code_IBS = dbo.vRateGrid.Cabin_Code_Pricing) AND (Cabin_Capacity.Cruise_Code = dbo.vRateGrid.Cruise_Code)
WHERE (((dbo.vRateGrid.Ponant_Bonus_Code)='A') AND ((dbo.vRateGrid.Currency)='EUR') AND ((dbo.vRateGrid.Occupancy_Type)='Double') AND ((dbo.vRateGrid.Revenue_Type)='Cruise' Or (dbo.vRateGrid.Revenue_Type)='Port charges'))
GROUP BY Cabin_Capacity.Cruise_Code, Cabin_Capacity.Cruise_Status, Cabin_Capacity.From_To, Cabin_Capacity.LOS, Cabin_Capacity.Cabin_Code_IBS, Cabin_Capacity.Cabin_NO, Cabin_Capacity.Capacity_Cabin, Cabin_Capacity.NB_PAX_Adult
) Revenu_Max_par_cabine
GROUP BY Revenu_Max_par_cabine.Cruise_Code, Revenu_Max_par_cabine.Cruise_Status, Revenu_Max_par_cabine.From_To, Revenu_Max_par_cabine.LOS;


/*Dernier_Booking_Cruise_Non_Active*/
IF OBJECT_ID('tempdb..[###Dernier_Booking_Cruise_Non_Active]') IS NOT NULL 
DROP TABLE [###Dernier_Booking_Cruise_Non_Active]
GO
print '[###Dernier_Booking_Cruise_Non_Active]'


SELECT dbo.vTableCentraleDeReservations.Booking_Date_OBS, dbo.cruise.Cruise_Status, dbo.vTableCentraleDeReservations.Cruise_Code, Max(dbo.vTableCentraleDeReservations.Booking_Created_Date) AS MaxDeBooking_Created_Date
INTO [###Dernier_Booking_Cruise_Non_Active]
FROM dbo.vTableCentraleDeReservations INNER JOIN dbo.cruise ON dbo.vTableCentraleDeReservations.Cruise_Code = dbo.cruise.Cruise_Code
WHERE (((dbo.vTableCentraleDeReservations.Client_Type)='INDIV'))
GROUP BY dbo.vTableCentraleDeReservations.Booking_Date_OBS, dbo.cruise.Cruise_Status, dbo.vTableCentraleDeReservations.Cruise_Code
HAVING (((dbo.vTableCentraleDeReservations.Booking_Date_OBS)=CAST(GETDATE() AS DATE)) AND ((dbo.cruise.Cruise_Status)='NON ACTIVE'));



/*Cruise with special pricing*/
IF OBJECT_ID('tempdb..[###Cruise_with_Special_Pricing]') IS NOT NULL 
DROP TABLE [###Cruise_with_Special_Pricing]
GO
print '[###Cruise_with_Special_Pricing]'


SELECT dbo.cruise.Year_Departure, dbo.vRateGrid.Cruise_Code
INTO [###Cruise_with_Special_Pricing]
FROM dbo.cruise INNER JOIN dbo.vRateGrid ON dbo.cruise.Cruise_Code = dbo.vRateGrid.Cruise_Code
WHERE (((dbo.vRateGrid.Ponant_Bonus_Code)<>'A' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'B' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'C' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'D' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'E' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'F' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'G' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'U' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'S' And (dbo.vRateGrid.Ponant_Bonus_Code)<>'T'))
GROUP BY dbo.cruise.Year_Departure, dbo.vRateGrid.Cruise_Code;



/*D�finition d'un mapping season pour le traitement de l'histo*/
IF OBJECT_ID('tempdb..###Mapping_Season') IS NOT NULL 
DROP TABLE ###Mapping_Season
CREATE TABLE ###Mapping_Season 
(Season VARCHAR(128),
Season_Previous VARCHAR(128)
)

print '[###Mapping_Season]'

INSERT INTO ###Mapping_Season (Season,Season_Previous)
VALUES 
('SUMMER 2013','SUMMER 2012'),
('SUMMER 2014','SUMMER 2013'),
('SUMMER 2015','SUMMER 2014'),
('SUMMER 2016','SUMMER 2015'),
('SUMMER 2017','SUMMER 2016'),
('SUMMER 2018','SUMMER 2017'),
('SUMMER 2019','SUMMER 2018'),
('SUMMER 2020','SUMMER 2019'),
('SUMMER 2021','SUMMER 2020'),
('SUMMER 2022','SUMMER 2021'),
('SUMMER 2023','SUMMER 2022'),
('SUMMER 2024','SUMMER 2023'),
('SUMMER 2025','SUMMER 2024'),
('SUMMER 2026','SUMMER 2025'),
('SUMMER 2027','SUMMER 2026'),
('SUMMER 2028','SUMMER 2027'),
('SUMMER 2029','SUMMER 2028'),
('SUMMER 2030','SUMMER 2029'),
('SUMMER 2031','SUMMER 2030'),
('SUMMER 2032','SUMMER 2031'),
('SUMMER 2033','SUMMER 2032'),
('SUMMER 2034','SUMMER 2033'),
('SUMMER 2035','SUMMER 2034'),
('SUMMER 2036','SUMMER 2035'),
('SUMMER 2037','SUMMER 2036'),
('SUMMER 2038','SUMMER 2037'),
('SUMMER 2039','SUMMER 2038'),
('SUMMER 2040','SUMMER 2039'),
('SUMMER 2041','SUMMER 2040'),
('SUMMER 2042','SUMMER 2041'),
('SUMMER 2043','SUMMER 2042'),
('SUMMER 2044','SUMMER 2043'),
('SUMMER 2045','SUMMER 2044'),
('SUMMER 2046','SUMMER 2045'),
('SUMMER 2047','SUMMER 2046'),
('SUMMER 2048','SUMMER 2047'),
('SUMMER 2049','SUMMER 2048'),
('SUMMER 2050','SUMMER 2049'),
('WINTER 2012-2013','WINTER 2011-2012'),
('WINTER 2013-2014','WINTER 2012-2013'),
('WINTER 2014-2015','WINTER 2013-2014'),
('WINTER 2015-2016','WINTER 2014-2015'),
('WINTER 2016-2017','WINTER 2015-2016'),
('WINTER 2017-2018','WINTER 2016-2017'),
('WINTER 2018-2019','WINTER 2017-2018'),
('WINTER 2019-2020','WINTER 2018-2019'),
('WINTER 2020-2021','WINTER 2019-2020'),
('WINTER 2021-2022','WINTER 2020-2021'),
('WINTER 2022-2023','WINTER 2021-2022'),
('WINTER 2023-2024','WINTER 2022-2023'),
('WINTER 2024-2025','WINTER 2023-2024'),
('WINTER 2025-2026','WINTER 2024-2025'),
('WINTER 2026-2027','WINTER 2025-2026'),
('WINTER 2027-2028','WINTER 2026-2027'),
('WINTER 2028-2029','WINTER 2027-2028'),
('WINTER 2029-2030','WINTER 2028-2029'),
('WINTER 2030-2031','WINTER 2029-2030'),
('WINTER 2031-2032','WINTER 2030-2031'),
('WINTER 2032-2033','WINTER 2031-2032'),
('WINTER 2033-2034','WINTER 2032-2033'),
('WINTER 2034-2035','WINTER 2033-2034'),
('WINTER 2035-2036','WINTER 2034-2035'),
('WINTER 2036-2037','WINTER 2035-2036'),
('WINTER 2037-2038','WINTER 2036-2037'),
('WINTER 2038-2039','WINTER 2037-2038'),
('WINTER 2039-2040','WINTER 2038-2039'),
('WINTER 2040-2041','WINTER 2039-2040'),
('WINTER 2041-2042','WINTER 2040-2041'),
('WINTER 2042-2043','WINTER 2041-2042'),
('WINTER 2043-2044','WINTER 2042-2043'),
('WINTER 2044-2045','WINTER 2043-2044'),
('WINTER 2045-2046','WINTER 2044-2045'),
('WINTER 2046-2047','WINTER 2045-2046'),
('WINTER 2047-2048','WINTER 2046-2047'),
('WINTER 2048-2049','WINTER 2047-2048'),
('WINTER 2049-2050','WINTER 2048-2049')
;




/*D�finition d'un mapping de ponts sup/inf*/
IF OBJECT_ID('tempdb..###Mapping_Ponts_Inf_Sup') IS NOT NULL 
DROP TABLE ###Mapping_Ponts_Inf_Sup
CREATE TABLE ###Mapping_Ponts_Inf_Sup 
(Cabin_Code VARCHAR(128),
Pont VARCHAR(128)
)

print '[###Mapping_Ponts_Inf_Sup]'

INSERT INTO ###Mapping_Ponts_Inf_Sup (Cabin_Code,Pont)
VALUES 
('ANT', 'Inf'),
('DEL', 'Inf'),
('DS3', 'Inf'),
('DS4', 'Sup'),
('DS5', 'Sup'),
('DS6', 'Sup'),
('GAR', 'Inf'),
('GD5', 'Sup'),
('GD6', 'Sup'),
('GDS', 'Sup'),
('GDV', 'Sup'),
('GPV', 'Sup'),
('MAG', 'Inf'),
('MGP', 'Sup'),
('N/A', 'Inf'),
('PR4', 'Inf'),
('PR5', 'Sup'),
('PR6', 'Sup'),
('PS5', 'Sup'),
('PS6', 'Sup'),
('PV', 'Sup'),
('PV5', 'Sup'),
('PV6', 'Sup'),
('SA', 'Sup'),
('SP2', 'Inf'),
('SP3', 'Inf'),
('SP4', 'Inf') 
;


/*Table de mapping croisi�re*/
IF OBJECT_ID('tempdb..[###Mapping_ML_00]') IS NOT NULL 
DROP TABLE [###Mapping_ML_00]
GO

print '[###Mapping_ML_00]'

SELECT cruise.Cruise_Code, cruise.Ship_Class,

IIf(Ship_Class='PONANT','PONANT','NOT PONANT') AS Ship_Class_2,

 cruise.Charter, cruise.Vessel, cruise.LOS, cruise.Cruise_Status, cruise.Date_Departure, cruise.Year_Departure, cruise.From_To, cruise.Destination, cruise.Season, cruise.Theme, cruise.Guest, cruise.Partnership, cruise.Cluster_Pricing, cruise.FCST_Model, cruise.Destination_Semester, cruise.Destination_Production, cruise.Cluster_Pricing_2, vessel.Cabin_Capacity, '' AS Cluster_ML, '' AS Histo_ML, Sum(IIf(###Mapping_Ponts_Inf_Sup.[Pont]='Inf',[Capacity_Cabin],0)) AS Mapping_Cabin_Capacity_Ponts_Inf, Sum(IIf(###Mapping_Ponts_Inf_Sup.[Pont]='Sup',[Capacity_Cabin],0)) AS Mapping_Cabin_Capacity_Ponts_Sup,
cruise.FCST_Cluster AS FCST_Cluster
INTO [###Mapping_ML_00]
FROM (CabinNumber RIGHT JOIN (vessel RIGHT JOIN cruise ON vessel.Vessel_Name = cruise.Vessel) ON CabinNumber.Vessel = vessel.Vessel_Name) LEFT JOIN ###Mapping_Ponts_Inf_Sup ON CabinNumber.Cabin_Code_IBS = ###Mapping_Ponts_Inf_Sup.Cabin_Code
GROUP BY cruise.Cruise_Code,cruise.Ship_Class,IIf(Ship_Class='PONANT','PONANT','NOT PONANT') , cruise.Charter, cruise.Vessel, cruise.LOS, cruise.Cruise_Status, cruise.Date_Departure, cruise.Year_Departure, cruise.From_To, cruise.Destination, cruise.Season, cruise.Theme, cruise.Guest, cruise.Partnership, cruise.Cluster_Pricing, cruise.FCST_Model, cruise.Destination_Semester, cruise.Destination_Production, cruise.Cluster_Pricing_2, vessel.Cabin_Capacity,FCST_Cluster
HAVING (((cruise.Year_Departure)>=2013));


/*Permet de r�cup�rer le statut NC / R de chaque booking*/
IF OBJECT_ID('tempdb..[###Traveller_Booking]') IS NOT NULL 
DROP TABLE [###Traveller_Booking]
GO

print '[###Traveller_Booking]'

SELECT dbo.Traveller.Booking_NO, dbo.Traveller.Booking_Status_Code, dbo.Traveller.Cabin_Status_Code, Max(dbo.Traveller.Statut_NC_Booking) AS MaxDeStatut_NC_Booking, Max(dbo.Traveller.Statut_NC_Traveller) AS MaxDeStatut_NC_Traveller
INTO [###Traveller_Booking]
FROM dbo.Traveller
GROUP BY dbo.Traveller.Booking_NO, dbo.Traveller.Booking_Status_Code, dbo.Traveller.Cabin_Status_Code;


/*Calcul du pick-up group sur la derni�re semaine*/

IF OBJECT_ID('tempdb..[###Group]') IS NOT NULL 
DROP TABLE [###Group]
GO

print '[###Group]'


SELECT a.Booking_Date_OBS, a.Cruise_Code,
Sum(a.Cabin_Total) AS SommeDeCabin_Total
INTO [###Group]
FROM VTableCentraleDeReservations a
WHERE (((a.Booking_Status_Code)='BKD' Or (a.Booking_Status_Code)='INV' Or (a.Booking_Status_Code)='OPT' Or (a.Booking_Status_Code)='BKO' Or (a.Booking_Status_Code)='GPD' Or (a.Booking_Status_Code)='GBK') AND ((a.Cabin_Status_Code)='BKD' Or (a.Cabin_Status_Code)='GPD' Or (a.Cabin_Status_Code)='GBK' Or (a.Cabin_Status_Code)='GIP' Or (a.Cabin_Status_Code)='OPT') AND ((a.Client_Type)='GROUP'))

/*AND Cruise_Code = 'Y250220'*/

GROUP BY a.Booking_Date_OBS, a.Cruise_Code
;


IF OBJECT_ID('tempdb..[###GroupPU]') IS NOT NULL 
DROP TABLE [###GroupPU]
GO

print '[###GroupPU]'

SELECT a.Booking_Date_OBS AS Group_Booking_Date_OBS, 
a.cruise_code AS Group_Key_Cruise_Code,
a.SommeDeCabin_Total,
IIF(b.Booking_Date_OBS IS NULL,0,a.SommeDeCabin_Total - COALESCE(b.SommeDeCabin_Total,0))
 AS previous_week_SommeDeCabin_Total,

IIF(c.Booking_Date_OBS IS NULL,0,a.SommeDeCabin_Total - COALESCE(c.SommeDeCabin_Total,0))
 AS previous_1month_SommeDeCabin_Total
INTO [###GroupPU]
FROM [###Group] a
LEFT JOIN [###Group] b
 ON b.Booking_Date_OBS = DATEADD(DAY, 7, a.Booking_Date_OBS) 
 AND
 a.Cruise_Code = b.Cruise_Code
 LEFT JOIN [###Group] c
 ON c.Booking_Date_OBS = dateadd(week,datediff(week,0,DateAdd(mm, -1, a.Booking_Date_OBS)),0) 
AND
a.Cruise_Code = c.Cruise_Code
/*WHERE DATEPART(weekday,a.Booking_Date_OBS ) = 1*/
ORDER BY a.cruise_code, a.Booking_Date_OBS





/*Calcul des indicateurs OTB*/

IF OBJECT_ID('tempdb..[###OTB]') IS NOT NULL 
DROP TABLE [###OTB]
GO

print '[###OTB]'

SET ARITHABORT OFF 
SET ANSI_WARNINGS OFF

SELECT [###Mapping_ML_00].Vessel AS Mapping_Vessel,
[###Mapping_ML_00].Ship_Class AS Mapping_Ship_Class,
[###Mapping_ML_00].Ship_Class_2 AS Mapping_Ship_Class_2,
 [###Mapping_ML_00].LOS AS Mapping_LOS, [###Mapping_ML_00].Charter AS Mapping_Charter, [###Mapping_ML_00].Cruise_Status AS Mapping_Cruise_Status, [###Mapping_ML_00].Date_Departure AS Mapping_Date_Departure, [###Mapping_ML_00].Year_Departure AS Mapping_Year_Departure, [###Mapping_ML_00].From_To AS Mapping_From_To, [###Mapping_ML_00].Destination AS Mapping_Destination, [###Mapping_ML_00].Season AS Mapping_Season, [###Mapping_ML_00].Theme AS Mapping_Theme, [###Mapping_ML_00].Guest AS Mapping_Guest, [###Mapping_ML_00].Partnership AS Mapping_Partnership, [###Mapping_ML_00].Cluster_Pricing AS Mapping_Cluster_Pricing, [###Mapping_ML_00].FCST_Model AS Mapping_FCST_Model, 
 [###Mapping_ML_00].FCST_Cluster AS Mapping_FCST_Cluster,
 [###Mapping_ML_00].Destination_Semester AS Mapping_Destination_Semester, [###Mapping_ML_00].Destination_Production AS Mapping_Destination_Production, [###Mapping_ML_00].Cluster_Pricing_2 AS Mapping_Cluster_Pricing_2, [###Mapping_ML_00].Cabin_Capacity AS Mapping_Cabin_Capacity, [###Mapping_ML_00].Cluster_ML AS Mapping_Cluster_ML, [###Mapping_ML_00].Histo_ML AS Mapping_Histo_ML,-(DATEDIFF(day,[date_departure],dbo.vTableCentraleDeReservations.[Booking_Date_OBS])) AS Mapping_Lead_Time_Days, [###Mapping_ML_00].Cruise_Code AS Key_Cruise_Code, 
Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0 THEN dbo.VTableCentraleDeReservations.PAX_Paying END) AS Total_Pax_Paying,
 Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') THEN dbo.VTableCentraleDeReservations.Cabin_Paying END) AS Total_Cab_Paying,
  Sum(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC) AS Total_NTR_EUR,
   Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0 THEN dbo.vTableCentraleDeReservations.PAX_Paying END) * Max(LOS) AS Total_PCD_Paying,
      Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') THEN dbo.vTableCentraleDeReservations.Cabin_Paying END) * Max(LOS) AS Total_CCD_Paying,
	 Sum(Revenue_TAX_ENI_DNC)/(Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') THEN dbo.VTableCentraleDeReservations.Cabin_Paying END)*Max(LOS)) AS Moyenne_PerDiem_Cabine_EUR,
	 Sum(Revenue_TAX_ENI_DNC)/(Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') THEN dbo.VTableCentraleDeReservations.Cabin_Paying END)) AS Moyenne_Panier_Cabine_EUR,
	  Sum(Revenue_TAX_ENI_DNC)/(Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying))*Max(LOS)) AS Moyenne_PerDiem_Pax_EUR,
	  Sum(Revenue_TAX_ENI_DNC)/(Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying))) AS Moyenne_Panier_Pax_EUR,
	   Max([cabin_capacity])-Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') THEN dbo.VTableCentraleDeReservations.Cabin_Paying END) - Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type In ('PRODUIT','DECOME') And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Total],0))  AS Total_Dispo_Cabine,  Round((

ISNULL ( Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') THEN dbo.vTableCentraleDeReservations.Cabin_Paying END) ,0) * Max(LOS)
+
ISNULL ( Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('PRODUIT') THEN dbo.vTableCentraleDeReservations.Cabin_Total END),0) * Max(LOS))
/
(Max([Cabin_Capacity])*Max([LOS])),2)  AS Occupation_Rate_excl_OPT, 


Sum(IIf(dbo.VTableCentraleDeReservations.[PAX_Paying]=1 And dbo.VTableCentraleDeReservations.Client_Type='INDIV',1,0)) AS Nombre_Singles, Round(Avg(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV',[Booking_PB_Applied_For_Erosion],Null)),2) AS Moyenne_PB, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=-0.3,[Cabin_Paying],Null)) AS Total_Cab_PB_30, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=-0.25,[Cabin_Paying],Null)) AS Total_Cab_PB_25, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=-0.2,[Cabin_Paying],Null)) AS Total_Cab_PB_20, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=-0.15,[Cabin_Paying],Null)) AS Total_Cab_PB_15, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=-0.1,[Cabin_Paying],Null)) AS Total_Cab_PB_10, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=-0.05,[Cabin_Paying],Null)) AS Total_Cab_PB_5, Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status]<>'CXL' And dbo.VTableCentraleDeReservations.Client_Type='INDIV' And [Booking_PB_Applied_For_Erosion]=0,[Cabin_Paying],Null)) AS Total_Cab_PB_0, Min(IIf(Client_Type='INDIV',[Booking_Created_Date],Null)) AS Date_Premiere_Reservation_Indiv, Max([Cabin_Capacity])*Max([LOS]) AS Total_ACCD, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type In ('PRODUIT','DECOME') And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Total],0)) AS Total_Cabin_Decom_Produit, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Total_Cabin_Paying_Indiv, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0,dbo.VTableCentraleDeReservations.[Pax_Paying],0)) AS Total_Pax_Paying_Indiv, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0,dbo.VTableCentraleDeReservations.[Pax_Paying],0)) AS Total_Pax_Paying_Group, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.vTableCentraleDeReservations.Cabin_Paying,0))*MAX(LOS) AS Total_CCD_Paying_Indiv, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' /*And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL'*/,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,0)) AS Total_NTR_EUR_INDIV, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' /*And dbo.VTableCentraleDeReservations.[Booking_Status_Code] IN ('INV','BKD','CXL')*/,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,0)) AS Total_NTR_EUR_Group, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' /*And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL'*/,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,0))/(Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.vTableCentraleDeReservations.Cabin_Paying,0))*MAX(LOS)) AS [Moyenne_Perdiem_Cabine_EUR_Indiv], 

Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' /*And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL'*/,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,0))/(Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' /*And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL'*/,dbo.vTableCentraleDeReservations.Cabin_Paying,0))*MAX(LOS)) AS [Moyenne_Perdiem_Cabine_EUR_Group], 



Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,0))/Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Moyenne_Panier_Cabin_EUR_Indiv, Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='GBK',[Cabin_Total],Null)) AS Total_Cabin_GIP_Group, Sum(IIf([source_pax_2]='FBS',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0)) AS Total_Pax_Paying_FBS, Round(Sum(IIf([source_pax_2]='FBS',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0))/Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying)),2) AS Part_de_Pax_Paying_FBS, Sum(IIf(dbo.VTableCentraleDeReservations.[Sales_Distribution_Channel]='direct',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0)) AS Total_Pax_Paying_Direct, 

Round(

Sum(IIf(dbo.VTableCentraleDeReservations.[Sales_Distribution_Channel]='direct',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0))

/

Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0,dbo.VTableCentraleDeReservations.[Pax_Paying],0))


,2) 


AS Part_Pax_Paying_Direct,

 Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Source]='RES' And dbo.VTableCentraleDeReservations.[Sales_Office_Code]='SOB',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0)) AS Total_Pax_Paying_SOB, Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Source]='B2C',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0)) AS Total_Pax_Paying_Web,
 
  Round(Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Source]='RES' And dbo.VTableCentraleDeReservations.[Sales_Office_Code]='SOB',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0))/Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0,dbo.VTableCentraleDeReservations.[Pax_Paying],0)),2) AS Part_Pax_Paying_SOB,
  
  
   Round(Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Source]='B2C',[pax_paying],0))/Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0,dbo.VTableCentraleDeReservations.[Pax_Paying],0)),2) AS Part_Pax_Paying_Web,
   
   
   
    Sum(IIf([###Traveller_Booking].[MaxDeStatut_NC_Booking]='Repeater',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0)) AS Total_Pax_Paying_Repeaters, 
	
	Round(Sum(IIf([###Traveller_Booking].[MaxDeStatut_NC_Booking]='Repeater',IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying),0))/Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.[Pax_Paying]>=0,dbo.VTableCentraleDeReservations.[Pax_Paying],0)),2) AS Part_Pax_Paying_Repeaters, 

Sum(
IIF(dbo.VTableCentraleDeReservations.Client_Type IN ('Indiv','GROUP'),
IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC AND dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR)
,
0
)
) 


AS Total_NTR_Max,

 1-Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.Client_Type IN ('Indiv','GROUP') ,Revenue_TAX_ENI_DNC,0))/Sum(IIf(dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL' AND dbo.VTableCentraleDeReservations.Client_Type IN ('Indiv','GROUP'),IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC AND dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR),0)) AS Erosion_OTB,
 
  Sum(Revenue_TAX_ENI_DNC)/(Max([LOS])*Max([cabin_capacity])) AS Yield, dbo.VTableCentraleDeReservations.Booking_Date_OBS, Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_First_Booked_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-1) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='paid' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='BKD' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Pick_Up_Indiv_Pos_1d, Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_First_Booked_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-3) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='paid' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='BKD' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Pick_Up_Indiv_Pos_3d, Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_First_Booked_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-7) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='paid' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='BKD' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Pick_Up_Indiv_Pos_1w,
Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_First_Booked_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-14) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='paid' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='BKD' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Pick_Up_Indiv_Pos_2w,
Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_First_Booked_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-7*4) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='paid' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='BKD' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Pick_Up_Indiv_Pos_1m,
Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_First_Booked_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-7*4*3) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='paid' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='BKD' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',dbo.VTableCentraleDeReservations.[Cabin_Paying],0)) AS Pick_Up_Indiv_Pos_3m,

 Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code_Last_Changed_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-1) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='cancelled' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',-1,0)) AS Pick_Up_Indiv_Neg_1d, Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code_Last_Changed_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-3) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='cancelled' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',-1,0)) AS Pick_Up_Indiv_Neg_3d, Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code_Last_Changed_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-7) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='cancelled' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',-1,0)) AS Pick_Up_Indiv_Neg_1w, 
  Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code_Last_Changed_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-14) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='cancelled' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',-1,0)) AS Pick_Up_Indiv_Neg_2w,
 Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code_Last_Changed_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-7*4) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='cancelled' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',-1,0)) AS Pick_Up_Indiv_Neg_1m,
 Sum(IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code_Last_Changed_Date]>=(dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-7*4*3) And dbo.VTableCentraleDeReservations.[Booking_Status_GP]='cancelled' And dbo.VTableCentraleDeReservations.Client_Type='Indiv',-1,0)) AS Pick_Up_Indiv_Neg_3m,

 Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Total],0)) AS Total_Cabin_Total_Group,
 
Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Total],0)) * MAX(LOS) AS Total_CCD_Total_Group, 
 
 1-(Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',Revenue_TAX_ENI_DNC,0))/Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR),0))) AS Erosion_OTB_Indiv, 

 Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Indiv' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR),0)) AS Total_NTR_MAX_INDIV,
 
 1-(Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',Revenue_TAX_ENI_DNC,0))/Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR),0))) AS Erosion_OTB_Group, 


 Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',/*IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,*/dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR/*)*/,0)) AS Total_NTR_MAX_GROUP,
 
 Round((
 
 ISNULL (Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') AND [Pont]='Sup' THEN dbo.vTableCentraleDeReservations.Cabin_Paying END),0) * Max(LOS)
+
ISNULL (Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('PRODUIT') AND [Pont]='Sup' THEN dbo.vTableCentraleDeReservations.Cabin_Total END),0) * Max(LOS))
/
(Max([Mapping_Cabin_Capacity_Ponts_Sup])*Max([LOS])),2) AS Occupation_Rate_Ponts_Sup_excl_OPT, 

Round((

ISNULL (Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('INDIV','GROUP') AND [Pont]='Inf' THEN dbo.vTableCentraleDeReservations.Cabin_Paying END),0) * Max(LOS)
+
ISNULL (Sum(CASE WHEN dbo.vTableCentraleDeReservations.Client_Type IN ('PRODUIT') AND [Pont]='Inf' THEN dbo.vTableCentraleDeReservations.Cabin_Total END),0) * Max(LOS))
/
(Max([Mapping_Cabin_Capacity_Ponts_Inf])*Max([LOS])),2) AS Occupation_Rate_Ponts_Inf_excl_OPT,
Sum(IIf([Pont]='Sup',[cabin_paying],0)) AS Total_Cabin_Paying_Sup,
Sum(IIf([Pont]='Inf',[cabin_paying],0)) AS Total_Cabin_Paying_Inf,
Max([###Mapping_ML_00].[Mapping_Cabin_Capacity_Ponts_Inf]) AS Mapping_Cabin_Capacity_Ponts_Inf, 
Max([###Mapping_ML_00].[Mapping_Cabin_Capacity_Ponts_Sup]) AS Mapping_Cabin_Capacity_Ponts_Sup,
/*Erosion OTB SINGLE,OFFRE,COMM*/

1 - Sum(
IIF(vTableCentraleDeReservations.Booking_Status_Code IN ('INV','BKD') AND vTableCentraleDeReservations.Client_Type = 'Indiv',REVENUE_TAX_ENI_DNC_COM_DC_OV_EUR,0)
) 

/ Sum(
IIF(vTableCentraleDeReservations.Booking_Status_Code IN ('INV','BKD') AND vTableCentraleDeReservations.Client_Type = 'Indiv',REVENUE_TAX_ENI_DNC_COM_DC_OV_SS_EUR,0)
) AS Erosion_Single_OTB,

1 - Sum(
IIF(vTableCentraleDeReservations.Booking_Status_GP = 'paid' AND vTableCentraleDeReservations.Client_Type_GP = 'Indiv',Revenue_TAX_ENI_DNC_COM_EUR,0)
) 

/ Sum(
IIF(vTableCentraleDeReservations.Booking_Status_GP = 'paid' AND vTableCentraleDeReservations.Client_Type_GP = 'Indiv',Revenue_TAX_ENI_DNC_COM_DC_EUR,0)
) AS Erosion_Offre_OTB,

1 - Sum(
IIF(vTableCentraleDeReservations.Booking_Status_GP = 'paid' AND vTableCentraleDeReservations.Client_Type_GP = 'Indiv',Revenue_TAX_ENI_DNC_EUR,0)
) 

/ Sum(
IIF(vTableCentraleDeReservations.Booking_Status_GP = 'paid' AND vTableCentraleDeReservations.Client_Type_GP = 'Indiv',Revenue_TAX_ENI_DNC_COM_EUR,0)
) AS Erosion_Commision_OTB,

MAX(###Rack_PR4.Rack_PerDiem_Cruise_Only_EUR) AS Rack_PR4_PerDiem_Cruise_Only_EUR ,
MAX(###Rack_PR4.Rack_Price_Cruise_Only_EUR) AS Rack_PR4_Price_Cruise_Only_EUR ,

Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]='GBK',[Cabin_Total],Null))
/
Sum(IIf(dbo.VTableCentraleDeReservations.Client_Type='Group' And dbo.VTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',dbo.VTableCentraleDeReservations.[Cabin_Total],0))

 AS Part_De_GIP



INTO [###OTB]
FROM (((dbo.VTableCentraleDeReservations INNER JOIN [###Mapping_ML_00] ON dbo.VTableCentraleDeReservations.Cruise_Code = [###Mapping_ML_00].Cruise_Code) LEFT JOIN Country ON dbo.VTableCentraleDeReservations.Client_Country = Country.Client_Country_Corrected) LEFT JOIN [###Traveller_Booking] ON (dbo.VTableCentraleDeReservations.Cabin_Status_Code = [###Traveller_Booking].Cabin_Status_Code) AND (dbo.VTableCentraleDeReservations.Booking_Status_Code = [###Traveller_Booking].Booking_Status_Code) AND (dbo.VTableCentraleDeReservations.Booking_No = [###Traveller_Booking].Booking_NO)) INNER JOIN ###Mapping_Ponts_Inf_Sup ON dbo.VTableCentraleDeReservations.Cabin_Code = ###Mapping_Ponts_Inf_Sup.Cabin_Code
  LEFT JOIN [###Cruise_with_Special_Pricing] ON VTableCentraleDeReservations.cruise_code =[###Cruise_with_Special_Pricing].Cruise_Code
  LEFT JOIN [###Dernier_Booking_Cruise_Non_Active] ON [###Dernier_Booking_Cruise_Non_Active].Cruise_Code = vTableCentraleDeReservations.Cruise_Code
  LEFT JOIN ###Rack_PR4 On vTableCentraleDeReservations.Cruise_Code = ###Rack_PR4.Cruise_Code
WHERE (((dbo.VTableCentraleDeReservations.Client_Type)<>'Charter') AND ((IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code]='OPT' And dbo.VTableCentraleDeReservations.Client_Type<>'Indiv','BKD',dbo.VTableCentraleDeReservations.[Booking_Status_Code]))='BKD' Or (IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code]='OPT' And dbo.VTableCentraleDeReservations.Client_Type<>'Indiv','BKD',dbo.VTableCentraleDeReservations.[Booking_Status_Code]))='BKO' Or (IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code]='OPT' And dbo.VTableCentraleDeReservations.Client_Type<>'Indiv','BKD',dbo.VTableCentraleDeReservations.[Booking_Status_Code]))='INV' Or (IIf(dbo.VTableCentraleDeReservations.[Booking_Status_Code]='OPT' And dbo.VTableCentraleDeReservations.Client_Type<>'Indiv','BKD',dbo.VTableCentraleDeReservations.[Booking_Status_Code]))='CXL') AND ((dbo.VTableCentraleDeReservations.Booking_Status_GP)='Paid' Or (dbo.VTableCentraleDeReservations.Booking_Status_GP)='Cancelled') AND ((dbo.VTableCentraleDeReservations.Cabin_Status_Code)='BKD' Or (dbo.VTableCentraleDeReservations.Cabin_Status_Code)='CXL' Or (dbo.VTableCentraleDeReservations.Cabin_Status_Code)='GBK' Or (dbo.VTableCentraleDeReservations.Cabin_Status_Code)='GIP'))
AND [###Cruise_with_Special_Pricing].Cruise_Code IS NULL
AND ((IIf([###Mapping_ML_00].Cruise_Status='NON ACTIVE' And ([###Dernier_Booking_Cruise_Non_Active].MaxDeBooking_Created_Date<=dbo.vTableCentraleDeReservations.Booking_Date_OBS OR [###Dernier_Booking_Cruise_Non_Active].MaxDeBooking_Created_Date IS NULL OR [###Dernier_Booking_Cruise_Non_Active].MaxDeBooking_Created_Date = ''),1,0))=0)
GROUP BY [###Mapping_ML_00].FCST_Cluster, [###Mapping_ML_00].Vessel,[###Mapping_ML_00].Ship_Class , 
[###Mapping_ML_00].Ship_Class_2, [###Mapping_ML_00].LOS, [###Mapping_ML_00].Charter, [###Mapping_ML_00].Cruise_Status, [###Mapping_ML_00].Date_Departure, [###Mapping_ML_00].Year_Departure, [###Mapping_ML_00].From_To, [###Mapping_ML_00].Destination, [###Mapping_ML_00].Season, [###Mapping_ML_00].Theme, [###Mapping_ML_00].Guest, [###Mapping_ML_00].Partnership, [###Mapping_ML_00].Cluster_Pricing, [###Mapping_ML_00].FCST_Model, [###Mapping_ML_00].Destination_Semester, [###Mapping_ML_00].Destination_Production, [###Mapping_ML_00].Cluster_Pricing_2, [###Mapping_ML_00].Cabin_Capacity, [###Mapping_ML_00].Cluster_ML, [###Mapping_ML_00].Histo_ML, -(DATEDIFF(day,[date_departure],dbo.vTableCentraleDeReservations.[Booking_Date_OBS])), [###Mapping_ML_00].Cruise_Code, dbo.VTableCentraleDeReservations.Booking_Date_OBS
HAVING ((([###Mapping_ML_00].Charter)='N') /*AND (([###Mapping_ML_00].Cruise_Status)='ACTIVE')*/ AND (([###Mapping_ML_00].Date_Departure)>=dbo.VTableCentraleDeReservations.[Booking_Date_OBS]-6*31) AND (([###Mapping_ML_00].Destination)<>'Technical Stop') AND ((IIf([###Mapping_ML_00].[Destination]='Positioning' And [###Mapping_ML_00].[Date_Departure]<'01/05/2018',1,0))=0)) 


/*AND (([###Mapping_ML_00].Cruise_Code)='S010618') 

AND ((dbo.VTableCentraleDeReservations.Booking_Date_OBS)=CONVERT(DATETIME, '2016-10-31 00:00:00', 102))*/


;


alter table ###OTB 
add 
Total_PAX_Paying_Group_Corrected 
as (Total_Cabin_Total_Group * 1.8),
Total_NTR_EUR_Group_Corrected as (Total_NTR_MAX_GROUP * (1-0.43))


UPDATE ###OTB
SET
 
Total_Pax_Paying =
###OTB.Total_Pax_Paying_Indiv + ###OTB.Total_PAX_Paying_Group_Corrected ,

Total_PCD_Paying = (###OTB.Total_Pax_Paying_Indiv + ###OTB.Total_PAX_Paying_Group_Corrected) * Mapping_LOS ,

Total_Pax_Paying_Group =  ###OTB.Total_PAX_Paying_Group_Corrected

WHERE (Total_Pax_Paying_Group/Total_Cabin_Total_Group) > 2 
OR    (Total_Pax_Paying_Group/Total_Cabin_Total_Group)  <1.3


UPDATE ###OTB
SET
Total_NTR_EUR = (Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected),

Total_NTR_EUR_Group = Total_NTR_EUR_Group_Corrected,

Moyenne_PerDiem_Cabine_EUR = (Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected) / Total_CCD_Paying,

Moyenne_Panier_Cabine_EUR =  (Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected)/Total_Cab_Paying,

Moyenne_PerDiem_Pax_EUR = (Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected) / Total_PCD_Paying,

Moyenne_Panier_Pax_EUR = (Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected) / Total_Pax_Paying,

Erosion_OTB = 1- ((Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected) / (Total_NTR_Max)),

Yield = (Total_NTR_EUR_INDIV  + Total_NTR_EUR_Group_Corrected)/Total_ACCD,

Erosion_OTB_Group = 0.43,

Moyenne_Perdiem_Cabine_EUR_Group = Total_NTR_EUR_Group_Corrected / Total_CCD_Total_Group

WHERE  
Moyenne_Perdiem_Cabine_EUR_Group > 2000 
OR
Moyenne_Perdiem_Cabine_EUR_Group < 300
OR
Erosion_OTB_Group < 0.05
OR
Erosion_OTB_Group > 0.7
OR
Total_NTR_EUR_Group> Total_NTR_MAX_GROUP









/*OTB Options*/

IF OBJECT_ID('tempdb..[###OTB_OPTIONS]') IS NOT NULL 
DROP TABLE [###OTB_OPTIONS]
GO

print '[###OTB_OPTIONS]'

SELECT dbo.vTableCentraleDeReservations.Booking_Date_OBS AS Option_Booking_Date_OBS, [###Mapping_ML_00].Cruise_Code AS Option_Key_Cruise_Code, 
sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying)) AS Option_Total_Pax_Paying,
Sum(dbo.vTableCentraleDeReservations.Cabin_Paying) AS Option_Total_Cab_Paying, Sum(dbo.vTableCentraleDeReservations.Revenue_TAX_ENI_DNC) AS Option_Total_NTR_EUR, 
Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying))*Max(LOS) AS Option_Total_PCD_Paying,

 Sum(dbo.vTableCentraleDeReservations.Cabin_Paying)*Max(LOS) AS Option_Total_CCD_Paying, Sum(Revenue_TAX_ENI_DNC)/(Sum(dbo.vTableCentraleDeReservations.Cabin_Paying)*Max(LOS)) AS Option_Moyenne_PerDiem_Cabine_EUR, Sum(Revenue_TAX_ENI_DNC)/(Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying))*Max(LOS)) AS Option_Moyenne_PerDiem_Pax_EUR, 
 
 Sum(Revenue_TAX_ENI_DNC)/(Sum(dbo.vTableCentraleDeReservations.Cabin_Paying)) AS Option_Moyenne_Panier_Cabine_EUR,
  Sum(Revenue_TAX_ENI_DNC)/(Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying<0,0,dbo.vTableCentraleDeReservations.PAX_Paying))) AS Option_Moyenne_Panier_Pax_EUR,
 
 Sum(IIf(dbo.vTableCentraleDeReservations.PAX_Paying=1,1,0)) AS Option_Nombre_Singles, 1-Sum(IIf(dbo.vTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',Revenue_TAX_ENI_DNC,0))/Sum(IIf(dbo.vTableCentraleDeReservations.[Cabin_Status_Code]<>'CXL',IIf(dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR<dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC,dbo.VTableCentraleDeReservations.Revenue_TAX_ENI_DNC_COM_DC_OV_SS_PB_OC_EUR),0)) AS Option_Erosion_OTB, Min(IIf([client_type]='INDIV',[Booking_Created_Date],Null)) AS Option_Date_Premiere_Option_Indiv
INTO [###OTB_OPTIONS]
FROM (((dbo.VTableCentraleDeReservations INNER JOIN [###Mapping_ML_00] ON dbo.VTableCentraleDeReservations.Cruise_Code = [###Mapping_ML_00].Cruise_Code) LEFT JOIN Country ON dbo.VTableCentraleDeReservations.Client_Country = Country.Client_Country_Corrected) LEFT JOIN [###Traveller_Booking] ON (dbo.VTableCentraleDeReservations.Cabin_Status_Code = [###Traveller_Booking].Cabin_Status_Code) AND (dbo.VTableCentraleDeReservations.Booking_Status_Code = [###Traveller_Booking].Booking_Status_Code) AND (dbo.VTableCentraleDeReservations.Booking_No = [###Traveller_Booking].Booking_NO)) INNER JOIN ###Mapping_Ponts_Inf_Sup ON dbo.VTableCentraleDeReservations.Cabin_Code = ###Mapping_Ponts_Inf_Sup.Cabin_Code
  LEFT JOIN [###Cruise_with_Special_Pricing] ON VTableCentraleDeReservations.cruise_code =[###Cruise_with_Special_Pricing].Cruise_Code
WHERE ((([###Mapping_ML_00].Charter)='N') AND (([###Mapping_ML_00].Cruise_Status)='ACTIVE') AND (([###Mapping_ML_00].Destination)<>'Technical Stop') AND ((dbo.vTableCentraleDeReservations.Client_Type)='INDIV') AND ((dbo.vTableCentraleDeReservations.Booking_Status_GP)='Option') AND (([###Cruise_with_Special_Pricing].Cruise_Code) Is Null))
	AND (
	IIf([###Mapping_ML_00].[Destination]='Positioning' And [###Mapping_ML_00].[Date_Departure]<'01/05/2018',1,0)
	=0
	)
	/*AND (([###Mapping_ML_00].Cruise_Code)='Y250220')*/
	/*(dbo.vTableCentraleDeReservations.Booking_Date_OBS)=CAST(GETDATE() AS DATE) AND*/
  
GROUP BY dbo.vTableCentraleDeReservations.Booking_Date_OBS, [###Mapping_ML_00].Cruise_Code
;



/*Cluster*/
IF OBJECT_ID('tempdb..[###CLUSTER]') IS NOT NULL 
DROP TABLE [###CLUSTER]
GO

print '[###CLUSTER]'

SELECT [###OTB].Booking_Date_OBS AS Cluster_Booking_Date_OBS, [###OTB].Mapping_Season AS Cluster_Mapping_Season, [###OTB].Mapping_Destination as Cluster_Mapping_Destination, [###OTB].Mapping_Cluster_Pricing as Cluster_Mapping_Cluster_Pricing, [###OTB].Mapping_Ship_Class_2 AS Cluster_Mapping_Ship_Class_2, Avg([###OTB].Total_Pax_Paying) AS Cluster_Moyenne_Total_Pax_Paying, Avg([###OTB].Total_Cab_Paying) AS Cluster_Moyenne_Total_Cab_Paying, Avg([###OTB].Total_NTR_EUR) AS Cluster_Moyenne_Total_NTR_EUR, Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_CCD_Paying]) AS Cluster_Moyenne_PerDiem_Cabine_EUR,
Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_Cab_Paying]) AS Cluster_Moyenne_Panier_Cabine_EUR,
 Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_PCD_Paying]) AS Cluster_Moyenne_PerDiem_Pax_EUR,
 Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_PAX_Paying]) AS Cluster_Moyenne_Panier_Pax_EUR, Count([###OTB].Mapping_Vessel) AS Cluster_Nombre_De_Departs, Avg([###OTB].Total_Dispo_Cabine) AS Cluster_Moyenne_Total_Dispo_Cabine, Avg(ISNULL([###OTB].Occupation_Rate_excl_OPT,0)) AS Cluster_Moyenne_Occupation_Rate_excl_OPT, Avg([###OTB].Yield) AS Cluster_Moyenne_Yield, Avg(IIf([Erosion_OTB]>0,[Erosion_OTB],Null)) AS Cluster_Erosion_OTB,

 /*Erosion Cluster*/
 Avg(IIf([Erosion_Single_OTB]>0,[Erosion_Single_OTB],Null)) AS Cluster_Erosion_Single_OTB,
 Avg(IIf([Erosion_Offre_OTB]>0,[Erosion_Offre_OTB],Null)) AS Cluster_Erosion_Offre_OTB,
 Avg(IIf([Erosion_Commision_OTB]>0,[Erosion_Commision_OTB],Null)) AS Cluster_Erosion_Commision_OTB,
 
  Sum([###OTB].Total_ACCD) AS Cluster_Total_ACCD, IIf(Sum([###OTB].[Total_Cabin_Total_Group])>0,Sum([###OTB].[Total_Cabin_Total_Group])/(Sum([###OTB].[Total_Cabin_Paying_Indiv])+Sum([###OTB].[Total_Cabin_Total_Group])),0) AS Cluster_Part_de_Groupes, Avg(IIf([Total_Cabin_GIP_Group] Is Null,0,[Total_Cabin_GIP_Group])) AS Cluster_Moyenne_Total_Cabin_GIP_Group, IIf(Sum([###OTB].[Total_Cab_Paying])>0,Sum([Moyenne_PB]*[###OTB].[Total_Cab_Paying])/Sum([###OTB].[Total_Cab_Paying]),Null) AS Cluster_Moyenne_Moyenne_PB, IIf(Sum([###OTB].[Total_Cabin_Total_Group])>0,Sum([###OTB].[Total_Cabin_GIP_Group])/Sum([###OTB].[Total_Cabin_Total_Group]),Null) AS Cluster_Part_de_GIP,
AVG(DATEDIFF(month,[###OTB].Date_Premiere_Reservation_Indiv,Mapping_Date_Departure)) AS Cluster_Moyenne_Duree_Commercialisation_Month,
Avg([###OTB].Rack_PR4_PerDiem_Cruise_Only_EUR) AS Cluster_Moyenne_Rack_PR4_PerDiem_Cruise_Only_EUR
INTO [###CLUSTER]
FROM [###OTB]
WHERE ((([###OTB].Mapping_Cruise_Status)='ACTIVE'))
GROUP BY [###OTB].Booking_Date_OBS, [###OTB].Mapping_Season, [###OTB].Mapping_Destination, [###OTB].Mapping_Cluster_Pricing, Mapping_Ship_Class_2

;

/*SELECT * FROM [###CLUSTER]*/



/*Cluster_Destination*/
IF OBJECT_ID('tempdb..[###CLUSTER_DESTI]') IS NOT NULL 
DROP TABLE [###CLUSTER_DESTI]
GO

print '[###CLUSTER_DESTI]'

SELECT [###OTB].Booking_Date_OBS AS Cluster_Booking_Date_OBS, [###OTB].Mapping_Season AS Cluster_Mapping_Season, [###OTB].Mapping_Destination AS Cluster_Mapping_Destination, [###OTB].Mapping_Ship_Class_2 AS Cluster_Mapping_Ship_Class_2, Avg([###OTB].Total_Pax_Paying) AS Cluster_Moyenne_Total_Pax_Paying, Avg([###OTB].Total_Cab_Paying) AS Cluster_Moyenne_Total_Cab_Paying, Avg([###OTB].Total_NTR_EUR) AS Cluster_Moyenne_Total_NTR_EUR, Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_CCD_Paying]) AS Cluster_Moyenne_PerDiem_Cabine_EUR,
Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_Cab_Paying]) AS Cluster_Moyenne_Panier_Cabine_EUR,
 Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_PCD_Paying]) AS Cluster_Moyenne_PerDiem_Pax_EUR,
 Sum([###OTB].[Total_NTR_EUR])/Sum([###OTB].[Total_PAX_Paying]) AS Cluster_Moyenne_Panier_Pax_EUR, Count([###OTB].Mapping_Vessel) AS Cluster_Nombre_De_Departs, Avg([###OTB].Total_Dispo_Cabine) AS Cluster_Moyenne_Total_Dispo_Cabine, Avg([###OTB].Occupation_Rate_excl_OPT) AS Cluster_Moyenne_Occupation_Rate_excl_OPT, Avg([###OTB].Yield) AS Cluster_Moyenne_Yield, Avg(IIf([Erosion_OTB]>0,[Erosion_OTB],Null)) AS Cluster_Erosion_OTB,

 /*Erosion Cluster*/
 Avg(IIf([Erosion_Single_OTB]>0,[Erosion_Single_OTB],Null)) AS Cluster_Erosion_Single_OTB,
 Avg(IIf([Erosion_Offre_OTB]>0,[Erosion_Offre_OTB],Null)) AS Cluster_Erosion_Offre_OTB,
 Avg(IIf([Erosion_Commision_OTB]>0,[Erosion_Commision_OTB],Null)) AS Cluster_Erosion_Commision_OTB,
 
  Sum([###OTB].Total_ACCD) AS Cluster_Total_ACCD, IIf(Sum([###OTB].[Total_Cabin_Total_Group])>0,Sum([###OTB].[Total_Cabin_Total_Group])/(Sum([###OTB].[Total_Cabin_Paying_Indiv])+Sum([###OTB].[Total_Cabin_Total_Group])),0) AS Cluster_Part_de_Groupes, Avg(IIf([Total_Cabin_GIP_Group] Is Null,0,[Total_Cabin_GIP_Group])) AS Cluster_Moyenne_Total_Cabin_GIP_Group, IIf(Sum([###OTB].[Total_Cab_Paying])>0,Sum([Moyenne_PB]*[###OTB].[Total_Cab_Paying])/Sum([###OTB].[Total_Cab_Paying]),Null) AS Cluster_Moyenne_Moyenne_PB, IIf(Sum([###OTB].[Total_Cabin_Total_Group])>0,Sum([###OTB].[Total_Cabin_GIP_Group])/Sum([###OTB].[Total_Cabin_Total_Group]),Null) AS Cluster_Part_de_GIP,
AVG(DATEDIFF(month,[###OTB].Date_Premiere_Reservation_Indiv,Mapping_Date_Departure)) AS Cluster_Moyenne_Duree_Commercialisation_Month,
Avg([###OTB].Rack_PR4_PerDiem_Cruise_Only_EUR) AS Cluster_Moyenne_Rack_PR4_PerDiem_Cruise_Only_EUR
INTO [###CLUSTER_DESTI]
FROM [###OTB]
WHERE ((([###OTB].Mapping_Cruise_Status)='ACTIVE'))
GROUP BY [###OTB].Booking_Date_OBS, [###OTB].Mapping_Season, [###OTB].Mapping_Destination, Mapping_Ship_Class_2

;

/*SELECT * FROM [###CLUSTER_DESTI]*/



/*Histo*/
IF OBJECT_ID('tempdb..[###HISTO]') IS NOT NULL 
DROP TABLE [###HISTO]
GO

print '[###HISTO]'

SELECT A.*,###Mapping_Season.Season_Previous,
IIF(B.Cluster_Booking_Date_OBS IS NULL,NULL,B.Cluster_Booking_Date_OBS) AS Histo_CP1_ISNULL,
IIF(C.Cluster_Booking_Date_OBS IS NULL,NULL,C.Cluster_Booking_Date_OBS) AS Histo_DESTI_ISNULL,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Total_Pax_Paying,C.Cluster_Moyenne_Total_Pax_Paying) AS Histo_Moyenne_Total_Pax_Paying,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Total_Cab_Paying,C.Cluster_Moyenne_Total_Cab_Paying) AS Histo_Moyenne_Total_Cab_Paying,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Total_NTR_EUR,C.Cluster_Moyenne_Total_NTR_EUR) AS Histo_Moyenne_Total_NTR_EUR,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_PerDiem_Cabine_EUR,C.Cluster_Moyenne_PerDiem_Cabine_EUR) AS Histo_Moyenne_PerDiem_Cabine_EUR,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_PerDiem_Pax_EUR,C.Cluster_Moyenne_PerDiem_Pax_EUR) AS Histo_Moyenne_PerDiem_Pax_EUR,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Panier_Cabine_EUR,C.Cluster_Moyenne_Panier_Cabine_EUR) AS Histo_Moyenne_Panier_Cabine_EUR,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Panier_Pax_EUR,C.Cluster_Moyenne_Panier_Pax_EUR) AS Histo_Moyenne_Panier_Pax_EUR,

IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Nombre_De_Departs,C.Cluster_Nombre_De_Departs) AS Histo_Nombre_De_Departs,

IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Total_Dispo_Cabine,C.Cluster_Moyenne_Total_Dispo_Cabine) AS Histo_Moyenne_Total_Dispo_Cabine,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Occupation_Rate_excl_OPT,C.Cluster_Moyenne_Occupation_Rate_excl_OPT) AS Histo_Moyenne_Occupation_Rate_excl_OPT,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Yield,C.Cluster_Moyenne_Yield) AS Histo_Moyenne_Yield,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Erosion_OTB,C.Cluster_Erosion_OTB) AS Histo_Erosion_OTB,

IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Erosion_Single_OTB,C.Cluster_Erosion_Single_OTB) AS Histo_Erosion_Single_OTB,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Erosion_Offre_OTB,C.Cluster_Erosion_Offre_OTB) AS Histo_Erosion_Offre_OTB,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Erosion_Commision_OTB,C.Cluster_Erosion_Commision_OTB) AS Histo_Erosion_Commision_OTB,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Total_ACCD,C.Cluster_Total_ACCD) AS Histo_Total_ACCD,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Part_de_Groupes,C.Cluster_Part_de_Groupes) AS Histo_Part_de_Groupes,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Total_Cabin_GIP_Group,C.Cluster_Moyenne_Total_Cabin_GIP_Group) AS Histo_Moyenne_Total_Cabin_GIP_Group,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Moyenne_PB,C.Cluster_Moyenne_Moyenne_PB) AS Histo_Moyenne_Moyenne_PB,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Part_de_GIP,C.Cluster_Part_de_GIP) AS Histo_Part_de_GIP,
IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Duree_Commercialisation_Month,C.Cluster_Moyenne_Duree_Commercialisation_Month) AS Histo_Moyenne_Duree_Commercialisation_Month,

IIF(B.Cluster_Booking_Date_OBS IS NOT NULL,B.Cluster_Moyenne_Rack_PR4_PerDiem_Cruise_Only_EUR,C.Cluster_Moyenne_Rack_PR4_PerDiem_Cruise_Only_EUR) AS Histo_Moyenne_Rack_PR4_PerDiem_Cruise_Only_EUR

INTO [###HISTO]

FROM ###CLUSTER A 

INNER JOIN ###Mapping_Season
ON A.Cluster_Mapping_Season = ###Mapping_Season.Season

LEFT JOIN ###CLUSTER AS B
ON 


B.Cluster_Booking_Date_OBS = IIF(

dateadd(week,datediff(week,0,DateAdd(yy, -1, A.Cluster_Booking_Date_OBS)),0) IN
(SELECT DISTINCT Cluster_Booking_Date_OBS FROM ###CLUSTER) ,
dateadd(week,datediff(week,0,DateAdd(yy, -1, A.Cluster_Booking_Date_OBS)),0),
dateadd(week,datediff(week,0,DateAdd(yy, -1, A.Cluster_Booking_Date_OBS)),7)
)

AND ###Mapping_Season.Season_Previous = B.Cluster_Mapping_Season
AND A.Cluster_Mapping_Cluster_Pricing = B.Cluster_Mapping_Cluster_Pricing
AND A.Cluster_Mapping_Ship_Class_2 = B.Cluster_Mapping_Ship_Class_2

LEFT JOIN ###CLUSTER_DESTI AS C
ON C.Cluster_Booking_Date_OBS = IIF(

dateadd(week,datediff(week,0,DateAdd(yy, -1, A.Cluster_Booking_Date_OBS)),0) IN
(SELECT DISTINCT Cluster_Booking_Date_OBS FROM ###CLUSTER) ,
dateadd(week,datediff(week,0,DateAdd(yy, -1, A.Cluster_Booking_Date_OBS)),0),
dateadd(week,datediff(week,0,DateAdd(yy, -1, A.Cluster_Booking_Date_OBS)),7)
)
AND ###Mapping_Season.Season_Previous = C.Cluster_Mapping_Season
AND A.Cluster_Mapping_Destination = C.Cluster_Mapping_Destination 
AND A.Cluster_Mapping_Ship_Class_2 = C.Cluster_Mapping_Ship_Class_2



/*Ajout du PB courant*/
/*Ajout changement de PB et delais depuis le dernier changement*/

IF OBJECT_ID('tempdb..[###temp]') IS NOT NULL 
DROP TABLE [###temp]
GO

print 'Ajout du PB courant'

SELECT [###OTB].* , 
PBSSCalendar.PB_CAL_Amount AS Current_PB, 
SS_CAL_Amount AS Current_SupSingle,
ND_CAL_Amount AS Current_NeedDateLevel,
a.Min_PBSS_CAL_Date AS PB_Changed_Date,
(DATEDIFF(day,Min_PBSS_CAL_Date,Booking_Date_OBS)) AS Delay_PB_Changed_Days
INTO [###temp]
FROM [###OTB]
LEFT JOIN PBSSCalendar 
ON [###OTB].Key_Cruise_Code = PBSSCalendar.Cruise_Code
AND
[###OTB].Booking_Date_OBS = PBSSCalendar.PBSS_CAL_Date
LEFT JOIN (
	SELECT Cruise_Code,PB_CAL_Amount, Min(PBSSCalendar.PBSS_CAL_Date) AS Min_PBSS_CAL_Date
	FROM PBSSCalendar
	/*WHERE Cruise_Code = 'Y111018'*/
	GROUP BY Cruise_Code,PB_CAL_Amount
) a ON PBSSCalendar.Cruise_Code = a.Cruise_Code 
AND PBSSCalendar.PB_CAL_Amount = a.PB_CAL_Amount
ORDER BY Booking_Date_OBS




/*Delais minimum depuis le dernier changement*/

IF OBJECT_ID('tempdb..[###temp2]') IS NOT NULL 
DROP TABLE [###temp2]
GO

print 'Ajout du PB courant 2'

SELECT [###temp].*, 
c.Delay_PB_Changed_Days AS Delay_PB_Changed_Days_MIN,
CASE WHEN 
[###temp].Delay_PB_Changed_Days = c.Delay_PB_Changed_Days
THEN
1
ELSE
0
END
AS PB_CHANGED
INTO [###temp2]
FROM [###temp]
LEFT JOIN (
select Key_Cruise_Code,Current_PB,Min(Delay_PB_Changed_Days) AS Delay_PB_Changed_Days ,Min(Booking_Date_OBS) AS Booking_Date_OBS
from [###temp] 
WHERE Delay_PB_Changed_Days IN (	
SELECT min(Delay_PB_Changed_Days) AS Delay_PB_Changed_Days FROM [###temp] 
GROUP BY Key_Cruise_Code,Current_PB
)
GROUP BY Key_Cruise_Code,Current_PB
) c
ON [###temp].Key_Cruise_Code = c.Key_Cruise_Code
AND
[###temp].Booking_Date_OBS = c.Booking_Date_OBS
/*WHERE [###temp].Key_Cruise_Code = 'A010317'*/
ORDER BY [###temp].Key_Cruise_code,Booking_Date_OBS



/*Add Forecast*/

/*Attention au TOP 1 FCST DATE CREATION -> prendre le dernier + FULLAnalyst vs Exclude Coeff*/

/*SELECT * FROM ForecastHisto
WHERE Cruise_Code = 'Y111018'
ORDER BY ForecastHisto.FCST_DATE*/

IF OBJECT_ID('tempdb..[###temp3]') IS NOT NULL 
DROP TABLE [###temp3]
GO

print 'Add Forecast'

select t.*, X.*, IIf(X.FCST_PERDIEM_RAF<0 AND X.FCST_PERDIEM_RAF> 4500,t.Moyenne_PerDiem_Cabine_EUR,FCST_PERDIEM_RAF) AS FCST_PERDIEM_RAF_CORRECTED
INTO [###temp3]
from [###temp2] t
OUTER APPLY (
select top 1 * 
    from ForecastHisto 
    where 
	
	ForecastHisto.FCST_DATE_CREATION <= 

	CASE WHEN 
	t.PB_CHANGED = 1  
	THEN 
	/*t.Booking_Date_OBS*/
	/*A tester le -1*/
    DATEADD(DAY, -t.Delay_PB_Changed_Days_MIN-1, t.Booking_Date_OBS) 
  ELSE
   t.Booking_Date_OBS
  END	
	AND
	ForecastHisto.CRUISE_CODE=t.Key_Cruise_Code
    order by ForecastHisto.FCST_DATE_CREATION desc
) AS X
/*WHERE PB_CHANGED = 1*/
ORDER BY Key_Cruise_code,t.Booking_Date_OBS



/*Add decrochage forecast TO DO*/

IF OBJECT_ID('tempdb..[###decrochage]') IS NOT NULL 
DROP TABLE [###decrochage]
GO


SELECT 
[###temp3].Key_Cruise_Code,[###temp3].Booking_Date_OBS,
SUM(
IIf(ForecastModels.LT_Model > 1 + CAST((DATEDIFF(day,[###temp3].Mapping_Date_Departure, ###temp3.Booking_Date_OBS)) AS FLOAT)/7,
ForecastModels.FCST_Model_PU_Cabins
,
0
)
) AS Temp_FCST_ONE,

MAX([###temp3].FCST_MODELE_PU_DEFORMATION) AS Temp_FCST_TWO,



SUM(
IIf(ForecastModels.LT_Model > 1 + CAST((DATEDIFF(day,[###temp3].Mapping_Date_Departure, ###temp3.Booking_Date_OBS)) AS FLOAT)/7,
ForecastModels.FCST_Model_PU_Cabins
,
0
)
)* MAX([###temp3].FCST_MODELE_PU_DEFORMATION) AS Temp_FCST_ONE_BY_TWO,

MAX([###temp3].Total_Cab_Paying) AS Temp_FCST_THREE,
MAX([###temp3].FCST_GRP) + MAX([###temp3].FCST_PB30) + MAX([###temp3].FCST_PB25) +MAX([###temp3].FCST_PB20) +
MAX([###temp3].FCST_PB15) + MAX([###temp3].FCST_PB10) + MAX([###temp3].FCST_PB5) + MAX([###temp3].FCST_PB0)
AS Temp_FCST_FOUR,
MAX([###temp3].Total_Cab_Paying) - (MAX([###temp3].FCST_GRP) + MAX([###temp3].FCST_PB30) + MAX([###temp3].FCST_PB25) +MAX([###temp3].FCST_PB20) +
MAX([###temp3].FCST_PB15) + MAX([###temp3].FCST_PB10) + MAX([###temp3].FCST_PB5) + MAX([###temp3].FCST_PB0)) AS Temp_FCST_THREE_MINUS_FOUR,


SUM(
IIf(ForecastModels.LT_Model > 1 + CAST((DATEDIFF(day,[###temp3].Mapping_Date_Departure, ###temp3.Booking_Date_OBS)) AS FLOAT)/7,
ForecastModels.FCST_Model_PU_Cabins
,
0
)
) * MAX([###temp3].FCST_MODELE_PU_DEFORMATION)

+
MAX([###temp3].Total_Cab_Paying) - (MAX([###temp3].FCST_GRP) + MAX([###temp3].FCST_PB30) + MAX([###temp3].FCST_PB25) +MAX([###temp3].FCST_PB20) +
MAX([###temp3].FCST_PB15) + MAX([###temp3].FCST_PB10) + MAX([###temp3].FCST_PB5) + MAX([###temp3].FCST_PB0))

AS FCST_ONE_BY_TWO_PLUS_THREE_MINUS_FOUR, 


ROUND(
SUM(
IIf(ForecastModels.LT_Model > 1 + CAST((DATEDIFF(day,[###temp3].Mapping_Date_Departure, ###temp3.Booking_Date_OBS)) AS FLOAT)/7,
ForecastModels.FCST_Model_PU_Cabins
,
0
)
) * MAX([###temp3].FCST_MODELE_PU_DEFORMATION)

+
MAX([###temp3].Total_Cab_Paying) - (MAX([###temp3].FCST_GRP) + MAX([###temp3].FCST_PB30) + MAX([###temp3].FCST_PB25) +MAX([###temp3].FCST_PB20) +
MAX([###temp3].FCST_PB15) + MAX([###temp3].FCST_PB10) + MAX([###temp3].FCST_PB5) + MAX([###temp3].FCST_PB0))
,
0
)
AS FCST_ONE_BY_TWO_PLUS_THREE_MINUS_FOUR_ROUNDED

INTO [###decrochage]

FROM [###temp3] LEFT JOIN ForecastModels
ON [###temp3].Mapping_FCST_Cluster = ForecastModels.FCST_Cluster
AND [###temp3].Mapping_FCST_Model = ForecastModels.FCST_Model
GROUP BY [###temp3].Key_Cruise_Code,[###temp3].Booking_Date_OBS
ORDER BY [###temp3].Key_Cruise_Code,[###temp3].Booking_Date_OBS


/*SELECT [###temp3].*, ###decrochage.* 
FROM [###temp3] LEFT JOIN ###decrochage ON
 [###temp3].Booking_Date_OBS = ###decrochage.Booking_Date_OBS AND
[###temp3].Key_Cruise_Code = ###decrochage.Key_Cruise_Code */

/*Table Finale*/

print 'Table Finale'

SELECT [###temp3].*, 
		###GroupPU.previous_week_SommeDeCabin_Total AS Pick_Up_Group_1w,
		###GroupPU.previous_1month_SommeDeCabin_Total AS Pick_Up_Group_1m,		
DATEDIFF(month,[###temp3].Date_Premiere_Reservation_Indiv,[###temp3].Mapping_Date_Departure) AS Duree_Commercialisation_Month,
###OTB_OPTIONS.*,
###HISTO.*,
/*Filter les cas ab�rants sur les d�crochages*/
IIF(
(ISNULL(ABS(FCST_ONE_BY_TWO_PLUS_THREE_MINUS_FOUR_ROUNDED),0) - 
(ISNULL(ABS(Pick_Up_Indiv_Pos_2w),0) + ISNULL(ABS(Pick_Up_Indiv_Neg_2w),0) + ISNULL(ABS(previous_1month_SommeDeCabin_Total),0))) >= 20 OR /*FCST_MODELE_PU_DEFORMATION=0 OR */FCST_MODELE_PU_DEFORMATION>2 ,0,FCST_ONE_BY_TWO_PLUS_THREE_MINUS_FOUR_ROUNDED) AS FCST_Decrochage_Corrected

FROM [###temp3]
LEFT JOIN ###GroupPU ON [###temp3].Booking_Date_OBS = ###GroupPU.Group_Booking_Date_OBS AND
[###temp3].Key_Cruise_Code = ###GroupPU.Group_Key_Cruise_Code
LEFT JOIN ###OTB_OPTIONS ON [###temp3].CRUISE_CODE = ###OTB_OPTIONS.Option_Key_Cruise_Code
AND [###temp3].Booking_Date_OBS = ###OTB_OPTIONS.Option_Booking_Date_OBS

LEFT JOIN ###HISTO on [###temp3].Mapping_Season = ###HISTO.Cluster_Mapping_Season
AND ###temp3.Mapping_Cluster_Pricing = ###HISTO.Cluster_Mapping_Cluster_Pricing
AND ###temp3.Mapping_Ship_Class_2 = ###HISTO.Cluster_Mapping_Ship_Class_2
AND ###temp3.Booking_Date_OBS = ###HISTO.Cluster_Booking_Date_OBS

LEFT JOIN ###decrochage ON
 [###temp3].Booking_Date_OBS = ###decrochage.Booking_Date_OBS AND
[###temp3].Key_Cruise_Code = ###decrochage.Key_Cruise_Code 


WHERE


[###temp3].Booking_Date_OBS >= 
CASE 
WHEN [###temp3].Date_Premiere_Reservation_Indiv < ###OTB_OPTIONS.Option_Date_Premiere_Option_Indiv OR  ###OTB_OPTIONS.Option_Date_Premiere_Option_Indiv iS NULL
Then [###temp3].Date_Premiere_Reservation_Indiv
Else 
###OTB_OPTIONS.Option_Date_Premiere_Option_Indiv
End 
AND
[###temp3].Booking_Date_OBS <= [###temp3].Mapping_Date_Departure
AND
FCST_DATE_CREATION IS NOT NULL

/*AND 
[###temp3].Key_Cruise_Code = 'C021018'*/

ORDER BY [###temp3].Key_Cruise_Code,[###temp3].Booking_Date_OBS



